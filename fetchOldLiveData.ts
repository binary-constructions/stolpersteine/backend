import "reflect-metadata";

import { GraphQLClient } from "graphql-request";

import { writeFileSync, existsSync } from "fs";

import {
  getSdk,
  ReviewStatus as OldReviewStatus,
  Gender as OldGender,
} from "./lib/oldLiveApi/api";

import { createSchema } from "./src/createSchema";

import {
  Locale,
  Spot,
  Image,
  Stolperstein,
  Person,
  Occupation,
  PersecutionPretext,
  Place,
  RelationshipType,
  Relationship,
  ReviewStatus,
  NameType,
  Gender,
} from "./src/internal";

const PORT = 1945;

const USE_SPREAD_ON_IMPORT = false;

const mapReviewStatus = (old: OldReviewStatus): ReviewStatus => {
  switch (old) {
    case OldReviewStatus.NeedsReview:
      return ReviewStatus.NEEDS_REVIEW;
    case OldReviewStatus.Released:
      return ReviewStatus.RELEASED;
    case OldReviewStatus.Stub:
      return ReviewStatus.STUB;
    case OldReviewStatus.Undefined:
      return ReviewStatus.UNDEFINED;
    case OldReviewStatus.WorkInProgress:
      return ReviewStatus.WORK_IN_PROGRESS;
    default:
      throw new Error("unknown review status: " + old);
  }
};

const mapNameType = (
  old:
    | "BirthName"
    | "FamilyName"
    | "Nickname"
    | "Pseudonym"
    | "MarriedName"
    | "GivenName"
) => {
  switch (old) {
    case "BirthName":
      return NameType.BIRTH;
    case "FamilyName":
      return NameType.FAMILY;
    case "Nickname":
      return NameType.NICKNAME;
    case "Pseudonym":
      return NameType.PSEUDONYM;
    case "MarriedName":
      return NameType.MARRIED;
    case "GivenName":
      return NameType.GIVEN;
    default:
      throw new Error("unknown name type: " + old);
  }
};

const mapGender = (old: OldGender) => {
  switch (old) {
    case OldGender.Female:
      return Gender.FEMALE;
    case OldGender.Male:
      return Gender.MALE;
    case OldGender.NonBinary:
      return Gender.NON_BINARY;
    case OldGender.Unspecified:
      return Gender.UNSPECIFIED;
    default:
      throw new Error("unknown gender: " + old);
  }
};

async function main(): Promise<void> {
  const schema = await createSchema();

  if (schema) {
    console.debug("Got a schema... what now?");
  }

  const client = new GraphQLClient("http://localhost:" + PORT + "/graphql");

  const oldLiveApi = getSdk(client);

  const importDir = "./import";

  const dataDir = "./data";

  if (!existsSync(importDir)) {
    throw new Error("import dir missing!");
  }

  if (!existsSync(dataDir)) {
    throw new Error("data dir missing!");
  }

  const writeRaw = (object: Object, baseName: string) => {
    const rawFileName = importDir + "/" + baseName + ".raw.json";

    if (existsSync(rawFileName)) {
      throw new Error("raw file base name already used!");
    }

    writeFileSync(rawFileName, JSON.stringify(object, undefined, 2));
  };

  await (async () => {
    const baseName = "Summaries";

    const result = await oldLiveApi[baseName]();

    writeRaw(result, baseName);
  })();

  await (async () => {
    const baseName = "Locales";

    const result = await oldLiveApi.Locales();

    writeRaw(result, baseName);

    for (const entity of result.locales) {
      await Locale.create(entity);
    }
  })();

  await (async () => {
    const baseName = "Occupations";

    const result = await oldLiveApi[baseName]();

    writeRaw(result, baseName);

    for (const entity of result.occupations) {
      await Occupation.create({
        ...(USE_SPREAD_ON_IMPORT ? entity : {}),
        typename: entity.__typename,
        id: entity.id,
        localeCode: entity.locale.code,
        label: entity.label,
        label_f: entity.label_f || null,
        label_m: entity.label_m || null,
        localizations: entity.localizations.map((v) => ({
          ...(USE_SPREAD_ON_IMPORT ? v : {}),
          label: v.label,
          localeCode: v.locale.code,
          label_f: v.label_f || null,
          label_m: v.label_m || null,
        })),
      });
    }
  })();

  await (async () => {
    const baseName = "PersecutionPretexts";

    const result = await oldLiveApi[baseName]();

    writeRaw(result, baseName);

    for (const entity of result.persecutionPretexts) {
      await PersecutionPretext.create({
        ...(USE_SPREAD_ON_IMPORT ? entity : {}),
        typename: entity.__typename,
        id: entity.id,
        localeCode: entity.locale.code,
        label: entity.label,
        label_f: entity.label_f || null,
        label_m: entity.label_m || null,
        localizations: entity.localizations.map((v) => ({
          ...(USE_SPREAD_ON_IMPORT ? v : {}),
          label: v.label,
          localeCode: v.locale.code,
          label_f: v.label_f || null,
          label_m: v.label_m || null,
        })),
      });
    }
  })();

  await (async () => {
    const baseName = "Places";

    const result = await oldLiveApi[baseName]();

    writeRaw(result, baseName);

    let remaining = result.places;

    while (remaining.length) {
      const existingIds = (await Place.getRepository().find()).map((v) => v.id);

      const nextUp = remaining.filter(
        (v) => !v.parent || existingIds.includes(v.parent.id)
      );

      if (!nextUp.length) {
        throw new Error("only places with unknown parent remaining!");
      }

      for (const next of nextUp) {
        await Place.create({
          ...(USE_SPREAD_ON_IMPORT ? next : {}),
          typename: next.__typename,
          id: next.id,
          parentId: next.parent?.id || null,
          label: next.label,
          alternativeNames: next.alternativeNames,
        });
      }

      const lastUpIds = nextUp.map((v) => v.id);

      remaining = remaining.filter((v) => !lastUpIds.includes(v.id));
    }
  })();

  await (async () => {
    const baseName = "Images";

    const result = await oldLiveApi[baseName]({ ids: null });

    writeRaw(result, baseName);

    for (const entity of result.images) {
      await Image.create({
        ...(USE_SPREAD_ON_IMPORT ? entity : {}),
        typename: entity.__typename,
        id: entity.id,
        caption: entity.caption || null,
        alternativeText: entity.alternativeText || null,
        authorshipRemark: entity.authorshipRemark,
        fileName: undefined,
      });
    }
  })();

  await (async () => {
    const baseName = "Spots";

    const result = await oldLiveApi[baseName]({ ids: null });

    writeRaw(result, baseName);

    for (const entity of result.spots) {
      await Spot.create({
        ...(USE_SPREAD_ON_IMPORT ? entity : {}),
        typename: entity.__typename,
        id: entity.id,
        point: entity.point,
        imageIds: [],
        street: entity.street || null,
        streetDetail: entity.streetDetail
          ? {
              ...(USE_SPREAD_ON_IMPORT ? entity.streetDetail : {}),
              typename: entity.streetDetail.__typename,
              value: entity.streetDetail.value,
            }
          : null,
        postalCode: entity.postalCode || null,
        placeId: entity.placeId || null,
        note: entity.note || null,
        created: entity.created ? Date.parse(entity.created) : null,
        lastModified: entity.lastModified
          ? Date.parse(entity.lastModified)
          : null,
        deleted: entity.deleted ? Date.parse(entity.deleted) : null,
        reviewStatus: mapReviewStatus(entity.reviewStatus),
      });
    }
  })();

  await (async () => {
    const baseName = "Persons";

    const result = await oldLiveApi[baseName]({ ids: null });

    writeRaw(result, baseName);

    for (const entity of result.persons) {
      await Person.create({
        ...(USE_SPREAD_ON_IMPORT ? entity : {}),
        typename: entity.__typename,
        id: entity.id,
        names: entity.names.map((v) => ({
          ...(USE_SPREAD_ON_IMPORT ? v : {}),
          typename: mapNameType(v.__typename),
          value: v.value,
        })),
        gender: mapGender(entity.gender),
        title: entity.title
          ? {
              ...(USE_SPREAD_ON_IMPORT ? entity.title : {}),
              typename: entity.title.__typename || "PersonTitle",
              value: entity.title.value,
            }
          : null,
        occupationIds: entity.occupations.map((v) => v.id),
        bornDate: entity.bornDate || null,
        bornPlaceId: entity.bornPlace?.id || null,
        diedDate: entity.diedDate || null,
        diedPlaceId: entity.diedPlace?.id || null,
        persecutionPretextIds: entity.persecutionPretexts.map((v) => v.id),
        biographyHTML: entity.biographyHTML || null,
        editingNotesHTML: entity.editingNotesHTML || null,
        created: entity.created ? Date.parse(entity.created) : null,
        lastModified: entity.lastModified
          ? Date.parse(entity.lastModified)
          : null,
        deleted: entity.deleted ? Date.parse(entity.deleted) : null,
        reviewStatus: mapReviewStatus(entity.reviewStatus),
      });
    }
  })();

  await (async () => {
    const baseName = "Stolpersteine";

    const result = await oldLiveApi[baseName]({ ids: null });

    writeRaw(result, baseName);

    for (const entity of result.stolpersteine) {
      await Stolperstein.create({
        ...(USE_SPREAD_ON_IMPORT ? entity : {}),
        typename: entity.__typename,
        id: entity.id,
        laid: entity.laid || null,
        subjectIds: entity.subjects.map((v) => v.id),
        spotId: entity.spot.id,
        imageId: entity.image?.id || null,
        historicalStreet: entity.historicalStreet || null,
        historicalStreetDetail: entity.historicalStreetDetail
          ? {
              ...(USE_SPREAD_ON_IMPORT ? entity.historicalStreetDetail : {}),
              typename: entity.historicalStreetDetail.__typename,
              value: entity.historicalStreetDetail.value,
            }
          : null,
        historicalPlaceId: entity.historicalPlace?.id || null,
        inscription: entity.inscription || null,
        initiativeHTML: entity.initiativeHTML || null,
        patronageHTML: entity.patronageHTML || null,
        dataGatheringHTML: entity.dataGatheringHTML || null,
        fundingHTML: entity.fundingHTML || null,
        created: entity.created ? Date.parse(entity.created) : null,
        lastModified: entity.lastModified
          ? Date.parse(entity.lastModified)
          : null,
        deleted: entity.deleted ? Date.parse(entity.deleted) : null,
        reviewStatus: mapReviewStatus(entity.reviewStatus),
      });
    }
  })();

  await (async () => {
    const baseName = "RelationshipTypes";

    const result = await oldLiveApi[baseName]();

    writeRaw(result, baseName);

    for (const entity of result.relationshipTypes) {
      await RelationshipType.create({
        ...(USE_SPREAD_ON_IMPORT ? entity : {}),
        typename: entity.__typename,
        id: entity.id,
        localeCode: entity.locale.code,
        subjectLabel: entity.subjectLabel,
        subjectLabel_f: entity.subjectLabel_f || null,
        subjectLabel_m: entity.subjectLabel_m || null,
        objectLabel: entity.objectLabel,
        objectLabel_f: entity.objectLabel_f || null,
        objectLabel_m: entity.objectLabel_m || null,
        localizations: entity.localizations.map((v) => ({
          ...(USE_SPREAD_ON_IMPORT ? v : {}),
          localeCode: v.locale.code,
          subjectLabel: v.subjectLabel,
          subjectLabel_f: v.subjectLabel_f || null,
          subjectLabel_m: v.subjectLabel_m || null,
          objectLabel: v.objectLabel,
          objectLabel_f: v.objectLabel_f || null,
          objectLabel_m: v.objectLabel_m || null,
        })),
      });
    }
  })();

  await (async () => {
    const baseName = "Relationships";

    const result = await oldLiveApi[baseName]();

    writeRaw(result, baseName);

    for (const entity of result.relationships) {
      await Relationship.create({
        ...(USE_SPREAD_ON_IMPORT ? entity : {}),
        typename: entity.__typename,
        // id: -- no id in the old system --
        typeId: entity.type.id,
        subjectId: entity.subject.id,
        objectId: entity.object.id,
      });
    }
  })();
}

main()
  .then(() => console.log("All done!"))
  .catch((error) => console.error("Error: ", error));
