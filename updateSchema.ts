import "reflect-metadata";

import { createSchema } from './src/createSchema';

createSchema(true);                // this will output schema.gql
