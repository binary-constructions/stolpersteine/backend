import { registerEnumType } from "type-graphql";

enum PlaceType {
  UNSPECIFIED = "unspecified",
  COUNTRY = "country",
  PROVINCE = "province",
  RURAL_DISTRICT = "rural_district",
  LOCALITY = "ortschaft",
  DISTRICT = "district",
}

registerEnumType(PlaceType, {
  name: "PlaceType",
  description: "The type of a geographic place.",
});

export { PlaceType };
