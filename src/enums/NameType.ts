import { registerEnumType } from "type-graphql";

export enum NameType {
  //UNKNOWN = 'unknown',
  GIVEN = "GivenName",
  NICKNAME = "Nickname",
  PSEUDONYM = "Pseudonym",
  FAMILY = "FamilyName", // family name without information on whether it's a birth name or married name
  BIRTH = "BirthName",
  MARRIED = "MarriedName",
}

registerEnumType(NameType, {
  name: "NameType",
  description: "The type of a name of a person.",
});
