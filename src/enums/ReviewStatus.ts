import { registerEnumType } from "type-graphql";

enum ReviewStatus {
  UNDEFINED = "undefined",
  STUB = "stub",
  WORK_IN_PROGRESS = "work in progress",
  NEEDS_REVIEW = "needs review",
  RELEASED = "released",
}

registerEnumType(ReviewStatus, {
  name: "ReviewStatus",
  description: "The review status of a content item.",
});

export { ReviewStatus };
