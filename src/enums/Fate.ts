import { registerEnumType } from "type-graphql";

enum Fate {
  UNKNOWN = "unknown",
  MURDERED = "murdered",
  SURVIVED = "survived",
  ESCAPED = "escaped",
}

registerEnumType(Fate, {
  name: "Fate",
});

export { Fate };
