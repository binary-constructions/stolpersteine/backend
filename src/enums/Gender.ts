import { registerEnumType } from "type-graphql";

enum Gender {
  UNSPECIFIED = "unspecified",
  NON_BINARY = "non-binary",
  FEMALE = "female",
  MALE = "male",
}

registerEnumType(Gender, {
  name: "Gender",
  description:
    "Gender identities as either actually or at least presumably attributed to a person by themselves and/or others (falling back to classical ideas of gender dichotomy in cases where nothing to the contrary is specifically known).",
});

export { Gender };
