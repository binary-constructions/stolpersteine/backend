import { ObjectType, Field } from "type-graphql";

import { Place } from "../internal";

@ObjectType({
  description:
    "Condensed information on a place as well as aassociated persons and Stolpersteine.",
})
export class PlaceSummary {
  @Field()
  readonly id: string;

  @Field()
  readonly label: string;

  @Field((_) => String, { nullable: true })
  readonly parentId: string | null;

  @Field((_) => [String])
  readonly alternativeNames: string[];

  constructor(place: Place) {
    this.id = place.id;

    this.label = place.label;

    this.parentId = place.parentId;

    this.alternativeNames = place.alternativeNames;
  }

  static async find(): Promise<PlaceSummary[]> {
    return (await Place.getRepository().find()).map((v) => new PlaceSummary(v));
  }
}
