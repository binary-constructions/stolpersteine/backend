import { ObjectType, Field, Int } from "type-graphql";

import { GeoPoint, ReviewStatus, Spot, StolpersteinSummary } from "../internal";

@ObjectType({
  description:
    "Condensed information on a spot as well as aassociated persons and Stolpersteine.",
})
export class SpotSummary {
  @Field()
  readonly id: string;

  @Field((_) => Date, { nullable: true })
  readonly created: Date | null;

  @Field((_) => Date, { nullable: true })
  readonly lastModified: Date | null;

  @Field((_) => Date, { nullable: true })
  readonly deleted: Date | null;

  @Field((_) => ReviewStatus, { nullable: true })
  readonly reviewStatus: ReviewStatus | null;

  @Field((_) => GeoPoint)
  readonly point: GeoPoint;

  @Field((_) => String, { nullable: true })
  readonly street: string | null;

  @Field((_) => String, { nullable: true })
  readonly streetNumber: string | null;

  @Field((_) => String, { nullable: true })
  readonly streetCorner: string | null;

  @Field((_) => String, { nullable: true })
  readonly postalCode: string | null;

  @Field((_) => String, { nullable: true })
  readonly placeId: string | null;

  @Field((_) => Int)
  readonly imageCount: number;

  @Field((_) => [StolpersteinSummary])
  stolpersteinSummaries: StolpersteinSummary[];

  constructor(
    spot: Spot,
    stolpersteine: { [spotId: string]: StolpersteinSummary[] | undefined }
  ) {
    this.id = spot.id;

    this.created = spot.created ? new Date(spot.created) : null;

    this.lastModified = spot.lastModified ? new Date(spot.lastModified) : null;

    this.deleted = spot.deleted ? new Date(spot.deleted) : null;

    this.reviewStatus = spot.reviewStatus;

    this.point = spot.point;

    this.street = spot.street;

    this.streetNumber =
      spot.streetDetail?.typename === "StreetDetailNumber"
        ? spot.streetDetail.value
        : null;

    this.streetCorner =
      spot.streetDetail?.typename === "StreetDetailCorner"
        ? spot.streetDetail.value
        : null;

    this.postalCode = spot.postalCode;

    this.placeId = spot.placeId;

    this.imageCount = spot.imageIds.length;

    this.stolpersteinSummaries = stolpersteine[spot.id] || [];
  }

  static async find(): Promise<SpotSummary[]> {
    const stolperteineBySpotId = await StolpersteinSummary.findAndMapToSpotId();

    return (await Spot.getRepository().find()).map(
      (v) => new SpotSummary(v, stolperteineBySpotId)
    );
  }
}
