import { ObjectType, Field } from "type-graphql";

import { ReviewStatus, Stolperstein, PersonSummary } from "../internal";

type StolpersteinSummariesBySpotId = {
  [spotId: string]: StolpersteinSummary[] | undefined;
};

@ObjectType({
  description:
    "Condensed information on a Stolperstein as well as aassociated persons.",
})
class StolpersteinSummary {
  @Field()
  readonly id: string;

  @Field((_) => Date, { nullable: true })
  readonly created: Date | null;

  @Field((_) => Date, { nullable: true })
  readonly lastModified: Date | null;

  @Field((_) => Date, { nullable: true })
  readonly deleted: Date | null;

  @Field((_) => ReviewStatus, { nullable: true })
  readonly reviewStatus: ReviewStatus | null;

  @Field((_) => String, { nullable: true })
  readonly spotId: string | null;

  @Field((_) => String, { nullable: true })
  readonly laid: string | null;

  @Field()
  readonly hasImage: boolean;

  @Field((_) => [PersonSummary])
  readonly personSummaries: PersonSummary[];

  constructor(stolperstein: Stolperstein, personSummaries: PersonSummary[]) {
    this.id = stolperstein.id;

    this.created = stolperstein.created ? new Date(stolperstein.created) : null;

    this.lastModified = stolperstein.lastModified
      ? new Date(stolperstein.lastModified)
      : null;

    this.deleted = stolperstein.deleted ? new Date(stolperstein.deleted) : null;

    this.reviewStatus = stolperstein.reviewStatus;

    this.spotId = stolperstein.spotId;

    this.laid = stolperstein.laid?.serialize() || null;

    this.hasImage = !!stolperstein.imageId;

    this.personSummaries = personSummaries.filter((v) =>
      v.stolpersteinIds.includes(stolperstein.id)
    );
  }

  static async find(): Promise<StolpersteinSummary[]> {
    const personSummaries = await PersonSummary.find();

    return (await Stolperstein.getRepository().find()).map(
      (v) => new StolpersteinSummary(v, personSummaries)
    );
  }

  static async findAndMapToSpotId(): Promise<StolpersteinSummariesBySpotId> {
    return (await StolpersteinSummary.find()).reduce((acc, cur) => {
      if (cur.spotId !== null) {
        if (cur.spotId in acc) {
          acc[cur.spotId]!.push(cur);
        } else {
          acc[cur.spotId] = [cur];
        }
      }

      return acc;
    }, {} as StolpersteinSummariesBySpotId);
  }
}

export { StolpersteinSummary };
