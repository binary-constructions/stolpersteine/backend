import { ObjectType, Field, Int } from "type-graphql";

import {
  ReviewStatus,
  Person,
  PersonNameInterface,
  Stolperstein,
  Fate,
  Gender,
} from "../internal";

@ObjectType({ description: "Condensed information on a Person." })
export class PersonSummary {
  @Field()
  readonly id: string;

  @Field((_) => Date, { nullable: true })
  readonly created: Date | null;

  @Field((_) => Date, { nullable: true })
  readonly lastModified: Date | null;

  @Field((_) => Date, { nullable: true })
  readonly deleted: Date | null;

  @Field((_) => ReviewStatus, { nullable: true })
  readonly reviewStatus: ReviewStatus | null;

  @Field((_) => [String])
  readonly stolpersteinIds: string[];

  @Field((_) => [PersonNameInterface])
  readonly names: PersonNameInterface[];

  @Field((_) => String, { nullable: true })
  readonly title: string | null;

  @Field((_) => Gender)
  readonly gender: Gender;

  @Field((_) => String, { nullable: true })
  readonly bornDate: string | null;

  @Field((_) => String, { nullable: true })
  readonly diedDate: string | null;

  @Field((_) => [String])
  readonly persecutionPretextIds: string[];

  @Field((_) => Fate)
  readonly fate: Fate;

  @Field((_) => Int)
  readonly imageCount: number;

  constructor(person: Person, stolpersteine: Stolperstein[]) {
    this.id = person.id;

    this.created = person.created ? new Date(person.created) : null;

    this.lastModified = person.lastModified
      ? new Date(person.lastModified)
      : null;

    this.deleted = person.deleted ? new Date(person.deleted) : null;

    this.reviewStatus = person.reviewStatus;

    this.stolpersteinIds = stolpersteine
      .filter((v) => v.subjectIds.includes(person.id))
      .map((v) => v.id);

    this.names = person.names;

    this.title = person.title?.value || null;

    this.gender = person.gender;

    this.bornDate = person.bornDate?.serialize() || null;

    this.diedDate = person.diedDate?.serialize() || null;

    this.persecutionPretextIds = person.persecutionPretextIds;

    this.fate = person.fate;

    this.imageCount = person.imageIds.length;
  }

  static async find(): Promise<PersonSummary[]> {
    const stolpersteine = await Stolperstein.getRepository().find();

    return (await Person.getRepository().find()).map(
      (v) => new PersonSummary(v, stolpersteine)
    );
  }
}
