import { InterfaceType, Field } from "type-graphql";

@InterfaceType("TypedLiteral", {
  description: "A literal value differentiated by type.",
})
export abstract class TypedLiteralInterface {
  @Field()
  readonly value: string;

  constructor(value: string) {
    this.value = value;
  }
}
