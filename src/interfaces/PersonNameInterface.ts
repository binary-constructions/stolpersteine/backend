import { InterfaceType, Field } from "type-graphql";

import { NameType } from "../internal";

@InterfaceType("PersonName", {
  description: "The name of a person.",
})
export abstract class PersonNameInterface {
  abstract readonly typename: NameType;

  @Field()
  readonly value: string;

  constructor(value: string) {
    this.value = value;
  }
}
