import * as moment from "moment";

const date_formats = ["YYYY-MM-DD", "YYYY-MM", "YYYY"];

type HistoricalDateValue = string | { between: string; and: string };

export class HistoricalDate {
  readonly value: HistoricalDateValue;

  readonly isApproximation: boolean = false;

  readonly isPresumption: boolean = false;

  serialize(): string {
    let serialization: string = "";

    if (this.isApproximation) {
      serialization += "~";
    }

    if (this.isPresumption) {
      serialization += "?";
    }

    if (typeof this.value === "string") {
      serialization += this.value;
    } else {
      serialization += this.value.between + "/" + this.value.and;
    }

    return serialization;
  }

  constructor(literal_value: string) {
    if (literal_value.startsWith("~")) {
      this.isApproximation = true;

      literal_value = literal_value.substr(1);
    }

    if (literal_value.startsWith("?")) {
      this.isPresumption = true;

      literal_value = literal_value.substr(1);
    }

    if (literal_value.includes("/")) {
      const [between_date, and_date] = literal_value.split("/", 2);

      if (!moment(between_date, date_formats, true).isValid()) {
        throw new Error("Invalid date string: " + between_date);
      }

      if (!moment(and_date, date_formats, true).isValid()) {
        throw new Error("Invalid date string: " + and_date);
      }

      this.value = { between: between_date, and: and_date };
    } else {
      if (!moment(literal_value, date_formats, true).isValid()) {
        throw new Error("Invalid date string: " + literal_value);
      }

      this.value = literal_value;
    }
  }

  toJSON(): string {
    return this.serialize();
  }
}
