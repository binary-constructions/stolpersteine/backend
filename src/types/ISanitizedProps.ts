import { IProps } from "../internal";

// FIXME: should better be
//   ISanitizedProps<T extends IProps> extends T { __sanitized: () => true }
// or something...

export interface ISanitizedProps extends IProps {
  __sanitized: () => true;
}
