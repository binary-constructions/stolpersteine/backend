import { GraphQLSchema } from "graphql";
import { Container } from "typedi";
import { buildSchema } from "type-graphql";

import {
  // interfaces
  EntityResolver,
  NodeResolver,
  ContentResolver,
  StolpersteinSubjectResolver,
  // meta
  NodesResolver,
  ContentsResolver,
  StolpersteinSubjectsResolver,
  // nodes
  ImageResolver,
  LocaleResolver,
  OccupationLocalizationResolver,
  OccupationResolver,
  PersecutionPretextLocalizationResolver,
  PersecutionPretextResolver,
  PersonResolver,
  PlaceResolver,
  RelationshipResolver,
  RelationshipTypeLocalizationResolver,
  RelationshipTypeResolver,
  SpotResolver,
  StolpersteinResolver,
  // summaries
  PlaceSummaryResolver,
  SpotSummaryResolver,
} from "./internal";

export function createSchema(emit: boolean = false): Promise<GraphQLSchema> {
  return buildSchema({
    resolvers: [
      // interfaces
      EntityResolver,
      NodeResolver,
      ContentResolver,
      StolpersteinSubjectResolver,
      // meta
      NodesResolver,
      ContentsResolver,
      StolpersteinSubjectsResolver,
      // nodes
      ImageResolver,
      LocaleResolver,
      OccupationLocalizationResolver,
      OccupationResolver,
      PersecutionPretextLocalizationResolver,
      PersecutionPretextResolver,
      PersonResolver,
      PlaceResolver,
      RelationshipResolver,
      RelationshipTypeLocalizationResolver,
      RelationshipTypeResolver,
      SpotResolver,
      StolpersteinResolver,
      // summaries
      PlaceSummaryResolver,
      SpotSummaryResolver,
    ],
    container: Container,
    emitSchemaFile: emit,
    dateScalarMode: "isoDate", // TODO: 'timestamp'?
  });
}
