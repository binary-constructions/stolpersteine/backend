import { Field, ObjectType } from "type-graphql";

/**
 * The class of all localization objects for `Occupation` entities.
 */
@ObjectType({ description: "A localization for an Occupation object." })
export class OccupationLocalization {
  @Field()
  readonly localeCode: string;

  @Field()
  readonly label: string;

  @Field((_) => String, { nullable: true })
  readonly label_f: string | null;

  @Field((_) => String, { nullable: true })
  readonly label_m: string | null;

  constructor(props: OccupationLocalizationProps) {
    this.localeCode = props.localeCode;

    this.label = props.label;

    this.label_f = props.label_f;

    this.label_m = props.label_m;
  }
}

interface OccupationLocalizationProps extends OccupationLocalization {}
