import { ObjectType } from "type-graphql";

import {
  PersonNameInterface,
  TypedLiteralInterface,
  NameType,
} from "../internal";

@ObjectType({
  implements: [PersonNameInterface, TypedLiteralInterface],
  description:
    "A family name given to a person at birth (as opposed to by marriage).",
})
export class BirthName extends PersonNameInterface {
  readonly typename = NameType.BIRTH;
}
