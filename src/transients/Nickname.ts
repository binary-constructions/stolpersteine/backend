import { ObjectType } from "type-graphql";

import {
  NameType,
  PersonNameInterface,
  TypedLiteralInterface,
} from "../internal";

@ObjectType({
  implements: [PersonNameInterface, TypedLiteralInterface],
  description: "A name given to a person by friends or others.",
})
export class Nickname extends PersonNameInterface {
  readonly typename = NameType.NICKNAME;
}
