import { ObjectType } from "type-graphql";

import { TypedLiteralInterface } from "../internal";

@ObjectType({
  implements: TypedLiteralInterface,
  description: "A street detail given as a cornering street.",
})
export class StreetDetailCorner extends TypedLiteralInterface {
  typename = "StreetDetailCorner";
}
