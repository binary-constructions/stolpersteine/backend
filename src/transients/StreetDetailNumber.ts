import { ObjectType } from "type-graphql";

import { TypedLiteralInterface } from "../internal";

@ObjectType({
  implements: TypedLiteralInterface,
  description: "A street detail given as a street number.",
})
export class StreetDetailNumber extends TypedLiteralInterface {
  typename = "StreetDetailNumber";
}
