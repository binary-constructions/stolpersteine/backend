import { ObjectType, Field } from "type-graphql";

@ObjectType({ description: "A geographic point on the map." })
export class GeoPoint {
  @Field()
  longitude: number;

  @Field()
  latitude: number;

  constructor(plain: { longitude: number; latitude: number }) {
    this.longitude = plain.longitude;

    this.latitude = plain.latitude;
  }
}
