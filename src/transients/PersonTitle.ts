import { ObjectType } from "type-graphql";

import { TypedLiteralInterface } from "../internal";

@ObjectType({
  implements: TypedLiteralInterface,
  description: "The title of a person.",
})
export class PersonTitle extends TypedLiteralInterface {
  static readonly typename = "PersonTitle";

  readonly typename = "PersonTitle";

  // TODO: add field resolver for something like "titlesInUse"?
}
