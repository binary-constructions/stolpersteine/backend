import { ObjectType } from "type-graphql";

import {
  NameType,
  PersonNameInterface,
  TypedLiteralInterface,
} from "../internal";

@ObjectType({
  implements: [PersonNameInterface, TypedLiteralInterface],
  description: "A family name given to a person at marriage.",
})
export class MarriedName extends PersonNameInterface {
  readonly typename = NameType.MARRIED;
}
