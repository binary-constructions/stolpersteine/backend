import { ObjectType } from "type-graphql";

import {
  PersonNameInterface,
  TypedLiteralInterface,
  NameType,
} from "../internal";

@ObjectType({
  implements: [PersonNameInterface, TypedLiteralInterface],
  description:
    "A name indicating the family affiliation of a person (without further information on whether it was assumed at birth or at marriage).",
})
export class FamilyName extends PersonNameInterface {
  readonly typename = NameType.FAMILY;
}
