import { Field, ObjectType } from "type-graphql";

/**
 * The class of all localization objects for `RelationshipType`
 * entities.
 */
@ObjectType({
  description: "A localization object for a RelationshipType object.",
})
export class RelationshipTypeLocalization {
  @Field()
  readonly localeCode: string;

  @Field()
  readonly subjectLabel: string;

  @Field((_) => String, { nullable: true })
  readonly subjectLabel_f: string | null;

  @Field((_) => String, { nullable: true })
  readonly subjectLabel_m: string | null;

  @Field()
  readonly objectLabel: string;

  @Field((_) => String, { nullable: true })
  readonly objectLabel_f: string | null;

  @Field((_) => String, { nullable: true })
  readonly objectLabel_m: string | null;

  constructor(props: RelationshipTypeLocalizationProps) {
    this.localeCode = props.localeCode;

    this.subjectLabel = props.subjectLabel;

    this.subjectLabel_f = props.subjectLabel_f;

    this.subjectLabel_m = props.subjectLabel_m;

    this.objectLabel = props.objectLabel;

    this.objectLabel_f = props.objectLabel_f;

    this.objectLabel_m = props.objectLabel_m;
  }
}

interface RelationshipTypeLocalizationProps
  extends RelationshipTypeLocalization {}
