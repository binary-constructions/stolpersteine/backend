import { ObjectType } from "type-graphql";

import {
  NameType,
  PersonNameInterface,
  TypedLiteralInterface,
} from "../internal";

@ObjectType({
  implements: [PersonNameInterface, TypedLiteralInterface],
  description: "A name given to a person usually at or soon after birth.",
})
export class GivenName extends PersonNameInterface {
  readonly typename = NameType.GIVEN;
}
