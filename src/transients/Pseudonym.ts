import { ObjectType } from "type-graphql";

import {
  NameType,
  PersonNameInterface,
  TypedLiteralInterface,
} from "../internal";

@ObjectType({
  implements: [PersonNameInterface, TypedLiteralInterface],
  description: "A name assumed by a person publicly for a particular purpose.",
})
export class Pseudonym extends PersonNameInterface {
  typename: NameType.PSEUDONYM = NameType.PSEUDONYM;
}
