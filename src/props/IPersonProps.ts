import {
  IStolpersteinSubjectProps,
  HistoricalDate,
  PersonTitle,
  PersonNameInterface,
  Gender,
  Fate,
} from "../internal";

export interface IPersonProps extends IStolpersteinSubjectProps<"Person"> {
  readonly names: PersonNameInterface[];

  readonly occupationIds: string[];

  readonly title: PersonTitle | null;

  readonly gender: Gender;

  readonly bornDate: HistoricalDate | null;

  readonly bornPlaceId: string | null;

  readonly diedDate: HistoricalDate | null;

  readonly diedPlaceId: string | null;

  readonly persecutionPretextIds: string[];

  readonly fate: Fate;

  readonly escapedToPlaceId: string | null;

  readonly imageIds: string[];

  readonly biographyHTML: string | null;

  readonly editingNotesHTML: string | null;
}
