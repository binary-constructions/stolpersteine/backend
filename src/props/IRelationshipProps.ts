import { INodeProps } from "../internal";

export interface IRelationshipProps extends INodeProps<"Relationship"> {
  readonly subjectId: string;

  readonly objectId: string;

  readonly typeId: string;
}
