import { INodeProps } from "../internal";

export interface IImageProps extends INodeProps<"Image"> {
  readonly authorshipRemark: string;

  readonly caption: string | null;

  readonly alternativeText: string | null;

  readonly data?: string;

  readonly fileName?: string;
}
