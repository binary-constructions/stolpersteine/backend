import { INodeProps } from "../internal";

export interface IPlaceProps extends INodeProps<"Place"> {
  readonly label: string;

  readonly parentId: string | null;

  readonly alternativeNames: string[];
}
