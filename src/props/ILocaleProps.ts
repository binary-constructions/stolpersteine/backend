import { IEntityProps } from "../internal";

export interface ILocaleProps extends IEntityProps<"Locale"> {
  readonly code: string;

  readonly label: string;
}
