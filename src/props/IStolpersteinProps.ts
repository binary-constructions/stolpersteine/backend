import {
  IContentProps,
  HistoricalDate,
  StreetDetailCorner,
  StreetDetailNumber,
} from "../internal";

export interface IStolpersteinProps extends IContentProps<"Stolperstein"> {
  readonly spotId: string | null;

  readonly laid: HistoricalDate | null;

  readonly inscription: string | null;

  readonly imageId: string | null;

  readonly subjectIds: string[];

  readonly historicalStreet: string | null;

  readonly historicalStreetDetail:
    | StreetDetailCorner
    | StreetDetailNumber
    | null;

  readonly historicalPlaceId: string | null;

  readonly initiativeHTML: string | null;

  readonly patronageHTML: string | null;

  readonly dataGatheringHTML: string | null;

  readonly fundingHTML: string | null;
}
