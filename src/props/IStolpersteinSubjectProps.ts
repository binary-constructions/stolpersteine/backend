import { IContentProps } from "../internal";

export interface IStolpersteinSubjectProps<T extends string>
  extends IContentProps<T> {
  // none, really
}
