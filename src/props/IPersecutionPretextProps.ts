import { INodeProps, PersecutionPretextLocalization } from "../internal";

export interface IPersecutionPretextProps
  extends INodeProps<"PersecutionPretext"> {
  readonly localeCode: string;

  readonly label: string;

  readonly label_f: string | null;

  readonly label_m: string | null;

  readonly localizations: PersecutionPretextLocalization[];
}
