import { IEntityProps } from "../internal";

export interface INodeProps<T extends string> extends IEntityProps<T> {
  /**
   * If undefined a random UUID will be automatically assigned.
   */
  readonly id?: string;
}
