import { INodeProps, ReviewStatus } from "../internal";

export interface IContentProps<T extends string> extends INodeProps<T> {
  /**
   * If undefined this field will be set to the current time, while
   * passing in null will define the creation time as unknown.
   */
  readonly created?: number | null;

  /**
   * Defaults to null.
   */
  readonly lastModified?: number | null;

  /**
   * Defaults to null.
   */
  readonly deleted?: number | null;

  /**
   * Defaults to ReviewStatus.UNDEFINED.
   */
  readonly reviewStatus?: ReviewStatus;
}
