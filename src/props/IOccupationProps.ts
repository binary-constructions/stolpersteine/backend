import { INodeProps, OccupationLocalization } from "../internal";

export interface IOccupationProps extends INodeProps<"Occupation"> {
  readonly localeCode: string;

  readonly label: string;

  readonly label_f: string | null;

  readonly label_m: string | null;

  readonly localizations: OccupationLocalization[];
}
