import { INodeProps, RelationshipTypeLocalization } from "../internal";

export interface IRelationshipTypeProps extends INodeProps<"RelationshipType"> {
  readonly localeCode: string;

  readonly subjectLabel: string;

  readonly subjectLabel_f: string | null;

  readonly subjectLabel_m: string | null;

  objectLabel: string;

  readonly objectLabel_f: string | null;

  readonly objectLabel_m: string | null;

  readonly localizations: RelationshipTypeLocalization[];
}
