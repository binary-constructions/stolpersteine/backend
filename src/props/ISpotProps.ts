import {
  IContentProps,
  GeoPoint,
  StreetDetailCorner,
  StreetDetailNumber,
} from "../internal";

export interface ISpotProps extends IContentProps<"Spot"> {
  readonly point: GeoPoint;

  readonly street: string | null;

  readonly streetDetail: StreetDetailCorner | StreetDetailNumber | null;

  readonly postalCode: string | null;

  readonly placeId: string | null;

  readonly note: string | null;

  readonly imageIds: string[];
}
