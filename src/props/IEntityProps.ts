import { IProps } from "../internal";

export interface IEntityProps<T extends string> extends IProps {
  /**
   * Optional typename to set as an additional safety measure against
   * type mixups.
   */
  readonly typename?: T;
}
