import { GraphQLScalarType, Kind } from "graphql";

import { HistoricalDate } from "../internal";

const HistoricalDateScalar = new GraphQLScalarType({
  name: "HistoricalDate",
  description:
    'A date that may be exact to the day ("YYYY-MM-DD"), less specific ("YYYY-MM" or "YYYY", meaning "sometime in") or a time-frame ("<earliest>/<latest>", meaning "in-between").  There may also be a prefix of "~", "?" or "~?" to mark the whole expression as an approximation, a presumption or an approximate presumption respectively.  (Note: in the future ISO-style times and time-zones might also be allowed, so that should be taken into account when parsing.)',
  parseValue: (value: string) => new HistoricalDate(value),
  parseLiteral: (ast) =>
    ast.kind === Kind.STRING ? new HistoricalDate(ast.value) : null,
  serialize: (value: HistoricalDate) => value.serialize(),
});

export { HistoricalDateScalar };
