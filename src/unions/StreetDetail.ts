import { createUnionType } from "type-graphql";
import { StreetDetailCorner, StreetDetailNumber } from "../internal";

export const StreetDetail = createUnionType({
  name: "StreetDetail",

  types: () => [StreetDetailCorner, StreetDetailNumber],
});
