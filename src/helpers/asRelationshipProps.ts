import { IDictionary, getStringFromDictionary } from "../../lib/dictionary";

import { IRelationshipProps, asNodeProps, Relationship } from "../internal";

/**
 * Turn the provided `dict` into `IRelationshipProps`, verifying
 * that all the properties that are not undefined are of the correct
 * type and no required properties are missing.  The returned object
 * also always has the `typename` property set.
 *
 * Property values will not be validated beyond type, though.
 *
 * Throws an error if `dict` is not actually compatible with
 * `IRelationshipProps`.
 */
export const asRelationshipProps = (dict: IDictionary): IRelationshipProps => ({
  ...asNodeProps(dict, Relationship.typename),
  subjectId: getStringFromDictionary(dict, "subjectId"),
  objectId: getStringFromDictionary(dict, "objectId"),
  typeId: getStringFromDictionary(dict, "typeId"),
});
