import {
  getStringFromDictionary,
  getStringOrNullFromDictionary,
  IDictionary,
} from "../../lib/dictionary";

import { PersecutionPretextLocalization } from "../internal";

export const asPersecutionPretextLocalization = (
  dict: IDictionary
): PersecutionPretextLocalization =>
  new PersecutionPretextLocalization({
    localeCode: getStringFromDictionary(dict, "localeCode"),
    label: getStringFromDictionary(dict, "label"),
    label_f: getStringOrNullFromDictionary(dict, "label_f"),
    label_m: getStringOrNullFromDictionary(dict, "label_m"),
  });
