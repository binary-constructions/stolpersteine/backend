import {
  getStringFromDictionaryIfDefined,
  IDictionary,
} from "../../lib/dictionary";

import { IEntityProps } from "../internal";

/**
 * Turn the provided `something` into `IEntityProps`, verifying that
 * all the properties that are not undefined are of the correct type
 * and no required properties are missing.
 *
 * Also checks whether the typename property matches the provided one,
 * sets it as such if undefined and throws an Error if it differs.
 *
 * Property values will not be validated beyond type, though.
 *
 * Throws an error if `dict` is not actually compatible with
 * `IEntityProps`.
 */
export const asEntityProps = <T extends string>(
  dict: IDictionary,
  typename: T
): IEntityProps<T> => {
  const value = getStringFromDictionaryIfDefined(dict, "typename");

  if (value !== undefined) {
    if (typeof value !== "string") {
      throw new Error(
        "A typename property must be of type string if it exists."
      );
    } else if (value !== typename) {
      throw new Error(
        `The typename property needs to have a value of "${typename}".`
      );
    }
  }

  return {
    typename,
  };
};
