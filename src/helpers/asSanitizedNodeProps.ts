import * as uuid from "uuid/v4";

import {
  Node,
  INodeProps,
  ISanitizedProps,
  asSanitizedEntityProps,
} from "../internal";

export const asSanitizedNodeProps = <T extends string>(
  origin: Node<T, any> | null,
  props: INodeProps<T>
): INodeProps<T> & ISanitizedProps => {
  if (origin) {
    if (props.id && props.id !== origin.id) {
      throw new Error(
        "Id in update props differs from the one of the updated node."
      );
    }
  }

  return {
    ...asSanitizedEntityProps(origin, props),
    id: props.id || uuid(),
  };
};
