import { basename } from "path";

export const asValidatedFileName = (value: unknown): string => {
  if (!value || typeof value !== "string") {
    throw new Error("File name must be a (non-empty) string.");
  }

  const fileName = basename(value).trim();

  if (!fileName) {
    throw new Error("File name is empty.");
  } else if (fileName !== value) {
    throw new Error("Not a valid file name: " + value);
  }

  return fileName;
};
