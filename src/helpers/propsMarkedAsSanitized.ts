import { IProps, ISanitizedProps } from "../internal";

const saneFunc: () => true = () => true;

export const propsMarkedAsSanitized = <T extends IProps>(
  props: T
): T & ISanitizedProps => ({
  ...props,
  __sanitized: saneFunc,
});
