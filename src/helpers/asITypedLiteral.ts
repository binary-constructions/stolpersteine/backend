import { IDictionary, getStringFromDictionary } from "../../lib/dictionary";

interface ITypedLiteral {
  typename: string;

  value: string;
}

export const asITypedLiteral = (dict: IDictionary): ITypedLiteral => ({
  typename: getStringFromDictionary(dict, "typename"),
  value: getStringFromDictionary(dict, "value"),
});
