import {
  getTokenFromDictionaryIfDefined,
  getNumberOrNullFromDictionaryIfDefined,
  IDictionary,
} from "../../lib/dictionary";

import { IContentProps, asNodeProps } from "../internal";
import { ReviewStatus } from "../enums/ReviewStatus";

/**
 * Turn the provided `something` into `IContentProps`, verifying that
 * all the properties that are not undefined are of the correct type
 * and no required properties are missing.
 *
 * Also checks whether the typename property matches the provided one,
 * sets it as such if undefined and throws an Error if it differs.
 *
 * Property values will not be validated beyond type, though.
 *
 * Throws an error if `dict` is not actually compatible with
 * `IContentProps`.
 */
export const asContentProps = <T extends string>(
  dict: IDictionary,
  typename: T
): IContentProps<T> => ({
  ...asNodeProps(dict, typename),
  created: getNumberOrNullFromDictionaryIfDefined(dict, "created"),
  lastModified: getNumberOrNullFromDictionaryIfDefined(dict, "lastModified"),
  deleted: getNumberOrNullFromDictionaryIfDefined(dict, "deleted"),
  reviewStatus: getTokenFromDictionaryIfDefined(
    dict,
    "reviewStatus",
    ReviewStatus
  ),
});
