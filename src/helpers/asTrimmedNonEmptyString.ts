export const asTrimmedNonEmptyString = (value: string): string => {
  const trimmedValue = value.trim();

  if (!trimmedValue) {
    throw new Error("String is empty or contains only whitespace.");
  }

  return trimmedValue;
};
