export const asId = (value: string): string => {
  if (!/^[-0-9a-z]+$/i.test(value)) {
    throw new Error("Not a valid UUID: " + value);
  }

  return value;
};
