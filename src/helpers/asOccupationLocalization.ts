import {
  getStringFromDictionary,
  getStringOrNullFromDictionary,
  IDictionary,
} from "../../lib/dictionary";

import { OccupationLocalization } from "../internal";

export const asOccupationLocalization = (
  dict: IDictionary
): OccupationLocalization =>
  new OccupationLocalization({
    localeCode: getStringFromDictionary(dict, "localeCode"),
    label: getStringFromDictionary(dict, "label"),
    label_f: getStringOrNullFromDictionary(dict, "label_f"),
    label_m: getStringOrNullFromDictionary(dict, "label_m"),
  });
