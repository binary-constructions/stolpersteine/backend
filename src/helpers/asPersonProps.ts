import {
  IDictionary,
  getObjectOrNullFromDictionary,
  getStringOrNullFromDictionary,
  getStringOrNullFromDictionaryIfDefined,
  getStringArrayFromDictionary,
  getStringArrayFromDictionaryIfDefined,
  getObjectArrayFromDictionary,
  getTokenFromDictionary,
} from "../../lib/dictionary";

import {
  IPersonProps,
  asStolpersteinSubjectProps,
  Person,
  Gender,
  HistoricalDate,
  asPersonNameInterface,
  asPersonTitle,
  Fate,
} from "../internal";

/**
 * Turn the provided `something` into `IPersonProps`, verifying
 * that all the properties that are not undefined are of the correct
 * type and no required properties are missing.  The returned object
 * also always has the `typename` property set.
 *
 * Property values will not be validated beyond type, though.
 *
 * Throws an error if `dict` is not actually compatible with
 * `IPersonProps`.
 */
export const asPersonProps = (dict: IDictionary): IPersonProps => ({
  ...asStolpersteinSubjectProps(dict, Person.typename),
  names: getObjectArrayFromDictionary(dict, "names", (v) =>
    asPersonNameInterface(v)
  ),
  occupationIds: getStringArrayFromDictionary(dict, "occupationIds"),
  title: getObjectOrNullFromDictionary(dict, "title", asPersonTitle),
  gender: getTokenFromDictionary(dict, "gender", Gender),
  bornDate:
    ((c) => c?.trim() && new HistoricalDate(c))(
      getStringOrNullFromDictionary(dict, "bornDate")
    ) || null,
  bornPlaceId: getStringOrNullFromDictionary(dict, "bornPlaceId"),
  diedDate:
    ((c) => c?.trim() && new HistoricalDate(c))(
      getStringOrNullFromDictionary(dict, "diedDate")
    ) || null,
  diedPlaceId: getStringOrNullFromDictionary(dict, "diedPlaceId"),
  persecutionPretextIds: getStringArrayFromDictionary(
    dict,
    "persecutionPretextIds"
  ),
  fate: getTokenFromDictionary(dict, "fate", Fate, Fate.UNKNOWN),
  escapedToPlaceId:
    getStringOrNullFromDictionaryIfDefined(dict, "escapedToPlaceId") || null,
  imageIds: getStringArrayFromDictionaryIfDefined(dict, "imageIds") || [],
  biographyHTML: getStringOrNullFromDictionary(dict, "biographyHTML"),
  editingNotesHTML: getStringOrNullFromDictionary(dict, "editingNotesHTML"),
});
