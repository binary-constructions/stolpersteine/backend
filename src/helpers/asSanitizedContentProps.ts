import {
  Content,
  IContentProps,
  ISanitizedProps,
  asSanitizedNodeProps,
  ReviewStatus,
} from "../internal";
import {} from "../enums/ReviewStatus";

export const asSanitizedContentProps = <T extends string>(
  origin: Content<T, any> | null,
  props: IContentProps<T>
): IContentProps<T> & ISanitizedProps => {
  return origin
    ? {
        ...asSanitizedNodeProps(origin, props),
        created: props.created !== undefined ? props.created : origin.created,
        lastModified: Date.now(),
        deleted: props !== undefined ? props.deleted : origin.deleted,
        reviewStatus:
          props.reviewStatus !== undefined
            ? props.reviewStatus
            : origin.reviewStatus,
      }
    : {
        ...asSanitizedNodeProps(origin, props),
        created: props.created !== undefined ? props.created : Date.now(),
        lastModified: props.lastModified || null,
        deleted: props.deleted || null,
        reviewStatus: props.reviewStatus || ReviewStatus.UNDEFINED,
      };
};
