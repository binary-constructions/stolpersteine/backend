import {
  Entity,
  IEntityProps,
  ISanitizedProps,
  propsMarkedAsSanitized,
} from "../internal";

export const asSanitizedEntityProps = <T extends string>(
  origin: Entity<T, any> | null,
  props: IEntityProps<T>
): IEntityProps<T> & ISanitizedProps => {
  if (origin) {
    if (props.typename && props.typename !== origin.typename) {
      throw new Error(
        "Typename of update props differs from the one of the updated entity."
      );
    }
  }

  return propsMarkedAsSanitized(props);
};
