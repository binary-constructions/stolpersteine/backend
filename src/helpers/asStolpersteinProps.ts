import {
  IDictionary,
  getObjectOrNullFromDictionary,
  getStringOrNullFromDictionary,
  getStringArrayFromDictionary,
} from "../../lib/dictionary";

import {
  IStolpersteinProps,
  asContentProps,
  Stolperstein,
  HistoricalDate,
  asStreetDetail,
} from "../internal";
import {} from "../types/HistoricalDate";

/**
 * Turn the provided `something` into `IStolpersteinProps`, verifying
 * that all the properties that are not undefined are of the correct
 * type and no required properties are missing.  The returned object
 * also always has the `typename` property set.
 *
 * Property values will not be validated beyond type, though.
 *
 * Throws an error if `dict` is not actually compatible with
 * `IStolpersteinProps`.
 */
export const asStolpersteinProps = (dict: IDictionary): IStolpersteinProps => ({
  ...asContentProps(dict, Stolperstein.typename),
  spotId: getStringOrNullFromDictionary(dict, "spotId"),
  laid: ((c) => (c === null ? null : new HistoricalDate(c)))(
    getStringOrNullFromDictionary(dict, "laid")
  ),
  inscription: getStringOrNullFromDictionary(dict, "inscription"),
  imageId: getStringOrNullFromDictionary(dict, "imageId"),
  subjectIds: getStringArrayFromDictionary(dict, "subjectIds"),
  historicalStreet: getStringOrNullFromDictionary(dict, "historicalStreet"),
  historicalStreetDetail: getObjectOrNullFromDictionary(
    dict,
    "historicalStreetDetail",
    asStreetDetail
  ),
  historicalPlaceId: getStringOrNullFromDictionary(dict, "historicalPlaceId"),
  initiativeHTML: getStringOrNullFromDictionary(dict, "initiativeHTML"),
  patronageHTML: getStringOrNullFromDictionary(dict, "patronageHTML"),
  dataGatheringHTML: getStringOrNullFromDictionary(dict, "dataGatheringHTML"),
  fundingHTML: getStringOrNullFromDictionary(dict, "fundingHTML"),
});
