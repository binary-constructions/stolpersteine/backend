import { IDictionary, getNumberFromDictionary } from "../../lib/dictionary";

import { GeoPoint } from "../internal";

export const asGeoPoint = (dict: IDictionary): GeoPoint =>
  new GeoPoint({
    longitude: getNumberFromDictionary(dict, "longitude"),
    latitude: getNumberFromDictionary(dict, "latitude"),
  });
