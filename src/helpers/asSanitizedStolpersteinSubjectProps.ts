import {
  StolpersteinSubject,
  IStolpersteinSubjectProps,
  ISanitizedProps,
  asSanitizedContentProps,
} from "../internal";

export const asSanitizedStolpersteinSubjectProps = <T extends string>(
  origin: StolpersteinSubject<T, any> | null,
  props: IStolpersteinSubjectProps<T>
): IStolpersteinSubjectProps<T> & ISanitizedProps =>
  asSanitizedContentProps(origin, props);
