export const asLocaleCode = (value: string): string => {
  const trimmedValue = value.trim();

  if (!/^[a-z][a-z](-[A-Z][A-Z])?$/.test(trimmedValue)) {
    throw new Error('Locale codes must be of the form "xx" or "xx-XX".');
  }

  return trimmedValue;
};
