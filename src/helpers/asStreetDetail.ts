import { IDictionary, getStringFromDictionary } from "../../lib/dictionary";

import { StreetDetailCorner, StreetDetailNumber } from "../internal";

export const asStreetDetail = (
  dict: IDictionary
): StreetDetailCorner | StreetDetailNumber => {
  const typename = getStringFromDictionary(dict, "typename");

  const value = getStringFromDictionary(dict, "value");

  if (typename === "StreetDetailCorner") {
    return new StreetDetailCorner(value);
  } else if (typename === "StreetDetailNumber") {
    return new StreetDetailNumber(value);
  } else {
    throw new Error("Unknown type for street detail: " + typename);
  }
};
