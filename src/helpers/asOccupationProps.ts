import {
  getObjectArrayFromDictionary,
  getStringOrNullFromDictionary,
  getStringFromDictionary,
  IDictionary,
} from "../../lib/dictionary";

import {
  IOccupationProps,
  asNodeProps,
  asOccupationLocalization,
  Occupation,
} from "../internal";

/**
 * Turn the provided `something` into `IOccupationProps`, verifying
 * that all the properties that are not undefined are of the correct
 * type and no required properties are missing.  The returned object
 * also always has the `typename` property set.
 *
 * Property values will not be validated beyond type, though.
 *
 * Throws an error if `dict` is not actually compatible with
 * `IOccupationProps`.
 */
export const asOccupationProps = (dict: IDictionary): IOccupationProps => ({
  ...asNodeProps(dict, Occupation.typename),
  localeCode: getStringFromDictionary(dict, "localeCode"),
  label: getStringFromDictionary(dict, "label"),
  label_f: getStringOrNullFromDictionary(dict, "label_f"),
  label_m: getStringOrNullFromDictionary(dict, "label_m"),
  localizations: getObjectArrayFromDictionary(
    dict,
    "localizations",
    asOccupationLocalization
  ),
});
