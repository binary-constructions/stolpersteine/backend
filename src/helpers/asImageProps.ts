import {
  getStringOrNullFromDictionary,
  getStringFromDictionary,
  getStringFromDictionaryIfDefined,
  IDictionary,
} from "../../lib/dictionary";

import { IImageProps, asNodeProps, Image } from "../internal";

/**
 * Turn the provided `something` into `IImageProps`, verifying that
 * all the properties that are not undefined are of the correct type
 * and no required properties are missing.  The returned object also
 * always has the `typename` property set.
 *
 * Property values will not be validated beyond type, though.
 *
 * Throws an error if `dict` is not actually compatible with
 * `IImageProps`.
 */
export const asImageProps = (dict: IDictionary): IImageProps => ({
  ...asNodeProps(dict, Image.typename),
  authorshipRemark: getStringFromDictionary(dict, "authorshipRemark"),
  caption: getStringOrNullFromDictionary(dict, "caption"),
  alternativeText: getStringOrNullFromDictionary(dict, "alternativeText"),
  data: getStringFromDictionaryIfDefined(dict, "data"),
  fileName: getStringFromDictionaryIfDefined(dict, "fileName"),
});
