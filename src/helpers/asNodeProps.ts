import {
  getStringFromDictionaryIfDefined,
  IDictionary,
} from "../../lib/dictionary";

import { INodeProps, asEntityProps } from "../internal";

/**
 * Turn the provided `something` into `INodeProps`, verifying that all
 * the properties that are not undefined are of the correct type and
 * no required properties are missing.
 *
 * Also checks whether the typename property matches the provided one,
 * sets it as such if undefined and throws an Error if it differs.
 *
 * Property values will not be validated beyond type, though.
 *
 * Throws an error if `dict` is not actually compatible with
 * `INodeProps`.
 */
export const asNodeProps = <T extends string>(
  dict: IDictionary,
  typename: T
): INodeProps<T> => ({
  ...asEntityProps(dict, typename),
  id: getStringFromDictionaryIfDefined(dict, "id"),
});
