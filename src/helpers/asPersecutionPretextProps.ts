import {
  IDictionary,
  getStringFromDictionary,
  getStringOrNullFromDictionary,
  getObjectArrayFromDictionary,
} from "../../lib/dictionary";

import {
  IPersecutionPretextProps,
  asNodeProps,
  PersecutionPretext,
  asPersecutionPretextLocalization,
} from "../internal";

/**
 * Turn the provided `dict` into `IPersecutionPretextProps`, verifying
 * that all the properties that are not undefined are of the correct
 * type and no required properties are missing.  The returned object
 * also always has the `typename` property set.
 *
 * Property values will not be validated beyond type, though.
 *
 * Throws an error if `dict` is not actually compatible with
 * `IPersecutionPretextProps`.
 */
export const asPersecutionPretextProps = (
  dict: IDictionary
): IPersecutionPretextProps => ({
  ...asNodeProps(dict, PersecutionPretext.typename),
  localeCode: getStringFromDictionary(dict, "localeCode"),
  label: getStringFromDictionary(dict, "label"),
  label_f: getStringOrNullFromDictionary(dict, "label_f"),
  label_m: getStringOrNullFromDictionary(dict, "label_m"),
  localizations: getObjectArrayFromDictionary(
    dict,
    "localizations",
    asPersecutionPretextLocalization
  ),
});
