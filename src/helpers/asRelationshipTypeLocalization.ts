import {
  getStringFromDictionary,
  getStringOrNullFromDictionary,
  IDictionary,
} from "../../lib/dictionary";

import { RelationshipTypeLocalization } from "../internal";

export const asRelationshipTypeLocalization = (
  dict: IDictionary
): RelationshipTypeLocalization =>
  new RelationshipTypeLocalization({
    localeCode: getStringFromDictionary(dict, "localeCode"),
    subjectLabel: getStringFromDictionary(dict, "subjectLabel"),
    subjectLabel_f: getStringOrNullFromDictionary(dict, "subjectLabel_f"),
    subjectLabel_m: getStringOrNullFromDictionary(dict, "subjectLabel_m"),
    objectLabel: getStringFromDictionary(dict, "objectLabel"),
    objectLabel_f: getStringOrNullFromDictionary(dict, "objectLabel_f"),
    objectLabel_m: getStringOrNullFromDictionary(dict, "objectLabel_m"),
  });
