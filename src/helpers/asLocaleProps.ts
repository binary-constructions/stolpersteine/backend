import { getStringFromDictionary, IDictionary } from "../../lib/dictionary";

import { ILocaleProps, asEntityProps, Locale } from "../internal";

/**
 * Turn the provided `something` into `ILocaleProps`, verifying that
 * all the properties that are not undefined are of the correct type
 * and no required properties are missing.  The returned object also
 * always has the `typename` property set.
 *
 * Property values will not be validated beyond type, though.
 *
 * Throws an error if `dict` is not actually compatible with
 * `ILocaleProps`.
 */
export const asLocaleProps = (dict: IDictionary): ILocaleProps => ({
  ...asEntityProps(dict, Locale.typename),
  code: getStringFromDictionary(dict, "code"),
  label: getStringFromDictionary(dict, "label"),
});
