import {
  IDictionary,
  getStringFromDictionary,
  getStringOrNullFromDictionary,
  getObjectArrayFromDictionary,
} from "../../lib/dictionary";

import {
  IRelationshipTypeProps,
  asNodeProps,
  RelationshipType,
  asRelationshipTypeLocalization,
} from "../internal";

/**
 * Turn the provided `dict` into `IRelationshipTypeProps`, verifying
 * that all the properties that are not undefined are of the correct
 * type and no required properties are missing.  The returned object
 * also always has the `typename` property set.
 *
 * Property values will not be validated beyond type, though.
 *
 * Throws an error if `dict` is not actually compatible with
 * `IRelationshipTypeProps`.
 */
export const asRelationshipTypeProps = (
  dict: IDictionary
): IRelationshipTypeProps => ({
  ...asNodeProps(dict, RelationshipType.typename),
  localeCode: getStringFromDictionary(dict, "localeCode"),
  subjectLabel: getStringFromDictionary(dict, "subjectLabel"),
  subjectLabel_f: getStringOrNullFromDictionary(dict, "subjectLabel_f"),
  subjectLabel_m: getStringOrNullFromDictionary(dict, "subjectLabel_m"),
  objectLabel: getStringFromDictionary(dict, "objectLabel"),
  objectLabel_f: getStringOrNullFromDictionary(dict, "objectLabel_f"),
  objectLabel_m: getStringOrNullFromDictionary(dict, "objectLabel_m"),
  localizations: getObjectArrayFromDictionary(
    dict,
    "localizations",
    asRelationshipTypeLocalization
  ),
});
