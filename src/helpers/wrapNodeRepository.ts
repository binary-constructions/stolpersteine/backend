import {
  Repository,
  SelfKeyedEntityRepositoryWrapper,
} from "../../lib/repository";

import { Node } from "../internal";

export const wrapNodeRepository = <T extends Node<any, any>>(
  repository: Repository<T>
): SelfKeyedEntityRepositoryWrapper<T> =>
  new SelfKeyedEntityRepositoryWrapper(repository, (node) => node.id);
