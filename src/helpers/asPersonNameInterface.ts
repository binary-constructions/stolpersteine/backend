import { IDictionary } from "../../lib/dictionary";

import {
  PersonNameInterface,
  NameType,
  BirthName,
  FamilyName,
  GivenName,
  MarriedName,
  Nickname,
  Pseudonym,
} from "../internal";

export const asPersonNameInterface = (
  dict: IDictionary
): PersonNameInterface => {
  const typename = dict["type"] || dict["typename"];
  const name = dict["name"] || dict["value"];

  // FIXME: the PersonName related types are a little bit messed up!

  if (typeof typename !== "string") {
    throw new Error("Name input 'type(name)' property is not a string.");
  }

  if (typeof name !== "string") {
    throw new Error("Name input 'name|value' property is not a string.");
  }

  switch (typename) {
    case NameType.BIRTH:
      return new BirthName(name);
    case NameType.FAMILY:
      return new FamilyName(name);
    case NameType.GIVEN:
      return new GivenName(name);
    case NameType.MARRIED:
      return new MarriedName(name);
    case NameType.NICKNAME:
      return new Nickname(name);
    case NameType.PSEUDONYM:
      return new Pseudonym(name);
    default:
      throw new Error("Object is not typed as a known name type.");
  }
};
