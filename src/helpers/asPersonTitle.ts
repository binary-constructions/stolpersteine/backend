import { IDictionary } from "../../lib/dictionary";

import { PersonTitle, asITypedLiteral } from "../internal";

export const asPersonTitle = (dict: IDictionary): PersonTitle => {
  const typedLiteral = asITypedLiteral(dict);

  if (typedLiteral.typename !== "PersonTitle") {
    throw new Error('Object is not typed as "PersonTitle".');
  }

  return new PersonTitle(typedLiteral.value);
};
