import {
  IDictionary,
  getObjectFromDictionary,
  getObjectOrNullFromDictionary,
  getStringOrNullFromDictionary,
  getStringArrayFromDictionary,
} from "../../lib/dictionary";

import {
  ISpotProps,
  asContentProps,
  Spot,
  asGeoPoint,
  asStreetDetail,
} from "../internal";

/**
 * Turn the provided `something` into `ISpotProps`, verifying that
 * all the properties that are not undefined are of the correct type
 * and no required properties are missing.  The returned object also
 * always has the `typename` property set.
 *
 * Property values will not be validated beyond type, though.
 *
 * Throws an error if `dict` is not actually compatible with
 * `ISpotProps`.
 */
export const asSpotProps = (dict: IDictionary): ISpotProps => ({
  ...asContentProps(dict, Spot.typename),
  point: getObjectFromDictionary(dict, "point", asGeoPoint),
  street: getStringOrNullFromDictionary(dict, "street"),
  streetDetail: getObjectOrNullFromDictionary(
    dict,
    "streetDetail",
    asStreetDetail
  ),
  postalCode: getStringOrNullFromDictionary(dict, "postalCode"),
  placeId: getStringOrNullFromDictionary(dict, "placeId"),
  note: getStringOrNullFromDictionary(dict, "note"),
  imageIds: getStringArrayFromDictionary(dict, "imageIds"),
});
