import {
  IDictionary,
  getStringFromDictionary,
  getStringOrNullFromDictionary,
  getStringArrayFromDictionary,
} from "../../lib/dictionary";

import { IPlaceProps, asNodeProps, Place } from "../internal";

/**
 * Turn the provided `something` into `IPlaceProps`, verifying that
 * all the properties that are not undefined are of the correct type
 * and no required properties are missing.  The returned object also
 * always has the `typename` property set.
 *
 * Property values will not be validated beyond type, though.
 *
 * Throws an error if `dict` is not actually compatible with
 * `IPlaceProps`.
 */
export const asPlaceProps = (dict: IDictionary): IPlaceProps => ({
  ...asNodeProps(dict, Place.typename),
  label: getStringFromDictionary(dict, "label"),
  parentId: getStringOrNullFromDictionary(dict, "parentId"),
  alternativeNames: getStringArrayFromDictionary(dict, "alternativeNames"),
});
