import { Field, InputType } from "type-graphql";

import { NameType } from "../internal";

@InputType()
export class PersonNameInput {
  @Field()
  name!: string;

  @Field((_) => NameType, { name: "type" })
  typename!: NameType;
}
