import { Field, InputType } from "type-graphql";

@InputType()
export class ImageInput {
  @Field((_) => String, {
    nullable: true,
    description:
      "The base64 encoded image data (needs to be set for new images and only should be set for existing ones if the image data changes).",
  })
  data?: string = undefined;

  @Field((_) => String, {
    nullable: true,
    description:
      "The file name of the image (needs to be set for new images but is optional for existing ones).",
  })
  fileName?: string = undefined;

  @Field((_) => String, {
    description:
      "Information on the authorship of the image and its copyright.",
  })
  authorshipRemark!: string;

  @Field((_) => String, {
    nullable: true,
    description: "The caption text for the image.",
  })
  caption: string | null = null;

  @Field((_) => String, {
    nullable: true,
    description:
      "The text to use as alternative content for the image (e.g. when rendering for a braille display).",
  })
  alternativeText: string | null = null;
}
