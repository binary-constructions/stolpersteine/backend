import { Field, InputType } from "type-graphql";

import { OccupationLocalizationInput } from "../internal";

@InputType()
class OccupationInput {
  @Field()
  locale!: string;

  @Field()
  label!: string;

  @Field((_) => String, { nullable: true })
  label_f: string | null = null;

  @Field((_) => String, { nullable: true })
  label_m: string | null = null;

  @Field((_) => [OccupationLocalizationInput])
  localizations: OccupationLocalizationInput[] = [];
}

export { OccupationInput };
