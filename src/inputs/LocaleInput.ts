import { Field, InputType } from "type-graphql";

@InputType()
export class LocaleInput {
  @Field((_) => String, {
    description:
      'The code to use for the Locale (of the for "xx" or "xx-XX", e.g. "de" or "de-DE").',
  })
  code!: string;

  @Field((_) => String, { description: "The label to use for the Locale." })
  label!: string;
}
