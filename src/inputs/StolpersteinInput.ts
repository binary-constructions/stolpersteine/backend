import { Field, InputType } from "type-graphql";

import { HistoricalDate, HistoricalDateScalar } from "../internal";

@InputType()
class StolpersteinInput {
  @Field((_) => String, {
    nullable: true,
    description: "The ID of the Spot of the Stolperstein.",
  })
  spotId: string | null = null;

  @Field((_) => HistoricalDateScalar, {
    nullable: true,
    description: "The date when the Stolperstein was laid.",
  })
  laid: HistoricalDate | null = null;

  @Field((_) => String, {
    nullable: true,
    description: "The text inscribed on the Stolperstein (usually multi-line).",
  })
  inscription: string | null = null;

  @Field((_) => String, {
    nullable: true,
    description: "The id of an image object.",
  })
  imageId: string | null = null;

  @Field((_) => [String], {
    description: "A list of the IDs of the subjects of this Stolperstein.",
  })
  subjectIds: string[] = [];

  @Field((_) => String, { nullable: true })
  historicalStreet: string | null = null;

  @Field((_) => String, { nullable: true })
  historicalCorner: string | null = null;

  @Field((_) => String, { nullable: true })
  historicalNumber: string | null = null;

  @Field((_) => String, { nullable: true })
  historicalPlaceId: string | null = null;

  @Field((_) => String, {
    nullable: true,
    description:
      "Information about people/groups that initiated the laying of this Stolperstein.",
  })
  initiativeHTML: string | null = null;

  @Field((_) => String, {
    nullable: true,
    description:
      "Information about people/groups that helped finance the laying of this Stolperstein.",
  })
  patronageHTML: string | null = null;

  @Field((_) => String, {
    nullable: true,
    description:
      "Information about people/groups that helped gather information about the subject of the Stolperstein.",
  })
  dataGatheringHTML: string | null = null;

  @Field((_) => String, {
    nullable: true,
    description:
      "Information about people/groups that funded this Stolperstein.",
  })
  fundingHTML: string | null = null;
}

export { StolpersteinInput };
