import { Field, InputType } from "type-graphql";

import {
  Gender,
  Fate,
  HistoricalDate,
  HistoricalDateScalar,
  PersonNameInput,
} from "../internal";

@InputType()
class PersonInput {
  @Field((_) => [PersonNameInput], {
    nullable: false,
    description:
      "A list of the names of the person in display order.  Must contain at least one name.",
  })
  names!: PersonNameInput[];

  @Field((_) => [String], {
    nullable: true,
    description: "A list of the IDs of the occupations of the person.",
  })
  occupationIds: string[] = [];

  @Field((_) => [String], {
    nullable: true,
    description:
      "A list of the IDs of the pretexts used in persecution of the person.",
  })
  persecutionPretextIds: string[] = [];

  @Field((_) => String, { nullable: true })
  title: string | null = null;

  @Field((_) => Gender, { nullable: true })
  gender: Gender = Gender.UNSPECIFIED;

  @Field((_) => HistoricalDateScalar, {
    nullable: true,
    description: "The date the person was born.",
  })
  bornDate: HistoricalDate | null = null;

  @Field((_) => String, {
    nullable: true,
    description: "The ID of the place that the person was born at.",
  })
  bornPlaceId: string | null = null;

  @Field((_) => HistoricalDateScalar, {
    nullable: true,
    description: "The date the person died.",
  })
  diedDate: HistoricalDate | null = null;

  @Field((_) => String, {
    nullable: true,
    description: "The ID of the place that the person died at.",
  })
  diedPlaceId: string | null = null;

  @Field((_) => [String], { nullable: true })
  imageIds: string[] = [];

  @Field((_) => Fate, { nullable: true })
  fate: Fate = Fate.UNKNOWN;

  @Field((_) => String, {
    nullable: true,
  })
  escapedToPlaceId: string | null = null;

  @Field((_) => String, {
    nullable: true,
    description: "The HTML-formatted biography text for the person.",
  })
  biographyHTML: string | null = null;

  @Field((_) => String, {
    nullable: true,
    description: "HTML-formatted internal notes.",
  })
  editingNotesHTML: string | null = null;
}

export { PersonInput };
