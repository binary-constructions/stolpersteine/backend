import { Field, InputType } from "type-graphql";

import { PersecutionPretextLocalizationInput } from "../internal";

@InputType()
class PersecutionPretextInput {
  @Field()
  locale!: string; // TODO (future): -> localeCode

  @Field()
  label!: string;

  @Field((_) => String, { nullable: true })
  label_f: string | null = null;

  @Field((_) => String, { nullable: true })
  label_m: string | null = null;

  @Field((_) => [PersecutionPretextLocalizationInput])
  localizations: PersecutionPretextLocalizationInput[] = [];
}

export { PersecutionPretextInput };
