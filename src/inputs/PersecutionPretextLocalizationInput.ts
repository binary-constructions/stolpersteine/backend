import { Field, InputType } from "type-graphql";

@InputType()
class PersecutionPretextLocalizationInput {
  @Field()
  locale!: string;

  @Field()
  label!: string;

  @Field((_) => String, { nullable: true })
  label_f: string | null = null;

  @Field((_) => String, { nullable: true })
  label_m: string | null = null;
}

export { PersecutionPretextLocalizationInput };
