import { Field, InputType } from "type-graphql";

import { RelationshipTypeLocalizationInput } from "../internal";

@InputType()
export class RelationshipTypeInput {
  @Field()
  locale!: string;

  @Field()
  subjectLabel!: string;

  @Field((_) => String, { nullable: true })
  subjectLabel_f: string | null = null;

  @Field((_) => String, { nullable: true })
  subjectLabel_m: string | null = null;

  @Field()
  objectLabel!: string;

  @Field((_) => String, { nullable: true })
  objectLabel_f: string | null = null;

  @Field((_) => String, { nullable: true })
  objectLabel_m: string | null = null;

  @Field((_) => [RelationshipTypeLocalizationInput])
  localizations: RelationshipTypeLocalizationInput[] = [];
}
