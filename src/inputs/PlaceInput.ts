import { Field, InputType } from "type-graphql";

import { IPlaceProps } from "../internal";

@InputType()
export class PlaceInput implements IPlaceProps {
  @Field((_) => String)
  label!: string;

  @Field((_) => String, {
    nullable: true,
    description: "The ID of a place containing this one.",
  })
  parentId: string | null = null;

  @Field((_) => [String], {
    description: "The list of alternative names this place is known by.",
  })
  alternativeNames: string[] = [];
}
