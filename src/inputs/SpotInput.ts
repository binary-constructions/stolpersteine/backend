import { Field, InputType, Float } from "type-graphql";

@InputType()
export class SpotInput {
  @Field((_) => Float)
  longitude!: number;

  @Field((_) => Float)
  latitude!: number;

  @Field((_) => String, { nullable: true })
  street: string | null = null;

  @Field((_) => String, { nullable: true })
  corner: string | null = null;

  @Field((_) => String, { nullable: true })
  number: string | null = null;

  @Field((_) => String, { nullable: true })
  postalCode: string | null = null;

  @Field((_) => String, { nullable: true })
  placeId: string | null = null;

  @Field((_) => String, { nullable: true })
  note: string | null = null;

  @Field((_) => [String], { nullable: true })
  imageIds: string[] = [];
}
