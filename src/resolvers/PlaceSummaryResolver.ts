import { Query, Resolver } from "type-graphql";

import { PlaceSummary } from "../internal";

/**
 * The resolver for `SpotSummary` objects.
 */
@Resolver((_) => PlaceSummary)
export class PlaceSummaryResolver {
  @Query((_) => [PlaceSummary], {
    deprecationReason:
      "Summary queries were only introduced as a hack and will probably be removed again in the future.",
  })
  placeSummaries(): Promise<PlaceSummary[]> {
    return PlaceSummary.find();
  }
}
