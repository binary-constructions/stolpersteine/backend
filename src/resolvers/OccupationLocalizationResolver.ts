import { Resolver, FieldResolver, Root } from "type-graphql";

import { OccupationLocalization, Locale } from "../internal";

/**
 * The resolver for `Locale` objects.
 */
@Resolver((_) => OccupationLocalization)
export class OccupationLocalizationResolver {
  @FieldResolver((_) => Locale)
  async locale(
    @Root() occupationLocalization: OccupationLocalization
  ): Promise<Locale> {
    const locale = await Locale.getRepository().findOne(
      occupationLocalization.localeCode
    );

    if (!locale) {
      throw new Error(
        `Unable to resolve locale code '${occupationLocalization.localeCode}'.`
      );
    }

    return locale;
  }
}
