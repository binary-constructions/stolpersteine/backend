import { Arg, Mutation, Resolver, FieldResolver, Root } from "type-graphql";

import {
  Stolperstein,
  StolpersteinInput,
  Place,
  Spot,
  Image,
  StreetDetailCorner,
  StreetDetailNumber,
  ReviewStatus,
  ContentResolver,
  StolpersteinSubject,
  stolpersteinSubjectRepository,
} from "../internal";

/**
 * The resolver for `Stolperstein` objects.
 */
@Resolver((_) => Stolperstein)
export class StolpersteinResolver extends ContentResolver(Stolperstein, {
  repository: Stolperstein.getRepository(),
  typename: Stolperstein.typename,
  singularQueryName: "stolperstein",
  multiplesQueryName: "stolpersteine",
}) {
  @FieldResolver((_) => Spot, { nullable: true })
  async spot(@Root() stolperstein: Stolperstein): Promise<Spot | null> {
    return stolperstein.spotId
      ? Spot.getRepository().findOne(stolperstein.spotId)
      : null;
  }

  @FieldResolver((_) => Image, { nullable: true })
  async image(@Root() stolperstein: Stolperstein): Promise<Image | null> {
    return stolperstein.imageId
      ? Image.getRepository().findOne(stolperstein.imageId)
      : null;
  }

  @FieldResolver((_) => [StolpersteinSubject])
  async subjects(
    @Root() stolperstein: Stolperstein
  ): Promise<StolpersteinSubject<any, any>[] | null> {
    return stolperstein.subjectIds.length
      ? (await stolpersteinSubjectRepository.find()).filter((v) =>
          stolperstein.subjectIds.includes(v.id)
        )
      : [];
  }

  @FieldResolver((_) => Place, { nullable: true })
  async historicalPlace(
    @Root() stolperstein: Stolperstein
  ): Promise<Place | null> {
    return stolperstein.historicalPlaceId
      ? Place.getRepository().findOne(stolperstein.historicalPlaceId)
      : null;
  }

  @Mutation((_) => Stolperstein, {
    description: "Create a new Stolperstein object.",
  })
  async createStolperstein(
    @Arg("input") input: StolpersteinInput
  ): Promise<Stolperstein> {
    const { historicalCorner, historicalNumber } = input;

    const historicalStreetDetail = historicalCorner
      ? new StreetDetailCorner(historicalCorner)
      : historicalNumber
      ? new StreetDetailNumber(historicalNumber)
      : null;

    const stolperstein = await Stolperstein.create({
      ...input,
      historicalStreetDetail,
    });

    if (!stolperstein) {
      throw new Error("Failed to create stolperstein.");
    }

    return stolperstein;
  }

  @Mutation((_) => Stolperstein, {
    description: "Update an existing Image object.",
  })
  async updateStolperstein(
    @Arg("id") id: string,
    @Arg("input") input: StolpersteinInput
  ): Promise<Stolperstein> {
    const { historicalCorner, historicalNumber } = input;

    const historicalStreetDetail = historicalCorner
      ? new StreetDetailCorner(historicalCorner)
      : historicalNumber
      ? new StreetDetailNumber(historicalNumber)
      : null;

    const updated = await Stolperstein.update({
      ...input,
      id,
      historicalStreetDetail,
    });

    if (!updated) {
      throw new Error("No Stolperstein object with the specified id found.");
    }

    return updated;
  }

  @Mutation((_) => Stolperstein, {
    description:
      "Change the ReviewStatus of the Stolperstein with the specified id.",
  })
  async setStolpersteinReviewStatus(
    @Arg("id") id: string,
    @Arg("status", (_) => ReviewStatus) status: ReviewStatus
  ): Promise<Stolperstein> {
    const current = await Stolperstein.getRepository().findOne(id);

    if (!current) {
      throw new Error("No Stolperstein object with the specified id found.");
    }

    const updated = await Stolperstein.update({
      ...current,
      reviewStatus: status,
    });

    if (!updated) {
      throw new Error("Failed to update stolperstein.");
    }

    return updated;
  }

  @Mutation((_) => Stolperstein, {
    description:
      "Mark the Stolperstein and all associated Stolperstein and StolpersteinSubject entities as deleted.",
  })
  async deepDeleteStolperstein(@Arg("id") id: string): Promise<Stolperstein> {
    const current = await Stolperstein.getRepository().findOne(id);

    if (!current) {
      throw new Error("No Stolperstein object with the specified id found.");
    }

    for (const subjectId of current.subjectIds) {
      const currentSubject = await stolpersteinSubjectRepository.findOne(
        subjectId
      );

      if (currentSubject && currentSubject.deleted === null) {
        const deletedSubject = await currentSubject.delete();

        if (!deletedSubject) {
          throw new Error(
            "Failed to mark at least one of the referenced subjects as deleted."
          );
        }
      }
    }

    const deleted = current.deleted !== null ? current : await current.delete();

    if (!deleted) {
      throw new Error(
        "Failed to mark the specified Stolperstein as deleted for an unknown reason." +
          id
      );
    }

    return deleted;
  }
}
