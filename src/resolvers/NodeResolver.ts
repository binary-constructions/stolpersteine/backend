import { Resolver, Root, FieldResolver, ClassType } from "type-graphql";

import { IReadRepository } from "../../lib/repository";

import { Node, EntityResolver, nodeRepository } from "../internal";

export interface INodeResolverProps<T extends string, U extends Node<T, any>> {
  repository: IReadRepository<U>;

  typename: T;

  singularQueryName: string;

  multiplesQueryName: string;
}

/**
 * The resolver constructor for `Node` objects.
 */
export function NodeResolver<T extends string, U extends Node<T, any>>(
  NodeClass: ClassType<U>,
  props: INodeResolverProps<T, U>
) {
  nodeRepository.addRepository(props.repository);

  @Resolver((_) => NodeClass, { isAbstract: true })
  abstract class NodeResolverClass extends EntityResolver(NodeClass, {
    ...props,
    singularKeyName: "id",
    multiplesKeyName: "ids",
  }) {
    @FieldResolver((_) => Boolean, {
      description: "Supposed to return 'true'.",
      deprecationReason: "Meant to be used for debugging only!",
    })
    protected debugResolvesAsNode(@Root() node: Node<any, any>): Boolean {
      return node instanceof Node;
    }
  }

  return NodeResolverClass as any;
}
