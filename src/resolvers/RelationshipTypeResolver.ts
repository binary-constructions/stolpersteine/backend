import { Arg, Mutation, Resolver, FieldResolver, Root } from "type-graphql";

import {
  RelationshipType,
  RelationshipTypeInput,
  Locale,
  NodeResolver,
} from "../internal";

/**
 * The resolver for `RelationshipType` objects.
 */
@Resolver((_) => RelationshipType)
export class RelationshipTypeResolver extends NodeResolver(RelationshipType, {
  repository: RelationshipType.getRepository(),
  typename: RelationshipType.typename,
  singularQueryName: "relationshipType",
  multiplesQueryName: "relationshipTypes",
}) {
  @FieldResolver((_) => Locale)
  async locale(@Root() relationshipType: RelationshipType): Promise<Locale> {
    const locale = await Locale.getRepository().findOne(
      relationshipType.localeCode
    );

    if (!locale) {
      throw new Error(
        `Unable to resolve locale code '${relationshipType.localeCode}'.`
      );
    }

    return locale;
  }

  @Mutation((_) => RelationshipType, {
    description: "Create a new RelationshipType object.",
  })
  async createRelationshipType(
    @Arg("input") input: RelationshipTypeInput
  ): Promise<RelationshipType> {
    const created = await RelationshipType.create({
      ...input,
      localeCode: input.locale,
      localizations: input.localizations.map((v) => ({
        ...v,
        localeCode: v.locale,
      })),
    });

    if (!created) {
      throw new Error("Failed to create relationship type.");
    }

    return created;
  }

  @Mutation((_) => RelationshipType, {
    description: "Update an existing RelationshipType object.",
  })
  async updateRelationshipType(
    @Arg("id") id: string,
    @Arg("input") input: RelationshipTypeInput
  ): Promise<RelationshipType> {
    const updated = await RelationshipType.update({
      ...input,
      id,
      localeCode: input.locale,
      localizations: input.localizations.map((v) => ({
        ...v,
        localeCode: v.locale,
      })),
    });

    if (!updated) {
      throw new Error("Failed to update relationship type.");
    }

    return updated;
  }
}
