import { Query, Resolver, Arg, Mutation } from "type-graphql";

import { Content, contentRepository, ReviewStatus } from "../internal";

/**
 * The meta resolver for all `Content` objects.
 */
@Resolver()
export class ContentsResolver {
  @Query((_) => [Content], { description: "Get all Content objects." })
  contents(): Promise<Content<any, any>[]> {
    return contentRepository.find();
  }

  @Query((_) => Content, {
    nullable: true,
    description: "Get the Content object with the specified id, if it exists.",
  })
  async content(
    @Arg("id", {
      description: "The ID of any Content object.",
    })
    id: string
  ): Promise<Content<any, any> | null> {
    return contentRepository.findOne(id);
  }

  // FIXME: generalize handling of deletions for non Content objects
  @Mutation((_) => Content, {
    description: "Mark the Content entity with the specified id as deleted.",
  })
  async delete(@Arg("id") id: string): Promise<Content<any, any>> {
    const content = await contentRepository.findOne(id);

    if (!content) {
      throw new Error(
        "No Content object with the specified id could be found."
      );
    }

    const deleted = await content.delete();

    if (!deleted) {
      throw new Error("The Content object could not be deleted.");
    }

    if (!(deleted instanceof Content)) {
      // FIXME: use generic typing for delete instead
      throw new Error(
        "The object returned by the delete operation was not of type Content :(" // assert()
      );
    }

    return deleted;
  }

  // FIXME: generalize handling of deletions for non Content objects
  @Mutation((_) => Content, {
    description: "Mark the Content entity with the specified id as undeleted.",
  })
  async undelete(@Arg("id") id: string): Promise<Content<any, any>> {
    const content = await contentRepository.findOne(id);

    if (!content) {
      throw new Error(
        "No Content object with the specified id could be found."
      );
    }

    const undeleted = await content.undelete();

    if (!undeleted) {
      throw new Error("The Content object could not be undeleted.");
    }

    if (!(undeleted instanceof Content)) {
      // FIXME: use generic typing for delete instead
      throw new Error(
        "The object returned by the undelete operation was not of type Content :(" // assert()
      );
    }

    return undeleted;
  }

  @Mutation((_) => Content, {
    description:
      "Change the review status of the Content entity with the specified id.",
  })
  async setReviewStatus(
    @Arg("id") id: string,
    @Arg("status", (_) => ReviewStatus) status: ReviewStatus
  ): Promise<Content<any, any>> {
    const content = await contentRepository.findOne(id);

    if (!content) {
      throw new Error(
        "No Content object with the specified id could be found."
      );
    }

    const updated = await content.setReviewStatus(status);

    if (!updated) {
      throw new Error(
        "Failed to update the review status of the specified content entity."
      );
    }

    return updated;
  }
}
