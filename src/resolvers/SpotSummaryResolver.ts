import { Query, Resolver } from "type-graphql";

import { SpotSummary } from "../internal";

/**
 * The resolver for `SpotSummary` objects.
 */
@Resolver((_) => SpotSummary)
export class SpotSummaryResolver {
  @Query((_) => [SpotSummary], {
    deprecationReason:
      "Summary queries were only introduced as a hack and will probably be removed again in the future.",
  })
  spotSummaries(): Promise<SpotSummary[]> {
    return SpotSummary.find();
  }
}
