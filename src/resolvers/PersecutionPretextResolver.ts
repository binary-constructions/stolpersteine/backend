import { Arg, Mutation, Resolver, FieldResolver, Root } from "type-graphql";

import {
  PersecutionPretext,
  PersecutionPretextInput,
  Locale,
  NodeResolver,
} from "../internal";

/**
 * The resolver for `PersecutionPretext` objects.
 */
@Resolver((_) => PersecutionPretext)
export class PersecutionPretextResolver extends NodeResolver(
  PersecutionPretext,
  {
    repository: PersecutionPretext.getRepository(),
    typename: PersecutionPretext.typename,
    singularQueryName: "persecutionPretext",
    multiplesQueryName: "persecutionPretexts",
  }
) {
  @FieldResolver((_) => Locale)
  async locale(
    @Root() persecutionPretext: PersecutionPretext
  ): Promise<Locale> {
    const locale = await Locale.getRepository().findOne(
      persecutionPretext.localeCode
    );

    if (!locale) {
      throw new Error(
        `Unable to resolve locale code '${persecutionPretext.localeCode}'.`
      );
    }

    return locale;
  }

  @Mutation((_) => PersecutionPretext, {
    description: "Create a new PersecutionPretext object.",
  })
  async createPersecutionPretext(
    @Arg("input") input: PersecutionPretextInput
  ): Promise<PersecutionPretext> {
    const created = await PersecutionPretext.create({
      ...input,
      localeCode: input.locale,
      localizations: input.localizations.map((v) => ({
        ...v,
        localeCode: v.locale,
      })),
    });

    if (!created) {
      throw new Error("Failed to create persecution pretext.");
    }

    return created;
  }

  @Mutation((_) => PersecutionPretext, {
    description: "Update an existing PersecutionPretext object.",
  })
  async updatePersecutionPretext(
    @Arg("id") id: string,
    @Arg("input") input: PersecutionPretextInput
  ): Promise<PersecutionPretext> {
    const updated = await PersecutionPretext.update({
      ...input,
      id,
      localeCode: input.locale,
      localizations: input.localizations.map((v) => ({
        ...v,
        localeCode: v.locale,
      })),
    });

    if (!updated) {
      throw new Error("Failed to update persecution pretext.");
    }

    return updated;
  }
}
