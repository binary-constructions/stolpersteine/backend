import { Arg, Mutation, Resolver, FieldResolver, Root } from "type-graphql";

import { Occupation, OccupationInput, Locale, NodeResolver } from "../internal";

/**
 * The resolver for `Occupation` objects.
 */
@Resolver((_) => Occupation)
export class OccupationResolver extends NodeResolver(Occupation, {
  repository: Occupation.getRepository(),
  typename: Occupation.typename,
  singularQueryName: "occupation",
  multiplesQueryName: "occupations",
}) {
  @FieldResolver((_) => Locale)
  async locale(@Root() occupation: Occupation): Promise<Locale> {
    const locale = await Locale.getRepository().findOne(occupation.localeCode);

    if (!locale) {
      throw new Error(
        `Unable to resolve locale code '${occupation.localeCode}'.`
      );
    }

    return locale;
  }

  @Mutation((_) => Occupation, {
    description: "Create a new Occupation object.",
  })
  async createOccupation(
    @Arg("input") input: OccupationInput
  ): Promise<Occupation> {
    const created = await Occupation.create({
      ...input,
      localeCode: input.locale,
      localizations: input.localizations.map((v) => ({
        ...v,
        localeCode: v.locale,
      })),
    });

    if (!created) {
      throw new Error("Failed to create occupation.");
    }

    return created;
  }

  @Mutation((_) => Occupation, {
    description: "Update an existing Occupation object.",
  })
  async updateOccupation(
    @Arg("id") id: string,
    @Arg("input") input: OccupationInput
  ): Promise<Occupation> {
    const updated = await Occupation.update({
      ...input,
      id,
      localeCode: input.locale,
      localizations: input.localizations.map((v) => ({
        ...v,
        localeCode: v.locale,
      })),
    });

    if (!updated) {
      throw new Error("Failed to update occupation.");
    }

    return updated;
  }
}
