import { Arg, Mutation, Resolver, FieldResolver, Root } from "type-graphql";

import {
  Spot,
  SpotInput,
  Place,
  Image,
  GeoPoint,
  StreetDetailCorner,
  StreetDetailNumber,
  Stolperstein,
  ContentResolver,
  stolpersteinSubjectRepository,
} from "../internal";

/**
 * The resolver for `Spot` objects.
 */
@Resolver((_) => Spot)
export class SpotResolver extends ContentResolver(Spot, {
  repository: Spot.getRepository(),
  typename: Spot.typename,
  singularQueryName: "spot",
  multiplesQueryName: "spots",
}) {
  @FieldResolver((_) => Place, { nullable: true })
  async place(@Root() spot: Spot): Promise<Place | null> {
    return spot.placeId ? Place.getRepository().findOne(spot.placeId) : null;
  }

  @FieldResolver((_) => [Image])
  async images(@Root() spot: Spot): Promise<Image[]> {
    if (spot.imageIds.length) {
      const images = await Image.getRepository().find();

      return images.filter((v) => spot.imageIds.includes(v.id));
    } else {
      return [];
    }
  }

  @FieldResolver((_) => [Stolperstein], {
    description: "Get all Stolperstein objects located at this spot.",
  })
  async stolpersteine(@Root() spot: Spot): Promise<Stolperstein[]> {
    return (await Stolperstein.getRepository().find()).filter(
      (v) => v.spotId === spot.id
    );
  }

  // FIXME: this "non-deleted" doesn't belong here, does it?
  @FieldResolver((_) => [String], {
    description:
      "Get the ids of all non-deleted stolpersteine present at this spot.",
  })
  async stolpersteinIds(@Root() spot: Spot): Promise<string[]> {
    return (await this.stolpersteine(spot))
      .filter((v) => !v.deleted)
      .map((v) => v.id);
  }

  @Mutation((_) => Spot, { description: "Create a new Spot object." })
  async createSpot(@Arg("input") input: SpotInput): Promise<Spot> {
    const { longitude, latitude, corner, number } = input;

    const point = new GeoPoint({ longitude, latitude });

    const streetDetail = corner
      ? new StreetDetailCorner(corner)
      : number
      ? new StreetDetailNumber(number)
      : null;

    const spot = await Spot.create({
      ...input,
      point,
      streetDetail,
    });

    if (!spot) {
      throw new Error("Failed to create spot.");
    }

    return spot;
  }

  @Mutation((_) => Spot, { description: "Update an existing Spot object." })
  async updateSpot(
    @Arg("id") id: string,
    @Arg("input") input: SpotInput
  ): Promise<Spot> {
    const { longitude, latitude, corner, number } = input;

    const point = new GeoPoint({ longitude, latitude });

    const streetDetail = corner
      ? new StreetDetailCorner(corner)
      : number
      ? new StreetDetailNumber(number)
      : null;

    const updated = await Spot.update({ ...input, id, point, streetDetail });

    if (!updated) {
      throw new Error("Failed to update spot.");
    }

    return updated;
  }

  @Mutation((_) => Spot, {
    description:
      "Mark the Spot and all associated Stolperstein and StolpersteinSubject entities as deleted.",
  })
  async deepDeleteSpot(@Arg("id") id: string): Promise<Spot> {
    const current = await Spot.getRepository().findOne(id);

    if (!current) {
      throw new Error("No Spot object with the specified id found.");
    }

    const stolpersteine = (await Stolperstein.getRepository().find()).filter(
      (v) => v.spotId === id
    );

    for (const stolperstein of stolpersteine) {
      for (const subjectId of stolperstein.subjectIds) {
        const currentSubject = await stolpersteinSubjectRepository.findOne(
          subjectId
        );

        if (currentSubject && currentSubject.deleted === null) {
          const deletedSubject = await currentSubject.delete();

          if (!deletedSubject) {
            throw new Error(
              "Failed to mark at least one of the referenced subjects as deleted."
            );
          }
        }
      }

      if (stolperstein.deleted === null) {
        const deletedStolperstein = await stolperstein.delete();

        if (!deletedStolperstein) {
          throw new Error(
            "Failed to mark at least one of the referenced Stolpersteine as deleted."
          );
        }
      }
    }

    const deleted = current.deleted !== null ? current : await current.delete();

    if (!deleted) {
      throw new Error(
        "Failed to mark the specified Stolperstein as deleted for an unknown reason." +
          id
      );
    }

    return deleted;
  }
}
