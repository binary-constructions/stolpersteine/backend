import { Resolver, Root, FieldResolver, ClassType } from "type-graphql";

import {
  Content,
  NodeResolver,
  INodeResolverProps,
  contentRepository,
} from "../internal";

/**
 * The resolver constructor for `Content` objects.
 */
export function ContentResolver<T extends string, U extends Content<T, any>>(
  ContentClass: ClassType<U>,
  props: INodeResolverProps<T, U>
) {
  contentRepository.addRepository(props.repository);

  @Resolver((_) => ContentClass, { isAbstract: true })
  abstract class ContentResolverClass extends NodeResolver(
    ContentClass,
    props
  ) {
    @FieldResolver((_) => Boolean, {
      description: "Supposed to return 'true'.",
      deprecationReason: "Meant to be used for debugging only!",
    })
    protected debugResolvesAsContent(
      @Root() content: Content<any, any>
    ): Boolean {
      return content instanceof Content;
    }

    // TODO: where do these need to go?
    @FieldResolver((_) => Date, {
      name: "created",
      nullable: true,
      description:
        "The date this content object was created.  May be null if the creation time is unknown.",
    })
    protected createdDate(@Root() content: Content<any, any>): Date | null {
      return content.created ? new Date(content.created) : null;
    }

    @FieldResolver((_) => Date, { name: "lastModified", nullable: true })
    protected lastModifiedDate(
      @Root() content: Content<any, any>
    ): Date | null {
      return content.lastModified ? new Date(content.lastModified) : null;
    }

    @FieldResolver((_) => Date, { name: "deleted", nullable: true })
    protected deletedDate(@Root() content: Content<any, any>): Date | null {
      return content.deleted ? new Date(content.deleted) : null;
    }
  }

  return ContentResolverClass as any;
}
