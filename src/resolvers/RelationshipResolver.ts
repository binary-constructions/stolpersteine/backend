import {
  Arg,
  Query,
  Mutation,
  Resolver,
  FieldResolver,
  Root,
} from "type-graphql";

import {
  Relationship,
  NodeResolver,
  StolpersteinSubject,
  RelationshipType,
  stolpersteinSubjectRepository,
} from "../internal";

/**
 * The resolver for `Relationship` objects.
 */
@Resolver((_) => Relationship)
export class RelationshipResolver extends NodeResolver(Relationship, {
  repository: Relationship.getRepository(),
  typename: Relationship.typename,
  singularQueryName: "relationshipById", // FIXME: merge with the one below somehow
  multiplesQueryName: "relationships",
}) {
  @Query((_) => Relationship, {
    nullable: true,
    description:
      "Get the (non-inverted) Relationship between the two specified StolpersteinSubject objects.  (Inverse relationships need to be queried separately!)",
  })
  async relationship(
    @Arg("subjectId", {
      description: "The ID of a StolpersteinSubject object.",
    })
    subjectId: string,
    @Arg("objectId", {
      description: "The ID of another StolpersteinSubject object.",
    })
    objectId: string
  ): Promise<Relationship | null> {
    return (
      (await Relationship.getRepository().find()).find(
        (v) => v.subjectId === subjectId && v.objectId === objectId
      ) || null
    );
  }

  @Query((_) => [Relationship], {
    description:
      "Get a list of all Relationships with the specified ID as the subject.",
  })
  async relationshipsWithSubject(
    @Arg("subjectId", {
      description: "The ID of a StolpersteinSubject object.",
    })
    id: string
  ): Promise<Relationship[]> {
    return (await Relationship.getRepository().find()).filter(
      (v) => v.subjectId === id
    );
  }

  @Query((_) => [Relationship], {
    description:
      "Get a list of all Relationships with the specified ID as the object.",
  })
  async relationshipsWithObject(
    @Arg("objectId", { description: "The ID of a StolpersteinSubject object." })
    id: string
  ): Promise<Relationship[]> {
    return (await Relationship.getRepository().find()).filter(
      (v) => v.objectId === id
    );
  }

  @Mutation((_) => Relationship, {
    description:
      "Create or update a Relationship between two StolpersteinSubject objects.",
  })
  async setRelationship(
    @Arg("subjectId", {
      description: "The ID of a StolpersteinSubject object.",
    })
    subjectId: string,
    @Arg("objectId", {
      description: "The ID of another StolpersteinSubject object.",
    })
    objectId: string,
    @Arg("typeId", { description: "The ID of a RelationshipType object." })
    typeId: string
  ): Promise<Relationship> {
    const created = await Relationship.create({ subjectId, objectId, typeId });

    if (!created) {
      throw new Error("Failed to create the requested relationship.");
    }

    const matching = (await Relationship.getRepository().find()).filter(
      (v) =>
        v.id !== created.id &&
        ((v.subjectId === subjectId && v.objectId === objectId) ||
          (v.objectId === subjectId && v.subjectId === objectId))
    );

    for (const match of matching) {
      const deleted = await match.delete();

      if (!deleted) {
        throw new Error(
          "Another relationship between those two already exists but it could not be deleted for some reason."
        );
      }
    }

    return created;
  }

  @Mutation((_) => Relationship, {
    nullable: true,
    description:
      "Remove a Relationship between two StolpersteinSubject objects (inverted or not).",
  })
  async unsetRelationship(
    @Arg("oneId", { description: "The ID of a StolpersteinSubject object." })
    oneId: string,
    @Arg("otherId", {
      description: "The ID of another StolpersteinSubject object.",
    })
    otherId: string
  ): Promise<Relationship | null> {
    const match = (await Relationship.getRepository().find()).find(
      (v) =>
        (v.subjectId === oneId && v.objectId === otherId) ||
        (v.objectId === oneId && v.subjectId === otherId)
    );

    return match ? match.delete() : null;
  }

  @FieldResolver((_) => StolpersteinSubject)
  async subject(
    @Root() rel: Relationship
  ): Promise<StolpersteinSubject<any, any>> {
    const subject = await stolpersteinSubjectRepository.findOne(rel.subjectId);

    if (!subject) {
      throw new Error("The referenced subject could not be found.");
    }

    return subject;
  }

  @FieldResolver((_) => StolpersteinSubject)
  async object(
    @Root() rel: Relationship
  ): Promise<StolpersteinSubject<any, any>> {
    const object = await stolpersteinSubjectRepository.findOne(rel.objectId);

    if (!object) {
      throw new Error("The referenced object could not be found.");
    }

    return object;
  }

  @FieldResolver((_) => RelationshipType)
  async type(@Root() rel: Relationship): Promise<RelationshipType> {
    const relationshipType = await RelationshipType.getRepository().findOne(
      rel.typeId
    );

    if (!relationshipType) {
      throw new Error("The referenced relationship type could not be found.");
    }

    return relationshipType;
  }
}
