import {
  Resolver,
  Root,
  FieldResolver,
  ClassType,
  Query,
  Arg,
} from "type-graphql";

import { IReadRepository } from "../../lib/repository";

import { Entity } from "../internal";

export interface IEntityResolverProps<
  T extends string,
  U extends Entity<T, any>
> {
  repository: IReadRepository<U>;

  typename: T;

  singularQueryName: string;

  multiplesQueryName: string;

  singularKeyName: string;

  multiplesKeyName: string;
}

/**
 * The resolver constructor for `Entity` objects.
 */
export function EntityResolver<T extends string, U extends Entity<T, any>>(
  EntityClass: ClassType<U>,
  props: IEntityResolverProps<T, U>
) {
  @Resolver((_) => EntityClass, { isAbstract: true })
  abstract class EntityResolverClass {
    @FieldResolver((_) => Boolean, {
      description: "Supposed to return 'true'.",
      deprecationReason: "Meant to be used for debugging only!",
    })
    protected debugResolvesAsEntity(@Root() entity: Entity<any, any>): Boolean {
      return entity instanceof Entity;
    }

    @FieldResolver((_) => String, {
      description:
        "The internal representation of the object as a JSON string.",
      deprecationReason: "Meant to be used for debugging only!",
    })
    protected debugInternalJsonRepresentation(
      @Root() entity: Entity<any, any>
    ): string {
      return entity.jsonify();
    }

    @Query(() => EntityClass, {
      name: props.singularQueryName,
      nullable: true,
      description: `Get the '${props.typename}' entity with the specified id if it exists.`,
    })
    async findOne(@Arg(props.singularKeyName) id: string): Promise<U | null> {
      return props.repository.findOne(id);
    }

    @Query(() => [EntityClass], {
      name: props.multiplesQueryName,
      description: `Get a list of either the specified or all '${props.typename}' entities.`,
    })
    async find(
      @Arg(props.multiplesKeyName, (_) => [String], {
        nullable: true,
        description:
          "Limit the returned list to entities with ids matching those here.",
      })
      ids?: string[]
    ): Promise<U[]> {
      return ids
        ? (await props.repository.find()).filter((v) =>
            ids.includes(v.getKey())
          ) // FIXME: maybe better to do it the other way around?
        : props.repository.find();
    }
  }

  return EntityResolverClass as any;
}
