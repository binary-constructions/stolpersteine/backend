import { Arg, Mutation, Resolver, FieldResolver, Root } from "type-graphql";

import { Image, ImageInput, NodeResolver } from "../internal";

/**
 * The resolver for `Image` objects.
 */
@Resolver((_) => Image)
export class ImageResolver extends NodeResolver(Image, {
  repository: Image.getRepository(),
  typename: Image.typename,
  singularQueryName: "image",
  multiplesQueryName: "images",
}) {
  @FieldResolver((_) => String)
  async fileName(@Root() image: Image): Promise<string> {
    return image.getFileName();
  }

  @FieldResolver((_) => String, {
    description: "Get the (encoded) path to use for accessing the image.",
  })
  async path(@Root() image: Image): Promise<string> {
    return image.getPath();
  }

  @Mutation((_) => Image, { description: "Create a new Image object." })
  async createImage(@Arg("input") input: ImageInput): Promise<Image> {
    const image = await Image.create(input);

    if (!image) {
      throw new Error("Failed to create image.");
    }

    return image;
  }

  @Mutation((_) => Image, { description: "Update an existing Image object." })
  async updateImage(
    @Arg("id") id: string,
    @Arg("input") input: ImageInput
  ): Promise<Image> {
    const updated = await Image.update({ ...input, id });

    if (!updated) {
      throw new Error("Failed to update image.");
    }

    return updated;
  }
}
