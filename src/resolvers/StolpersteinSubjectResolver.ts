import { Resolver, Root, FieldResolver, ClassType } from "type-graphql";

import {
  StolpersteinSubject,
  ContentResolver,
  Stolperstein,
  INodeResolverProps,
  stolpersteinSubjectRepository,
} from "../internal";

/**
 * The resolver constructor for `StolpersteinSubject` objects.
 */
export function StolpersteinSubjectResolver<
  T extends string,
  U extends StolpersteinSubject<T, any>
>(StolpersteinSubjectClass: ClassType<U>, props: INodeResolverProps<T, U>) {
  stolpersteinSubjectRepository.addRepository(props.repository);

  @Resolver((_) => StolpersteinSubjectClass, { isAbstract: true })
  abstract class StolpersteinSubjectResolverClass extends ContentResolver(
    StolpersteinSubjectClass,
    props
  ) {
    @FieldResolver((_) => Boolean, {
      description: "Supposed to return 'true'.",
      deprecationReason: "Meant to be used for debugging only!",
    })
    protected debugResolvesAsStolpersteinSubject(
      @Root() stolpersteinSubject: StolpersteinSubject<any, any>
    ): Boolean {
      return stolpersteinSubject instanceof StolpersteinSubject;
    }

    @FieldResolver((_) => [String], {
      name: "subjectOfIds",
    })
    protected async resolveSubjectOfIds(
      @Root() stolpersteinSubject: StolpersteinSubject<any, any>
    ): Promise<string[]> {
      return (await Stolperstein.getRepository().find())
        .filter((v) => v.subjectIds.includes(stolpersteinSubject.id))
        .map((v) => v.id);
    }

    @FieldResolver((_) => [Stolperstein], {
      name: "subjectOf",
    })
    protected async resolveSubjectOf(
      @Root() stolpersteinSubject: StolpersteinSubject<any, any>
    ): Promise<Stolperstein[]> {
      return (await Stolperstein.getRepository().find()).filter((v) =>
        v.subjectIds.includes(stolpersteinSubject.id)
      );
    }
  }

  return StolpersteinSubjectResolverClass as any;
}
