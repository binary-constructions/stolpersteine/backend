import { Arg, Mutation, Resolver, FieldResolver, Root } from "type-graphql";

import { asDictionary } from "../../lib/dictionary";

import {
  StolpersteinSubjectResolver,
  Person,
  PersonInput,
  Place,
  Image,
  ReviewStatus,
  Occupation,
  PersecutionPretext,
  asPersonNameInterface,
  PersonTitle,
} from "../internal";

/**
 * The resolver for `Person` objects.
 */
@Resolver((_) => Person)
export class PersonResolver extends StolpersteinSubjectResolver(Person, {
  repository: Person.getRepository(),
  typename: Person.typename,
  singularQueryName: "person",
  multiplesQueryName: "persons",
}) {
  @FieldResolver((_) => [Occupation])
  async occupations(@Root() person: Person): Promise<Occupation[]> {
    return person.occupationIds.length // FIXME(?): better get entities individually in such cases?
      ? (await Occupation.getRepository().find()).filter((v) =>
          person.occupationIds.includes(v.id)
        )
      : [];
  }

  @FieldResolver((_) => Place, { nullable: true })
  async bornPlace(@Root() person: Person): Promise<Place | null> {
    return person.bornPlaceId
      ? Place.getRepository().findOne(person.bornPlaceId)
      : null;
  }

  @FieldResolver((_) => Place, { nullable: true })
  async diedPlace(@Root() person: Person): Promise<Place | null> {
    return person.diedPlaceId
      ? Place.getRepository().findOne(person.diedPlaceId)
      : null;
  }

  @FieldResolver((_) => [PersecutionPretext])
  async persecutionPretexts(
    @Root() person: Person
  ): Promise<PersecutionPretext[]> {
    return person.persecutionPretextIds.length // FIXME(?): better get entities individually in such cases?
      ? (await PersecutionPretext.getRepository().find()).filter((v) =>
          person.persecutionPretextIds.includes(v.id)
        )
      : [];
  }

  @FieldResolver((_) => [Image])
  async images(@Root() person: Person): Promise<Image[]> {
    if (person.imageIds.length) {
      const images = await Image.getRepository().find();

      return images.filter((v) => person.imageIds.includes(v.id));
    } else {
      return [];
    }
  }

  @Mutation((_) => Person, {
    description: "Create a new Person object.",
  })
  async createPerson(@Arg("input") input: PersonInput): Promise<Person> {
    const names = input.names.map((v) =>
      asPersonNameInterface(asDictionary(v))
    );

    const title = input.title?.trim() ? new PersonTitle(input.title) : null;

    const person = await Person.create({
      ...input,
      names,
      title,
    });

    if (!person) {
      throw new Error("Failed to create person.");
    }

    return person;
  }

  @Mutation((_) => Person, {
    description: "Update an existing Image object.",
  })
  async updatePerson(
    @Arg("id") id: string,
    @Arg("input") input: PersonInput
  ): Promise<Person> {
    const names = input.names.map((v) =>
      asPersonNameInterface(asDictionary(v))
    );

    const title = input.title?.trim() ? new PersonTitle(input.title) : null;

    const updated = await Person.update({
      ...input,
      id,
      names,
      title,
    });

    if (!updated) {
      throw new Error("No Person object with the specified id found.");
    }

    return updated;
  }

  @Mutation((_) => Person, {
    description: "Change the ReviewStatus of the Person with the specified id.",
  })
  async setPersonReviewStatus(
    @Arg("id") id: string,
    @Arg("status", (_) => ReviewStatus) status: ReviewStatus
  ): Promise<Person> {
    const current = await Person.getRepository().findOne(id);

    if (!current) {
      throw new Error("No Person object with the specified id found.");
    }

    const updated = await Person.update({
      ...current,
      reviewStatus: status,
    });

    if (!updated) {
      throw new Error("Failed to update person.");
    }

    return updated;
  }

  @Mutation((_) => Person, {
    description: "Mark the specified Person entity as deleted.",
    deprecationReason: "This actually does the same as a simple delete.",
  })
  async deepDeletePerson(@Arg("id") id: string): Promise<Person> {
    const deleted = await Person.delete(id);

    if (deleted === null) {
      throw new Error(
        "Failed to delete the person with the specified id; maybe it doesn't exist?"
      );
    }

    return deleted;
  }
}
