import { Resolver, FieldResolver, Root } from "type-graphql";

import { PersecutionPretextLocalization, Locale } from "../internal";

/**
 * The resolver for `PersecutionPretextLocalization` objects.
 */
@Resolver((_) => PersecutionPretextLocalization)
export class PersecutionPretextLocalizationResolver {
  @FieldResolver((_) => Locale)
  async locale(
    @Root() persecutionPretextLocalization: PersecutionPretextLocalization
  ): Promise<Locale> {
    const locale = await Locale.getRepository().findOne(
      persecutionPretextLocalization.localeCode
    );

    if (!locale) {
      throw new Error(
        `Unable to resolve locale code '${persecutionPretextLocalization.localeCode}'.`
      );
    }

    return locale;
  }
}
