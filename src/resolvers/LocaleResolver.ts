import { Arg, Resolver, Mutation } from "type-graphql";

import { Locale, EntityResolver } from "../internal";

/**
 * The resolver for `Locale` objects.
 */
@Resolver()
export class LocaleResolver extends EntityResolver(Locale, {
  repository: Locale.getRepository(),
  typename: Locale.typename,
  singularQueryName: "locale",
  multiplesQueryName: "locales",
  singularKeyName: "code",
  multiplesKeyName: "codes",
}) {
  @Mutation((_) => Locale, { description: "Add a new Locale.", nullable: true })
  async addLocale(
    @Arg("code", {
      description:
        'The language code for the Locale, either in the form "xx" or "xx-XX".',
    })
    code: string,
    @Arg("label", {
      description: "The label to use for the created Locale.",
    })
    label: string
  ): Promise<Locale | null> {
    return Locale.create({ code, label });
  }

  @Mutation((_) => Locale, { description: "Update a Locale.", nullable: true })
  async updateLocale(
    @Arg("code", { description: "The language code of the Locale to update." })
    code: string,
    @Arg("label", { description: "The new label for the Locale." })
    label: string
  ): Promise<Locale | null> {
    return Locale.update({ code, label });
  }

  @Mutation((_) => Locale, { description: "Remove a Locale.", nullable: true })
  async removeLocale(
    @Arg("code", { description: "The language code of the Locale to remove." })
    code: string
  ): Promise<Locale | null> {
    return Locale.delete(code);
  }
}
