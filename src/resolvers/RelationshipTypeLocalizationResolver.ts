import { Resolver, FieldResolver, Root } from "type-graphql";

import { RelationshipTypeLocalization, Locale } from "../internal";

// TODO: generalize l18n resolvers to use a common interface and then
// share resolvers?

/**
 * The resolver for `RelationshipTypeLocalization` objects.
 */
@Resolver((_) => RelationshipTypeLocalization)
export class RelationshipTypeLocalizationResolver {
  @FieldResolver((_) => Locale)
  async locale(
    @Root() relationshipTypeLocalization: RelationshipTypeLocalization
  ): Promise<Locale> {
    const locale = await Locale.getRepository().findOne(
      relationshipTypeLocalization.localeCode
    );

    if (!locale) {
      throw new Error(
        `Unable to resolve locale code '${relationshipTypeLocalization.localeCode}'.`
      );
    }

    return locale;
  }
}
