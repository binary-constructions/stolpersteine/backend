import { Query, Resolver, Arg } from "type-graphql";

import {
  StolpersteinSubject,
  stolpersteinSubjectRepository,
} from "../internal";

/**
 * The meta resolver for all `StolpersteinSubject` objects.
 */
@Resolver()
export class StolpersteinSubjectsResolver {
  @Query((_) => [StolpersteinSubject], {
    description: "Get all StolpersteinSubject objects.",
  })
  stolpersteinSubjects(): Promise<StolpersteinSubject<any, any>[]> {
    return stolpersteinSubjectRepository.find();
  }

  @Query((_) => StolpersteinSubject, {
    nullable: true,
    description:
      "Get the StolpersteinSubject object with the specified id, if it exists.",
  })
  async stolpersteinSubject(
    @Arg("id", {
      description: "The ID of any StolpersteinSubject object.",
    })
    id: string
  ): Promise<StolpersteinSubject<any, any> | null> {
    return stolpersteinSubjectRepository.findOne(id);
  }
}
