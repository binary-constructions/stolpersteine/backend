import {
  Root,
  Arg,
  Query,
  Resolver,
  FieldResolver,
  Mutation,
} from "type-graphql";

import { Place, PlaceInput, NodeResolver } from "../internal";

/**
 * The resolver for `Locale` objects.
 */
@Resolver((_) => Place)
export class PlaceResolver extends NodeResolver(Place, {
  repository: Place.getRepository(),
  typename: Place.typename,
  singularQueryName: "place",
  multiplesQueryName: "places",
}) {
  @Query((_) => [Place], {
    description: "Get a list of all Place objects without a parent.",
  })
  async placesByParent(
    @Arg("id", { nullable: true }) id?: string
  ): Promise<Place[]> {
    return id === undefined
      ? (await Place.getRepository().find()).filter((v) => v.parentId === null)
      : (await Place.getRepository().find()).filter((v) => v.parentId === id);
  }

  @FieldResolver((_) => Place, {
    nullable: true,
    description: "Get the parent of this Place, if any.",
  })
  parent(@Root() place: Place): Promise<Place | null> {
    return place.parentId
      ? Place.getRepository().findOne(place.parentId)
      : Promise.resolve(null);
  }

  @FieldResolver((_) => [Place], {
    nullable: true,
    description:
      "Get an ordered list of all the parents of this Place (with the immediate parent coming first).",
  })
  async parents(@Root() place: Place): Promise<Place[]> {
    const parents: Place[] = [];

    let child = place;

    while (child.parentId !== null) {
      const parent = await Place.getRepository().findOne(child.parentId);

      if (!parent) {
        break;
      }

      if (parents.findIndex((v) => v.id === parent.id) !== -1) {
        throw new Error("Circular reference in parent chain of Place.");
      }

      parents.push(parent);

      child = parent;
    }

    return parents;
  }

  @FieldResolver((_) => [Place], {
    nullable: true,
    description: "Get a list of all the children of this Place.",
  })
  async children(@Root() place: Place): Promise<Place[]> {
    return (await Place.getRepository().find()).filter(
      (v) => v.parentId === place.id
    );
  }

  @Mutation((_) => Place, { description: "Create a new Place object." })
  async createPlace(@Arg("input") input: PlaceInput): Promise<Place> {
    const place = await Place.create(input);

    if (!place) {
      throw new Error("Failed to create place.");
    }

    return place;
  }

  @Mutation((_) => Place, {
    description: "Restore a previously existing Place object.",
  })
  async restorePlace(
    @Arg("id") id: string,
    @Arg("input") input: PlaceInput
  ): Promise<Place> {
    const place = await Place.create({ ...input, id });

    if (!place) {
      throw new Error("Failed to restore place.");
    }

    return place;
  }

  @Mutation((_) => Place, { description: "Update an existing Place object." })
  async updatePlace(
    @Arg("id") id: string,
    @Arg("input") input: PlaceInput
  ): Promise<Place> {
    const place = await Place.update({ ...input, id });

    if (!place) {
      throw new Error("Feiled to update place.");
    }

    return place;
  }
}
