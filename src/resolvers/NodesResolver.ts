import { Query, Resolver, Arg } from "type-graphql";

import { Node, nodeRepository } from "../internal";

/**
 * The meta resolver for all `Node` objects.
 */
@Resolver()
export class NodesResolver {
  @Query((_) => [Node])
  nodes(): Promise<Node<any, any>[]> {
    return nodeRepository.find();
  }

  @Query((_) => Node, {
    nullable: true,
    description: "Get the Node with the specified id, if it exists.",
  })
  async node(
    @Arg("id", {
      description: "The ID of any Node object.",
    })
    id: string
  ): Promise<Node<any, any> | null> {
    return nodeRepository.findOne(id);
  }
}
