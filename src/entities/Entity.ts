import { IEntityProps, ISanitizedProps } from "../internal";

/**
 * A base class to use for implementations of `IEntity`.
 */
export abstract class Entity<T extends string, U extends IEntityProps<T>>
  implements IEntityProps<T> {
  abstract readonly typename: T;

  abstract getKey(): string;

  jsonify(): string {
    return JSON.stringify(this, null, 2);
  }

  abstract delete(): Promise<Entity<T, U> | null>;

  constructor(_: IEntityProps<T> & ISanitizedProps) {}
}
