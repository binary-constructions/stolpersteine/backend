import { Field, ID, InterfaceType } from "type-graphql";

import { Entity, INodeProps, ISanitizedProps } from "../internal";

/**
 * A base class to use for implementations of `INode`.
 */
@InterfaceType("Node", {
  description: "An object uniquely identified by an ID.",
})
export abstract class Node<T extends string, U extends INodeProps<T>>
  extends Entity<T, U>
  implements INodeProps<T> {
  getKey() {
    return this.id;
  }

  @Field((_) => ID)
  readonly id: string;

  constructor(props: INodeProps<T> & ISanitizedProps) {
    super(props);

    const { id } = props;

    if (!id) {
      throw new Error("The Node constructor requires 'id' to be set.");
    } else if (props.id && !/^[-0-9a-z]+$/i.test(props.id)) {
      throw new Error(
        "Node ids must be made up of letters, numbers and dashes only."
      );
    }

    this.id = id;
  }
}
