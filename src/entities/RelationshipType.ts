import { Field, ObjectType } from "type-graphql";

import {
  FsInMemoryStringRepository,
  ReadOnlyRepositoryWrapper,
  IReadRepository,
} from "../../lib/repository";

import { asDictionary } from "../../lib/dictionary";

import {
  IRelationshipTypeProps,
  ISanitizedProps,
  Node,
  Locale,
  RelationshipTypeLocalization,
  asLocaleCode,
  asTrimmedNonEmptyString,
  asSanitizedNodeProps,
  asRelationshipTypeProps,
  wrapNodeRepository,
} from "../internal";

const typename = "RelationshipType";

type Typename = typeof typename;

const asPropsWithValidatedReferences = async (
  props: IRelationshipTypeProps & ISanitizedProps
): Promise<IRelationshipTypeProps & ISanitizedProps> => {
  const codes = [
    props.localeCode,
    ...props.localizations.map((v) => v.localeCode),
  ];

  const locales = await Locale.getRepository().find();

  for (const code of codes) {
    if (locales.findIndex((v) => v.code === code) === -1) {
      throw new Error("Not the code of a known Locale: " + code);
    }
  }

  return props;
};

/**
 * The class of all objects representing types of relationships.
 */
@ObjectType({
  implements: [Node],
  description: "An object representing a type of relationship.",
})
export class RelationshipType extends Node<Typename, IRelationshipTypeProps>
  implements IRelationshipTypeProps {
  readonly typename = RelationshipType.typename;

  @Field()
  readonly localeCode: string;

  @Field()
  readonly subjectLabel: string;

  @Field((_) => String, { nullable: true })
  readonly subjectLabel_f: string | null;

  @Field((_) => String, { nullable: true })
  readonly subjectLabel_m: string | null;

  @Field()
  objectLabel: string;

  @Field((_) => String, { nullable: true })
  readonly objectLabel_f: string | null;

  @Field((_) => String, { nullable: true })
  readonly objectLabel_m: string | null;

  @Field((_) => [RelationshipTypeLocalization])
  readonly localizations: RelationshipTypeLocalization[];

  constructor(props: IRelationshipTypeProps & ISanitizedProps) {
    super(props);

    const {
      localeCode,
      subjectLabel,
      subjectLabel_f,
      subjectLabel_m,
      objectLabel,
      objectLabel_f,
      objectLabel_m,
      localizations,
    } = props;

    this.localeCode = asLocaleCode(localeCode);

    this.subjectLabel = asTrimmedNonEmptyString(subjectLabel);

    this.subjectLabel_f = subjectLabel_f?.trim() || null;

    this.subjectLabel_m = subjectLabel_m?.trim() || null;

    this.objectLabel = asTrimmedNonEmptyString(objectLabel);

    this.objectLabel_f = objectLabel_f?.trim() || null;

    this.objectLabel_m = objectLabel_m?.trim() || null;

    this.localizations = localizations;
  }

  async update(
    props: IRelationshipTypeProps
  ): Promise<RelationshipType | null> {
    return repository.update(
      new RelationshipType(
        await asPropsWithValidatedReferences({
          ...props,
          ...asSanitizedNodeProps(this, props),
        })
      )
    );
  }

  delete(): Promise<RelationshipType | null> {
    return RelationshipType.delete(this.id);
  }

  static readonly typename = typename;

  static getRepository(): IReadRepository<RelationshipType> {
    return new ReadOnlyRepositoryWrapper(repository);
  }

  static restoreAlreadySaved(json: string): RelationshipType {
    const props = asRelationshipTypeProps(asDictionary(JSON.parse(json)));

    return new RelationshipType({
      ...props,
      ...asSanitizedNodeProps(null, props),
    });
  }

  static async create(
    props: IRelationshipTypeProps
  ): Promise<RelationshipType | null> {
    return repository.create(
      new RelationshipType(
        await asPropsWithValidatedReferences({
          ...props,
          ...asSanitizedNodeProps(null, props),
        })
      )
    );
  }

  static async update(
    props: IRelationshipTypeProps & { id: string }
  ): Promise<RelationshipType | null> {
    return (await repository.findOne(props.id))?.update(props) || null;
  }

  static delete(id: string): Promise<RelationshipType | null> {
    return repository.delete(id);
  }
}

const repository = wrapNodeRepository(
  new FsInMemoryStringRepository<RelationshipType>(
    "./data/" + RelationshipType.typename,
    RelationshipType.restoreAlreadySaved,
    (entity) => entity.jsonify(),
    "json"
  )
);
