import { Field, ObjectType } from "type-graphql";

import {
  FsInMemoryStringRepository,
  ReadOnlyRepositoryWrapper,
  IReadRepository,
} from "../../lib/repository";

import { asDictionary } from "../../lib/dictionary";

import {
  ISpotProps,
  ISanitizedProps,
  Node,
  Content,
  Place,
  Image,
  asSpotProps,
  asSanitizedContentProps,
  asId,
  GeoPoint,
  StreetDetail,
  StreetDetailCorner,
  StreetDetailNumber,
  wrapNodeRepository,
  ReviewStatus,
} from "../internal";

const typename = "Spot";

type Typename = typeof typename;

const asPropsWithValidatedReferences = async (
  props: ISpotProps & ISanitizedProps
): Promise<ISpotProps & ISanitizedProps> => {
  const { placeId, imageIds } = props;

  if (placeId && (await Place.getRepository().findOne(placeId)) === null) {
    throw new Error("Not a valid place id: " + placeId);
  }

  if (imageIds.length) {
    const images = await Image.getRepository().find();

    imageIds.forEach((v) => {
      if (images.findIndex((w) => w.id === v) === -1) {
        throw new Error("Not a valid image id: " + v);
      }
    });
  }

  return props;
};

/**
 * The class of all objects representing spots.
 */
@ObjectType({
  implements: [Node, Content],
  description: "An object representing a geographic spot.",
})
export class Spot extends Content<Typename, ISpotProps> implements ISpotProps {
  readonly typename = Spot.typename;

  @Field((_) => GeoPoint)
  readonly point: GeoPoint;

  @Field((_) => String, { nullable: true })
  readonly street: string | null;

  @Field((_) => StreetDetail, { nullable: true })
  readonly streetDetail: StreetDetailCorner | StreetDetailNumber | null;

  @Field((_) => String, { nullable: true })
  readonly postalCode: string | null;

  @Field((_) => String, { nullable: true })
  readonly placeId: string | null;

  @Field((_) => String, { nullable: true })
  readonly note: string | null;

  @Field((_) => [String])
  readonly imageIds: string[];

  constructor(props: ISpotProps & ISanitizedProps) {
    super(props);

    const {
      point,
      street,
      streetDetail,
      postalCode,
      placeId,
      note,
      imageIds,
    } = props;

    this.point = point;

    this.street = street?.trim() || null;

    this.streetDetail = streetDetail?.value.trim() ? streetDetail : null;

    this.postalCode = postalCode?.trim() || null;

    this.placeId = placeId ? asId(placeId) : null;

    this.note = note?.trim() || null;

    this.imageIds = imageIds.map((v) => asId(v));
  }

  async update(props: ISpotProps): Promise<Spot | null> {
    return repository.update(
      new Spot(
        await asPropsWithValidatedReferences({
          ...props,
          ...asSanitizedContentProps(this, props),
        })
      )
    );
  }

  setReviewStatus(reviewStatus: ReviewStatus): Promise<Spot | null> {
    return this.update({ ...this, reviewStatus });
  }

  delete(): Promise<Spot | null> {
    return this.update({ ...this, deleted: Date.now() });
  }

  undelete(): Promise<Spot | null> {
    return this.update({ ...this, deleted: null });
  }

  static readonly typename = typename;

  static getRepository(): IReadRepository<Spot> {
    return new ReadOnlyRepositoryWrapper(repository);
  }

  static restoreAlreadySaved(json: string): Spot {
    const props = asSpotProps(asDictionary(JSON.parse(json)));

    return new Spot({
      ...props,
      ...asSanitizedContentProps(null, props),
    });
  }

  static async create(props: ISpotProps): Promise<Spot | null> {
    return repository.create(
      new Spot(
        await asPropsWithValidatedReferences({
          ...props,
          ...asSanitizedContentProps(null, props),
        })
      )
    );
  }

  static async update(
    props: ISpotProps & { id: string }
  ): Promise<Spot | null> {
    return (await repository.findOne(props.id))?.update(props) || null; // -> canonical implementation
  }

  static async delete(id: string): Promise<Spot | null> {
    return (await repository.findOne(id))?.delete() || null; // -> canonical implementation
  }

  static async undelete(id: string): Promise<Spot | null> {
    return (await repository.findOne(id))?.undelete() || null; // -> canonical implementation
  }
}

const repository = wrapNodeRepository(
  new FsInMemoryStringRepository<Spot>(
    "./data/" + Spot.typename,
    Spot.restoreAlreadySaved,
    (entity) => entity.jsonify(),
    "json"
  )
);
