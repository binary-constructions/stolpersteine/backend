import { Field, ObjectType } from "type-graphql";

import {
  FsInMemoryStringRepository,
  IReadRepository,
  ReadOnlyRepositoryWrapper,
} from "../../lib/repository";

import { asDictionary } from "../../lib/dictionary";

import {
  Node,
  IOccupationProps,
  ISanitizedProps,
  asOccupationProps,
  OccupationLocalization,
  asLocaleCode,
  asSanitizedNodeProps,
  asTrimmedNonEmptyString,
  Locale,
  wrapNodeRepository,
} from "../internal";

const typename = "Occupation";

type Typename = typeof typename;

const asPropsWithValidatedReferences = async (
  props: IOccupationProps & ISanitizedProps
): Promise<IOccupationProps & ISanitizedProps> => {
  const codes = [
    props.localeCode,
    ...props.localizations.map((v) => v.localeCode),
  ];

  const locales = await Locale.getRepository().find();

  for (const code of codes) {
    if (locales.findIndex((v) => v.code === code) === -1) {
      throw new Error("Not the code of a known Locale: " + code);
    }
  }

  return props;
};

/**
 * The class of objects representing a persons occupation.
 */
@ObjectType({
  implements: [Node],
  description: "An object representing a persons occupation.",
})
export class Occupation extends Node<Typename, IOccupationProps>
  implements IOccupationProps {
  readonly typename = Occupation.typename;

  readonly localeCode: string;

  @Field()
  readonly label: string;

  @Field((_) => String, { nullable: true })
  readonly label_f: string | null;

  @Field((_) => String, { nullable: true })
  readonly label_m: string | null;

  @Field((_) => [OccupationLocalization])
  readonly localizations: OccupationLocalization[];

  constructor(props: IOccupationProps & ISanitizedProps) {
    super(props);

    const { localeCode, label, label_f, label_m, localizations } = props;

    this.localeCode = asLocaleCode(localeCode);

    this.label = asTrimmedNonEmptyString(label);

    this.label_f = label_f?.trim() || null;

    this.label_m = label_m?.trim() || null;

    this.localizations = localizations;
  }

  async update(props: IOccupationProps): Promise<Occupation | null> {
    return repository.update(
      new Occupation(
        await asPropsWithValidatedReferences({
          ...props,
          ...asSanitizedNodeProps(this, props),
        })
      )
    );
  }

  delete(): Promise<Occupation | null> {
    return Occupation.delete(this.id);
  }

  static readonly typename = typename;

  static getRepository(): IReadRepository<Occupation> {
    return new ReadOnlyRepositoryWrapper(repository);
  }

  static restoreAlreadySaved(json: string): Occupation {
    const props = asOccupationProps(asDictionary(JSON.parse(json)));

    return new Occupation({
      ...props,
      ...asSanitizedNodeProps(null, props),
    });
  }

  static async create(props: IOccupationProps): Promise<Occupation | null> {
    return repository.create(
      new Occupation(
        await asPropsWithValidatedReferences({
          ...props,
          ...asSanitizedNodeProps(null, props),
        })
      )
    );
  }

  static async update(
    props: IOccupationProps & { id: string }
  ): Promise<Occupation | null> {
    return (await repository.findOne(props.id))?.update(props) || null;
  }

  static delete(id: string): Promise<Occupation | null> {
    return repository.delete(id);
  }
}

const repository = wrapNodeRepository(
  new FsInMemoryStringRepository<Occupation>(
    "./data/" + Occupation.typename,
    Occupation.restoreAlreadySaved,
    (entity) => entity.jsonify(),
    "json"
  )
);
