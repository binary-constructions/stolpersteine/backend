import { Field, ObjectType } from "type-graphql";

import {
  FsInMemoryStringRepository,
  ReadOnlyRepositoryWrapper,
  IReadRepository,
} from "../../lib/repository";

import { asDictionary } from "../../lib/dictionary";

import {
  IStolpersteinProps,
  ISanitizedProps,
  Node,
  Content,
  HistoricalDateScalar,
  HistoricalDate,
  StreetDetail,
  StreetDetailCorner,
  StreetDetailNumber,
  asStolpersteinProps,
  Spot,
  Image,
  Place,
  wrapNodeRepository,
  asSanitizedContentProps,
  asId,
  Person,
  ReviewStatus,
} from "../internal";

const typename = "Stolperstein";

type Typename = typeof typename;

const asPropsWithValidatedReferences = async (
  props: IStolpersteinProps & ISanitizedProps
): Promise<IStolpersteinProps & ISanitizedProps> => {
  const { spotId, imageId, subjectIds, historicalPlaceId } = props;

  if (spotId && (await Spot.getRepository().findOne(spotId)) === null) {
    throw new Error("Not a valid spot id: " + spotId);
  }

  if (imageId && (await Image.getRepository().findOne(imageId)) === null) {
    throw new Error("Not a valid image id: " + imageId);
  }

  if (subjectIds.length) {
    const subjects = await Person.getRepository().find(); // FIXME: Person -> StolpersteinSubject

    subjectIds.forEach((v) => {
      if (subjects.findIndex((w) => w.id === v) === -1) {
        throw new Error("Not a valid subject id: " + v);
      }
    });
  }

  if (
    historicalPlaceId &&
    (await Place.getRepository().findOne(historicalPlaceId)) === null
  ) {
    throw new Error("Not a valid image id: " + historicalPlaceId);
  }

  return props;
};

/**
 * The class of all objects representing a Stolperstein.
 */
@ObjectType({
  implements: [Node, Content],
  description: "An object representing a Stolperstein.",
})
export class Stolperstein extends Content<Typename, IStolpersteinProps>
  implements IStolpersteinProps {
  readonly typename = typename;

  @Field((_) => String, { nullable: true })
  readonly spotId: string | null;

  @Field((_) => HistoricalDateScalar, { nullable: true })
  readonly laid: HistoricalDate | null;

  @Field((_) => String, {
    nullable: true,
    description: "The text inscribed on the stone (containing newlines).",
  })
  readonly inscription: string | null;

  @Field((_) => String, { nullable: true })
  readonly imageId: string | null;

  @Field((_) => [String])
  readonly subjectIds: string[];

  @Field((_) => String, { nullable: true })
  readonly historicalStreet: string | null;

  @Field((_) => StreetDetail, { nullable: true })
  readonly historicalStreetDetail:
    | StreetDetailCorner
    | StreetDetailNumber
    | null;

  @Field((_) => String, { nullable: true })
  readonly historicalPlaceId: string | null;

  @Field((_) => String, {
    nullable: true,
    description:
      "Information about people/groups that initiated the laying of this Stolperstein.",
  })
  readonly initiativeHTML: string | null;

  @Field((_) => String, {
    nullable: true,
    description:
      "Information about people/groups that helped finance the laying of this Stolperstein.",
  })
  readonly patronageHTML: string | null;

  @Field((_) => String, {
    nullable: true,
    description:
      "Information about people/groups that helped gather information about the subject of the Stolperstein.",
  })
  readonly dataGatheringHTML: string | null;

  @Field((_) => String, {
    nullable: true,
    description:
      "Information about people/groups that funded this Stolperstein.",
  })
  readonly fundingHTML: string | null;

  constructor(props: IStolpersteinProps & ISanitizedProps) {
    super(props);

    const {
      spotId,
      laid,
      inscription,
      imageId,
      subjectIds,
      historicalStreet,
      historicalStreetDetail,
      historicalPlaceId,
      initiativeHTML,
      patronageHTML,
      dataGatheringHTML,
      fundingHTML,
    } = props;

    this.spotId = spotId ? asId(spotId) : null;

    this.laid = laid;

    this.inscription = inscription?.trim() || null;

    this.imageId = imageId ? asId(imageId) : null;

    this.subjectIds = subjectIds.map((v) => asId(v));

    this.historicalStreet = historicalStreet?.trim() || null;

    this.historicalStreetDetail = historicalStreetDetail?.value.trim()
      ? historicalStreetDetail
      : null;

    this.historicalPlaceId = historicalPlaceId ? asId(historicalPlaceId) : null;

    this.initiativeHTML = initiativeHTML?.trim() || null;

    this.patronageHTML = patronageHTML?.trim() || null;

    this.dataGatheringHTML = dataGatheringHTML?.trim() || null;

    this.fundingHTML = fundingHTML?.trim() || null;
  }

  async update(props: IStolpersteinProps): Promise<Stolperstein | null> {
    return repository.update(
      new Stolperstein(
        await asPropsWithValidatedReferences({
          ...props,
          ...asSanitizedContentProps(this, props),
        })
      )
    );
  }

  setReviewStatus(reviewStatus: ReviewStatus): Promise<Stolperstein | null> {
    return this.update({ ...this, reviewStatus });
  }

  delete(): Promise<Stolperstein | null> {
    return this.update({ ...this, deleted: Date.now() });
  }

  undelete(): Promise<Stolperstein | null> {
    return this.update({ ...this, deleted: null });
  }

  static readonly typename = typename;

  static getRepository(): IReadRepository<Stolperstein> {
    return new ReadOnlyRepositoryWrapper(repository);
  }

  static restoreAlreadySaved(json: string): Stolperstein {
    const props = asStolpersteinProps(asDictionary(JSON.parse(json)));

    return new Stolperstein({
      ...props,
      ...asSanitizedContentProps(null, props),
    });
  }

  static async create(props: IStolpersteinProps): Promise<Stolperstein | null> {
    return repository.create(
      new Stolperstein(
        await asPropsWithValidatedReferences({
          ...props,
          ...asSanitizedContentProps(null, props),
        })
      )
    );
  }

  static async update(
    props: IStolpersteinProps & { id: string }
  ): Promise<Stolperstein | null> {
    return (await repository.findOne(props.id))?.update(props) || null; // -> canonical implementation
  }

  static async delete(id: string): Promise<Stolperstein | null> {
    return (await repository.findOne(id))?.delete() || null; // -> canonical implementation
  }

  static async undelete(id: string): Promise<Stolperstein | null> {
    return (await repository.findOne(id))?.undelete() || null; // -> canonical implementation
  }
}

const repository = wrapNodeRepository(
  new FsInMemoryStringRepository<Stolperstein>(
    "./data/" + Stolperstein.typename,
    Stolperstein.restoreAlreadySaved,
    (entity) => entity.jsonify(),
    "json"
  )
);
