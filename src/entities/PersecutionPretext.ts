import { Field, ObjectType } from "type-graphql";

import {
  FsInMemoryStringRepository,
  ReadOnlyRepositoryWrapper,
  IReadRepository,
} from "../../lib/repository";

import { asDictionary } from "../../lib/dictionary";

import {
  IPersecutionPretextProps,
  ISanitizedProps,
  Node,
  Locale,
  PersecutionPretextLocalization,
  asPersecutionPretextProps,
  asSanitizedNodeProps,
  asLocaleCode,
  asTrimmedNonEmptyString,
  wrapNodeRepository,
} from "../internal";

const typename = "PersecutionPretext";

type Typename = typeof typename;

const asPropsWithValidatedReferences = async (
  props: IPersecutionPretextProps & ISanitizedProps
): Promise<IPersecutionPretextProps & ISanitizedProps> => {
  const codes = [
    props.localeCode,
    ...props.localizations.map((v) => v.localeCode),
  ];

  const locales = await Locale.getRepository().find();

  for (const code of codes) {
    if (locales.findIndex((v) => v.code === code) === -1) {
      throw new Error("Not the code of a known Locale: " + code);
    }
  }

  return props;
};

/**
 * The class of all objects representing persecution pretexts.
 */
@ObjectType({
  implements: [Node],
  description: "An object representing a persecution pretext.",
})
export class PersecutionPretext extends Node<Typename, IPersecutionPretextProps>
  implements IPersecutionPretextProps {
  readonly typename = PersecutionPretext.typename;

  @Field()
  readonly localeCode: string;

  @Field()
  readonly label: string;

  @Field((_) => String, { nullable: true })
  readonly label_f: string | null;

  @Field((_) => String, { nullable: true })
  readonly label_m: string | null;

  @Field((_) => [PersecutionPretextLocalization])
  readonly localizations: PersecutionPretextLocalization[];

  constructor(props: IPersecutionPretextProps & ISanitizedProps) {
    super(props);

    const { localeCode, label, label_f, label_m, localizations } = props;

    this.localeCode = asLocaleCode(localeCode);

    this.label = asTrimmedNonEmptyString(label);

    this.label_f = label_f?.trim() || null;

    this.label_m = label_m?.trim() || null;

    this.localizations = localizations;
  }

  async update(
    props: IPersecutionPretextProps
  ): Promise<PersecutionPretext | null> {
    return repository.update(
      new PersecutionPretext(
        await asPropsWithValidatedReferences({
          ...props,
          ...asSanitizedNodeProps(this, props),
        })
      )
    );
  }

  delete(): Promise<PersecutionPretext | null> {
    return PersecutionPretext.delete(this.id);
  }

  static readonly typename = typename;

  static getRepository(): IReadRepository<PersecutionPretext> {
    return new ReadOnlyRepositoryWrapper(repository);
  }

  static restoreAlreadySaved(json: string): PersecutionPretext {
    const props = asPersecutionPretextProps(asDictionary(JSON.parse(json)));

    return new PersecutionPretext({
      ...props,
      ...asSanitizedNodeProps(null, props),
    });
  }

  static async create(
    props: IPersecutionPretextProps
  ): Promise<PersecutionPretext | null> {
    return repository.create(
      new PersecutionPretext(
        await asPropsWithValidatedReferences({
          ...props,
          ...asSanitizedNodeProps(null, props),
        })
      )
    );
  }

  static async update(
    props: IPersecutionPretextProps & { id: string }
  ): Promise<PersecutionPretext | null> {
    return (await repository.findOne(props.id))?.update(props) || null;
  }

  static delete(id: string): Promise<PersecutionPretext | null> {
    return repository.delete(id);
  }
}

const repository = wrapNodeRepository(
  new FsInMemoryStringRepository<PersecutionPretext>(
    "./data/" + PersecutionPretext.typename,
    PersecutionPretext.restoreAlreadySaved,
    (entity) => entity.jsonify(),
    "json"
  )
);
