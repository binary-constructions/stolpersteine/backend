import { InterfaceType, Field } from "type-graphql";

import {
  Content,
  IStolpersteinSubjectProps,
  ISanitizedProps,
  Stolperstein,
} from "../internal";

/**
 * A base class to use for implementations of `IStolpersteinSubject`.
 *
 * *Note:* this is created on top of `Content`, because even though
 * theoretically a stolperstein subject does not necessarily need to
 * be a content item, in practice it probably always will be.
 */
@InterfaceType("StolpersteinSubject", {
  description: "An object that can be the subject of a Stolperstein.",
})
export abstract class StolpersteinSubject<
  T extends string,
  U extends IStolpersteinSubjectProps<T>
> extends Content<T, U> implements IStolpersteinSubjectProps<T> {
  @Field((_) => [Stolperstein])
  readonly subjectOf?: Stolperstein[]; // -> meant to be resolved

  @Field((_) => [String])
  readonly subjectOfIds?: string[]; // -> meant to be resolved

  constructor(props: IStolpersteinSubjectProps<T> & ISanitizedProps) {
    super(props);
  }
}
