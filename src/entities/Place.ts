import { ObjectType, Field } from "type-graphql";

import {
  FsInMemoryStringRepository,
  ReadOnlyRepositoryWrapper,
  IReadRepository,
} from "../../lib/repository";

import { asDictionary } from "../../lib/dictionary";

import {
  IPlaceProps,
  ISanitizedProps,
  Node,
  PlaceType,
  asId,
  asPlaceProps,
  asTrimmedNonEmptyString,
  asSanitizedNodeProps,
  wrapNodeRepository,
} from "../internal";

const typename = "Place";

type Typename = typeof typename;

const asPropsWithValidatedReferences = async (
  props: IPlaceProps & ISanitizedProps
): Promise<IPlaceProps & ISanitizedProps> => {
  const { id, parentId } = props;

  if (!!parentId) {
    const parent = await repository.findOne(parentId);

    if (!parent) {
      throw new Error("The specified parent place could not be found.");
    }

    const parentIds = [parent.id];

    let nextParentAsChild = parent;

    // check for circular references
    while (nextParentAsChild.parentId !== null) {
      if (id && parent.id === id) {
        throw new Error("This would make the place a parent of itself.");
      }

      if (parentIds.findIndex((v) => v === nextParentAsChild.parentId) !== -1) {
        throw new Error("Circular parent/child relationship found.");
      }

      parentIds.push(nextParentAsChild.parentId);

      const grandParent = await repository.findOne(nextParentAsChild.parentId);

      if (!grandParent) {
        throw new Error("Invalid reference in parent chain.");
      }

      nextParentAsChild = grandParent;
    }
  }

  return props;
};

/**
 * The class of objects representing a labeled geographical area.
 */
@ObjectType({
  implements: [Node],
  description: "An object representing a labeled geographic area.",
})
export class Place extends Node<Typename, IPlaceProps> implements IPlaceProps {
  readonly typename = Place.typename;

  @Field()
  readonly label: string;

  @Field((_) => String, { nullable: true })
  readonly parentId: string | null;

  @Field((_) => PlaceType)
  readonly placeType: PlaceType = PlaceType.UNSPECIFIED; // FIXME: currently unused

  @Field((_) => [String], {
    description: "The list of alternative names for this Place.",
  })
  alternativeNames: string[];

  constructor(props: IPlaceProps & ISanitizedProps) {
    super(props);

    const { label, parentId, alternativeNames } = props;

    this.label = asTrimmedNonEmptyString(label);

    this.parentId = parentId ? asId(parentId) : null;

    this.alternativeNames = alternativeNames
      .map((v) => v.trim())
      .filter((v) => !!v);
  }

  async update(props: IPlaceProps): Promise<Place | null> {
    return repository.update(
      new Place(
        await asPropsWithValidatedReferences({
          ...props,
          ...asSanitizedNodeProps(this, props),
        })
      )
    );
  }

  delete(): Promise<Place | null> {
    return Place.delete(this.id);
  }

  static readonly typename = typename;

  static getRepository(): IReadRepository<Place> {
    return new ReadOnlyRepositoryWrapper(repository);
  }

  static restoreAlreadySaved(json: string): Place {
    const props = asPlaceProps(asDictionary(JSON.parse(json)));

    return new Place({
      ...props,
      ...asSanitizedNodeProps(null, props),
    });
  }

  static async create(props: IPlaceProps): Promise<Place | null> {
    return repository.create(
      new Place(
        await asPropsWithValidatedReferences({
          ...props,
          ...asSanitizedNodeProps(null, props),
        })
      )
    );
  }

  static async update(
    props: IPlaceProps & { id: string }
  ): Promise<Place | null> {
    return (await repository.findOne(props.id))?.update(props) || null;
  }

  static delete(id: string): Promise<Place | null> {
    return repository.delete(id);
  }
}

const repository = wrapNodeRepository(
  new FsInMemoryStringRepository<Place>(
    "./data/" + Place.typename,
    Place.restoreAlreadySaved,
    (entity) => entity.jsonify(),
    "json"
  )
);
