import { Field, InterfaceType } from "type-graphql";

import {
  Node,
  IContentProps,
  ReviewStatus,
  ISanitizedProps,
} from "../internal";

/**
 * A base class to use for implementations of `IContent`.
 */
@InterfaceType("Content", {
  description:
    "An object representing a primary unit of user-created and -changeable content.",
})
export abstract class Content<T extends string, U extends IContentProps<T>>
  extends Node<T, U>
  implements IContentProps<T> {
  readonly created: number | null;

  @Field((_) => Date, { name: "created", nullable: true })
  readonly createdDate?: Date | null; // -> meant to be resolved

  readonly lastModified: number | null;

  @Field((_) => Date, { name: "lastModified", nullable: true })
  readonly lastModifiedDate?: Date | null; // -> meant to be resolved

  readonly deleted: number | null;

  @Field((_) => Date, { name: "deleted", nullable: true })
  readonly deletedDate?: Date | null; // -> meant to be resolved

  @Field((_) => ReviewStatus)
  readonly reviewStatus: ReviewStatus;

  // FIXME: generalize for nodes/entities
  abstract update(props: IContentProps<T>): Promise<Content<T, U> | null>;

  // FIXME: turn the following into generic implementations using update()
  abstract setReviewStatus(
    reviewStatus: ReviewStatus
  ): Promise<Content<T, U> | null>;

  abstract delete(): Promise<Content<T, U> | null>;

  abstract undelete(): Promise<Content<T, U> | null>;

  constructor(props: IContentProps<T> & ISanitizedProps) {
    super(props);

    const { created, lastModified, deleted, reviewStatus } = props;

    if (created === undefined) {
      throw new Error(
        "The Content constructor requires 'created' to not be 'undefined'."
      );
    }

    this.created = created;

    this.lastModified = lastModified || null;

    this.deleted = deleted || null;

    this.reviewStatus = reviewStatus || ReviewStatus.UNDEFINED;
  }
}
