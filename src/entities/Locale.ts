import { ObjectType, Field } from "type-graphql";

import {
  FsInMemoryStringRepository,
  SelfKeyedEntityRepositoryWrapper,
  IReadRepository,
  ReadOnlyRepositoryWrapper,
} from "../../lib/repository";

import { asDictionary } from "../../lib/dictionary";

import {
  Entity,
  ILocaleProps,
  ISanitizedProps,
  asLocaleCode,
  asLocaleProps,
  asSanitizedEntityProps,
  asTrimmedNonEmptyString,
} from "../internal";

/**
 * The class of objects representing localization targets.
 */
@ObjectType({ description: "A localization target" })
export class Locale extends Entity<"Locale", ILocaleProps>
  implements ILocaleProps {
  readonly typename = Locale.typename;

  getKey() {
    return this.code;
  }

  @Field()
  readonly code: string;

  @Field()
  readonly label: string;

  constructor(props: ILocaleProps & ISanitizedProps) {
    super(props);

    const { code, label } = props;

    this.code = asLocaleCode(code);

    this.label = asTrimmedNonEmptyString(label);
  }

  async update(props: ILocaleProps): Promise<Locale | null> {
    if (props.code !== this.code) {
      throw new Error("The 'code' property of a Locale cannot be changed.");
    }

    return repository.update(
      new Locale({
        ...asSanitizedEntityProps(this, props),
        code: props.code,
        label: props.label,
      })
    );
  }

  delete(): Promise<Locale | null> {
    return Locale.delete(this.code);
  }

  static readonly typename = "Locale";

  static getRepository(): IReadRepository<Locale> {
    return new ReadOnlyRepositoryWrapper(repository);
  }

  static restoreAlreadySaved(json: string): Locale {
    const props = asLocaleProps(asDictionary(JSON.parse(json)));

    return new Locale({
      ...props,
      ...asSanitizedEntityProps(null, props),
    });
  }

  static async create(props: ILocaleProps): Promise<Locale | null> {
    return repository.create(
      new Locale({
        ...props,
        ...asSanitizedEntityProps(null, props),
      })
    );
  }

  static async update(props: ILocaleProps): Promise<Locale | null> {
    return (await repository.findOne(props.code))?.update(props) || null;
  }

  static async delete(_: string): Promise<Locale | null> {
    throw new Error("Locales are not currently allowed to be deleted!"); // FIXME?
  }
}

const repository = new SelfKeyedEntityRepositoryWrapper(
  new FsInMemoryStringRepository<Locale>(
    "./data/" + Locale.typename,
    Locale.restoreAlreadySaved,
    (entity) => entity.jsonify(),
    "json"
  ),
  (locale: Locale) => locale.code
);
