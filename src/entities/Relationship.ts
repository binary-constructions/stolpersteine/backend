import { ObjectType, Field, ID } from "type-graphql";

import {
  FsInMemoryStringRepository,
  ReadOnlyRepositoryWrapper,
  IReadRepository,
} from "../../lib/repository";

import { asDictionary } from "../../lib/dictionary";

import {
  RelationshipType,
  IRelationshipProps,
  ISanitizedProps,
  Node,
  wrapNodeRepository,
  asId,
  asRelationshipProps,
  asSanitizedNodeProps,
  stolpersteinSubjectRepository,
} from "../internal";

const typename = "Relationship";

type Typename = typeof typename;

const asPropsWithValidatedReferences = async (
  props: IRelationshipProps & ISanitizedProps
): Promise<IRelationshipProps & ISanitizedProps> => {
  if ((await stolpersteinSubjectRepository.findOne(props.subjectId)) === null) {
    throw new Error("the subject of the relationship doesn't seem to exist.");
  }

  if ((await stolpersteinSubjectRepository.findOne(props.objectId)) === null) {
    throw new Error("The object of the relationship doesn't seem to exist.");
  }

  if ((await RelationshipType.getRepository().findOne(props.typeId)) === null) {
    throw new Error("The relationship has an unknown type.");
  }

  // FIXME(?): currently the resolver deletes all matching relationships itself after(!) creating the new one
  // if (
  //   (await repository.find()).find(
  //     (v) =>
  //       v.id !== props.id &&
  //       ((v.subjectId === props.subjectId && v.objectId === props.objectId) ||
  //         (v.subjectId === props.objectId && v.objectId === props.subjectId))
  //   )
  // ) {
  //   throw new Error(
  //     "There already exists a relationship between those two that would need to be updated instead."
  //   );
  // }

  return props;
};

@ObjectType({
  implements: [Node],
  description: "A relationship between two StolpersteinSubject objects.",
})
export class Relationship extends Node<Typename, IRelationshipProps>
  implements IRelationshipProps {
  readonly typename = Relationship.typename;

  @Field((_) => ID)
  readonly subjectId: string;

  @Field((_) => ID)
  readonly objectId: string;

  @Field((_) => ID)
  readonly typeId: string;

  constructor(props: IRelationshipProps & ISanitizedProps) {
    super(props);

    const { subjectId, objectId, typeId } = props;

    this.subjectId = asId(subjectId);

    this.objectId = asId(objectId);

    this.typeId = asId(typeId);

    if (this.subjectId === this.objectId) {
      throw new Error(
        "Self-referencing relationships are not currently allowed."
      );
    }
  }

  async update(props: IRelationshipProps): Promise<Relationship | null> {
    return repository.update(
      new Relationship(
        await asPropsWithValidatedReferences({
          ...props,
          ...asSanitizedNodeProps(this, props),
        })
      )
    );
  }

  delete(): Promise<Relationship | null> {
    return Relationship.delete(this.id);
  }

  static readonly typename = typename;

  static getRepository(): IReadRepository<Relationship> {
    return new ReadOnlyRepositoryWrapper(repository);
  }

  static restoreAlreadySaved(json: string): Relationship {
    const props = asRelationshipProps(asDictionary(JSON.parse(json)));

    return new Relationship({
      ...props,
      ...asSanitizedNodeProps(null, props),
    });
  }

  static async create(props: IRelationshipProps): Promise<Relationship | null> {
    return repository.create(
      new Relationship(
        await asPropsWithValidatedReferences({
          ...props,
          ...asSanitizedNodeProps(null, props),
        })
      )
    );
  }

  static async update(
    props: IRelationshipProps & { id: string }
  ): Promise<Relationship | null> {
    return (await repository.findOne(props.id))?.update(props) || null;
  }

  static delete(id: string): Promise<Relationship | null> {
    return repository.delete(id);
  }
}

const repository = wrapNodeRepository(
  new FsInMemoryStringRepository<Relationship>(
    "./data/" + Relationship.typename,
    Relationship.restoreAlreadySaved,
    (entity) => entity.jsonify(),
    "json"
  )
);
