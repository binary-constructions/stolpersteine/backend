import { Field, ObjectType } from "type-graphql";

import {
  FsInMemoryStringRepository,
  ReadOnlyRepositoryWrapper,
  IReadRepository,
} from "../../lib/repository";

import { asDictionary } from "../../lib/dictionary";

import {
  IPersonProps,
  ISanitizedProps,
  Node,
  Content,
  StolpersteinSubject,
  Occupation,
  Gender,
  HistoricalDate,
  HistoricalDateScalar,
  PersonNameInterface,
  PersonTitle,
  PersecutionPretext,
  Place,
  Image,
  wrapNodeRepository,
  asSanitizedStolpersteinSubjectProps,
  asPersonProps,
  asId,
  ReviewStatus,
  Fate,
} from "../internal";

const typename = "Person";

type Typename = typeof typename;

const asPropsWithValidatedReferences = async (
  props: IPersonProps & ISanitizedProps
): Promise<IPersonProps & ISanitizedProps> => {
  for (const occupationId of props.occupationIds) {
    if ((await Occupation.getRepository().findOne(occupationId)) === null) {
      throw new Error("Not a valid occupation id: " + occupationId);
    }
  }

  if (
    props.bornPlaceId &&
    (await Place.getRepository().findOne(props.bornPlaceId)) === null
  ) {
    throw new Error("Not a valid place id: " + props.bornPlaceId);
  }

  if (
    props.diedPlaceId &&
    (await Place.getRepository().findOne(props.diedPlaceId)) === null
  ) {
    throw new Error("Not a valid place id: " + props.diedPlaceId);
  }

  for (const persecutionPretextId of props.persecutionPretextIds) {
    if (
      (await PersecutionPretext.getRepository().findOne(
        persecutionPretextId
      )) === null
    ) {
      throw new Error("Not a valid occupation id: " + persecutionPretextId);
    }
  }

  if (
    props.escapedToPlaceId &&
    (await Place.getRepository().findOne(props.escapedToPlaceId)) === null
  ) {
    throw new Error("Not a valid place id: " + props.bornPlaceId);
  }

  if (props.imageIds.length) {
    const images = await Image.getRepository().find();

    props.imageIds.forEach((v) => {
      if (images.findIndex((w) => w.id === v) === -1) {
        throw new Error("Not a valid image id: " + v);
      }
    });
  }

  return props;
};

/**
 * The class of all objects representing a person.
 */
@ObjectType({
  implements: [Node, Content, StolpersteinSubject],
  description: "An object representing a person.",
})
export class Person extends StolpersteinSubject<Typename, IPersonProps>
  implements IPersonProps {
  readonly typename = typename;

  @Field((_) => [PersonNameInterface])
  readonly names: PersonNameInterface[];

  @Field((_) => [String])
  readonly occupationIds: string[];

  @Field((_) => PersonTitle, { nullable: true })
  readonly title: PersonTitle | null;

  @Field((_) => Gender)
  readonly gender: Gender;

  @Field((_) => HistoricalDateScalar, { nullable: true })
  readonly bornDate: HistoricalDate | null;

  @Field((_) => String, { nullable: true })
  readonly bornPlaceId: string | null;

  @Field((_) => HistoricalDateScalar, { nullable: true })
  readonly diedDate: HistoricalDate | null;

  @Field((_) => String, { nullable: true })
  readonly diedPlaceId: string | null;

  @Field((_) => [String])
  readonly persecutionPretextIds: string[];

  @Field((_) => Fate)
  readonly fate: Fate;

  @Field((_) => String, { nullable: true })
  readonly escapedToPlaceId: string | null;

  @Field((_) => [String])
  readonly imageIds: string[];

  @Field((_) => String, {
    nullable: true,
    description: "The HTML-formatted biography text for the person.",
  })
  readonly biographyHTML: string | null;

  @Field((_) => String, {
    nullable: true,
    description: "HTML-formatted internal notes.",
  })
  readonly editingNotesHTML: string | null;

  constructor(props: IPersonProps & ISanitizedProps) {
    super(props);

    const {
      names,
      occupationIds,
      title,
      gender,
      bornDate,
      bornPlaceId,
      diedDate,
      diedPlaceId,
      persecutionPretextIds,
      fate,
      escapedToPlaceId,
      imageIds,
      biographyHTML,
      editingNotesHTML,
    } = props;

    this.names = names.filter((v) => v.value.trim());

    if (!this.names.length) {
      throw new Error("A Person needs at least one name.");
    }

    this.occupationIds = occupationIds.map((v) => asId(v));

    this.title = title?.value.trim() ? title : null;

    this.gender = gender;

    this.bornDate = bornDate;

    this.bornPlaceId = bornPlaceId ? asId(bornPlaceId) : null;

    this.diedDate = diedDate;

    this.diedPlaceId = diedPlaceId ? asId(diedPlaceId) : null;

    this.persecutionPretextIds = persecutionPretextIds.map((v) => asId(v));

    this.fate = fate;

    this.escapedToPlaceId = escapedToPlaceId ? asId(escapedToPlaceId) : null;

    this.imageIds = imageIds;

    this.biographyHTML = biographyHTML?.trim() || null;

    this.editingNotesHTML = editingNotesHTML?.trim() || null;
  }

  async update(props: IPersonProps): Promise<Person | null> {
    return repository.update(
      new Person(
        await asPropsWithValidatedReferences({
          ...props,
          ...asSanitizedStolpersteinSubjectProps(this, props),
        })
      )
    );
  }

  setReviewStatus(reviewStatus: ReviewStatus): Promise<Person | null> {
    return this.update({ ...this, reviewStatus });
  }

  delete(): Promise<Person | null> {
    return this.update({ ...this, deleted: Date.now() });
  }

  undelete(): Promise<Person | null> {
    return this.update({ ...this, deleted: null });
  }

  static readonly typename = typename;

  static getRepository(): IReadRepository<Person> {
    return new ReadOnlyRepositoryWrapper(repository);
  }

  static restoreAlreadySaved(json: string): Person {
    const props = asPersonProps(asDictionary(JSON.parse(json)));

    return new Person({
      ...props,
      ...asSanitizedStolpersteinSubjectProps(null, props),
    });
  }

  static async create(props: IPersonProps): Promise<Person | null> {
    return repository.create(
      new Person(
        await asPropsWithValidatedReferences({
          ...props,
          ...asSanitizedStolpersteinSubjectProps(null, props),
        })
      )
    );
  }

  static async update(
    props: IPersonProps & { id: string }
  ): Promise<Person | null> {
    return (await repository.findOne(props.id))?.update(props) || null; // -> canonical implementaion
  }

  static async delete(id: string): Promise<Person | null> {
    return (await repository.findOne(id))?.delete() || null; // -> canonical implementation
  }

  static async undelete(id: string): Promise<Person | null> {
    return (await repository.findOne(id))?.undelete() || null; // -> canonical implementation
  }
}

const repository = wrapNodeRepository(
  new FsInMemoryStringRepository<Person>(
    "./data/" + Person.typename,
    Person.restoreAlreadySaved,
    (entity) => entity.jsonify(),
    "json"
  )
);
