import { ObjectType, Field } from "type-graphql";

import {
  existsSync,
  mkdirSync,
  writeFileSync,
  readdirSync,
  renameSync,
} from "fs";

import {
  FsInMemoryStringRepository,
  ReadOnlyRepositoryWrapper,
  IReadRepository,
} from "../../lib/repository";

import { asDictionary } from "../../lib/dictionary";

import {
  IImageProps,
  Node,
  ISanitizedProps,
  asImageProps,
  asTrimmedNonEmptyString,
  asSanitizedNodeProps,
  asValidatedFileName,
  wrapNodeRepository,
} from "../internal";

const typename = "Image";

type Typename = typeof typename;

/**
 * The class of objects representing an Image.
 */
@ObjectType({
  implements: [Node],
  description: "An object representing an Image.",
})
export class Image extends Node<Typename, IImageProps> implements IImageProps {
  readonly typename = Image.typename;

  @Field((_) => String, {
    description:
      "Information on the authorship of the image and its copyright.",
  })
  readonly authorshipRemark: string;

  @Field((_) => String, {
    nullable: true,
    description: "The caption text for the image.",
  })
  readonly caption: string | null;

  @Field((_) => String, {
    nullable: true,
    description:
      "The text to use as alternative content for the image (e.g. when rendering for a braille display).",
  })
  readonly alternativeText: string | null;

  constructor(props: IImageProps & ISanitizedProps) {
    super(props);

    const {
      authorshipRemark,
      caption,
      alternativeText,
      data,
      fileName,
    } = props;

    if (data !== undefined) {
      throw new Error(
        "The image data should not be passed on to the constructor."
      );
    }

    if (fileName !== undefined) {
      throw new Error(
        "The image file name should not be passed on to the constructor."
      );
    }

    this.authorshipRemark = asTrimmedNonEmptyString(authorshipRemark);

    this.caption = caption?.trim() || null;

    this.alternativeText = alternativeText?.trim() || null;
  }

  async update(props: IImageProps): Promise<Image | null> {
    const sanitizedProps = {
      ...props,
      ...asSanitizedNodeProps(this, props),
    };

    const dir = `${__dirname}/../../public/images/${this.id}`;

    if (!existsSync(dir)) {
      throw new Error("Directory for image does not exists.");
    }

    const files = readdirSync(dir);

    if (files.length !== 1) {
      throw new Error(
        "There should be exactly one file in the image diretory."
      );
    }

    // replace the image if data is set
    if (sanitizedProps.data) {
      writeFileSync(`${dir}/${files[0]}`, sanitizedProps.data, {
        encoding: "base64",
      });
    }

    const fileName =
      sanitizedProps.fileName !== undefined
        ? asValidatedFileName(sanitizedProps.fileName)
        : undefined;

    // rename the file if fileName differs from the current one
    if (fileName && fileName !== files[0]) {
      renameSync(`${dir}/${files[0]}`, `${dir}/${fileName}`);
    }

    return repository.update(
      new Image({ ...sanitizedProps, fileName: undefined, data: undefined })
    );
  }

  delete(): Promise<Image | null> {
    return Image.delete(this.id); // -> canonical implementation
  }

  async getFileName(): Promise<string> {
    const dir = `${__dirname}/../../public/images/${this.id}`;

    if (!existsSync(dir)) {
      throw new Error("Directory for image does not exists.");
    }

    const files = readdirSync(dir);

    if (files.length !== 1) {
      throw new Error(
        "There should be exactly one file in the image diretory."
      );
    }

    return files[0];
  }

  async getPath(): Promise<string> {
    const fileName = await this.getFileName();

    return `/images/${this.id}/${encodeURIComponent(fileName)
      .replace(/'/g, "%27")
      .replace(/\(/g, "%28")
      .replace(/\)/g, "%29")}`;
  }

  static readonly typename = typename;

  static getRepository(): IReadRepository<Image> {
    return new ReadOnlyRepositoryWrapper(repository);
  }

  static restoreAlreadySaved(json: string): Image {
    const props = asImageProps(asDictionary(JSON.parse(json)));

    return new Image({
      ...props,
      ...asSanitizedNodeProps(null, props),
    });
  }

  static async create(props: IImageProps): Promise<Image | null> {
    const image = new Image({
      ...props,
      ...asSanitizedNodeProps(null, props),
      data: undefined,
      fileName: undefined,
    });

    if (props.id) {
      if (props.fileName !== undefined || props.data !== undefined) {
        throw new Error(
          "When craeting an image that already has an id the fileName and data props are not allowed."
        );
      }

      if (!existsSync(`${__dirname}/../../public/images/${props.id}`)) {
        throw new Error(
          "When creating an image that already has an id the image directory already needs to exist."
        );
      }
    } else {
      if (!props.data) {
        throw new Error("Data needs to be provided when creating a new image.");
      }

      const fileName = asValidatedFileName(props.fileName);

      const dir = `${__dirname}/../../public/images/${image.id}`;

      if (existsSync(dir)) {
        throw new Error("Directory for image already exists.");
      }

      mkdirSync(dir);

      writeFileSync(`${dir}/${fileName}`, props.data, { encoding: "base64" });
    }

    return repository.create(image);
  }

  static async update(
    props: IImageProps & { id: string }
  ): Promise<Image | null> {
    return (await repository.findOne(props.id))?.update(props) || null; // -> canonical implementation
  }

  static async delete(id: string): Promise<Image | null> {
    return repository.delete(id);
  }
}

const repository = wrapNodeRepository(
  new FsInMemoryStringRepository<Image>(
    "./data/" + Image.typename,
    Image.restoreAlreadySaved,
    (entity) => entity.jsonify(),
    "json"
  )
);
