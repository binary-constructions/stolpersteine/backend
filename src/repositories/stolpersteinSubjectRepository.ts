import { MultiReadRepositoryWrapper } from "../../lib/repository";

import { StolpersteinSubject } from "../internal";

export const stolpersteinSubjectRepository = new MultiReadRepositoryWrapper<
  StolpersteinSubject<any, any>
>();
