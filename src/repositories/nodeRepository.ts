import { MultiReadRepositoryWrapper } from "../../lib/repository";

import { Node } from "../internal";

export const nodeRepository = new MultiReadRepositoryWrapper<Node<any, any>>();
