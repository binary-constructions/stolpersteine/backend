import { MultiReadRepositoryWrapper } from "../../lib/repository";

import { Content } from "../internal";

export const contentRepository = new MultiReadRepositoryWrapper<
  Content<any, any>
>();
