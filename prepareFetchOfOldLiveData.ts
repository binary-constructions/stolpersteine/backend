import { existsSync, renameSync, mkdirSync } from "fs";

const importDir = "./import";

const dataDir = "./data";

const isoDate = new Date().toISOString();

if (!existsSync("./oldImports")) {
  mkdirSync("./oldImports");
}

const previousImportDir = `./oldImports/import.pre:${isoDate}`;

const previousDataDir = `./oldImports/data.pre:${isoDate}`;

if (existsSync(importDir)) {
  renameSync(importDir, previousImportDir);
}

if (existsSync(dataDir)) {
  renameSync(dataDir, previousDataDir);
}

mkdirSync(importDir);

mkdirSync(dataDir);

if (existsSync(previousImportDir + "/.git")) {
  renameSync(previousImportDir + "/.git", importDir + "/.git");
}

if (existsSync(previousDataDir + "/.git")) {
  renameSync(previousDataDir + "/.git", dataDir + "/.git");
}
