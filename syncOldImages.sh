#!/bin/bash

SOURCE="";

if [ -z "$SOURCE" ]; then
    echo "SOURCE needs to be configured in this script first!"

    exit 1
fi

rsync -avz "$SOURCE" public/images
