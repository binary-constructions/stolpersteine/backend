import {
  readFileSync,
  unlinkSync,
  writeFileSync,
  readdirSync,
  existsSync,
  mkdirSync,
} from "fs";
import { sep, join } from "path";

import { Repository } from "./Repository";

type EntityIndex<T> = {
  [key: string]: undefined | T;
};

/**
 * An implementation of the abstract `Repository` class that keeps all
 * stored entities in memory while at the same time persisting them as
 * individual text files to a directory.
 *
 * **Beware:** any entity returned by the repository methods should
 * either be considered immutable or otherwise needs to be
 * *explicitly* (and successfully!) saved after every change, because
 * otherwise the repository might get out of sync with its persistence
 * layer.
 */
export class FsInMemoryStringRepository<T> extends Repository<T> {
  private getEntities = (() => {
    const loadEntities = async (): Promise<EntityIndex<T>> => {
      const entities: EntityIndex<T> = {};

      const files = readdirSync(this.dir);

      for (const file of files) {
        if (this.extension && !file.endsWith("." + this.extension)) {
          continue; // file does not have the correct extension
        }

        const key = !this.extension
          ? file
          : file.slice(0, -(this.extension.length + 1));

        const entity = this.fromString(
          readFileSync(join(this.dir, file), "utf8")
        );

        entities[key] = entity instanceof Promise ? await entity : entity;
      }

      return entities;
    };

    let entitiesPromise: undefined | Promise<EntityIndex<T>> = undefined;

    return () => {
      if (entitiesPromise === undefined) {
        entitiesPromise = loadEntities();
      }

      return entitiesPromise;
    };
  })(); // applied for closure

  private getEntityFilePath(key: string): string {
    // TODO: validate key

    if (this.extension) {
      return this.dir + sep + key + "." + this.extension;
    } else {
      return this.dir + sep + key;
    }
  }

  constructor(
    private readonly dir: string, // TODO: validate

    private readonly fromString: (value: string) => Promise<T> | T,

    private readonly toString: (entity: T) => Promise<string> | string,

    private readonly extension?: string // TODO: validate
  ) {
    super();

    if (!existsSync(dir)) {
      mkdirSync(dir, { recursive: true });
    }
  }

  async find(): Promise<T[]> {
    const entities = await this.getEntities();

    return Object.values(entities).filter((v): v is T => v !== undefined);
  }

  async findOne(key: string): Promise<T | null> {
    const entities = await this.getEntities();

    return entities[key] || null;
  }

  async create(key: string, entity: T): Promise<T | null> {
    const entities = await this.getEntities();

    if (entities[key] !== undefined) {
      return null;
    }

    const entityStringOrPromise = this.toString(entity);

    const entityString =
      entityStringOrPromise instanceof Promise
        ? await entityStringOrPromise
        : entityStringOrPromise;

    writeFileSync(this.getEntityFilePath(key), entityString);

    const entityOrPromise = this.fromString(entityString);

    entities[key] =
      entityOrPromise instanceof Promise
        ? await entityOrPromise
        : entityOrPromise;

    return entities[key] || null;
  }

  async update(key: string, entity: T): Promise<T | null> {
    const entities = await this.getEntities();

    if (entities[key] === undefined) {
      return null;
    }

    const entityStringOrPromise = this.toString(entity);

    const entityString =
      entityStringOrPromise instanceof Promise
        ? await entityStringOrPromise
        : entityStringOrPromise;

    writeFileSync(this.getEntityFilePath(key), entityString);

    const entityOrPromise = this.fromString(entityString);

    entities[key] =
      entityOrPromise instanceof Promise
        ? await entityOrPromise
        : entityOrPromise;

    return entities[key] || null;
  }

  async delete(key: string): Promise<T | null> {
    const entities = await this.getEntities();

    const entity = entities[key];

    if (entity === undefined) {
      return null;
    }

    unlinkSync(this.getEntityFilePath(key));

    delete entities[key];

    return entity;
  }
}
