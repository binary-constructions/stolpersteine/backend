import { IWriteRepository } from "./IWriteRepository";
import { IReadRepository } from "./IReadRepository";

export abstract class Repository<T>
  implements IReadRepository<T>, IWriteRepository<T> {
  find(): Promise<T[]> {
    throw new Error("Method not implemented.");
  }

  findOne(key: string): Promise<T | null> {
    throw new Error("Method not implemented.");
  }

  create(key: string, entity: T): Promise<T | null> {
    throw new Error("Method not implemented.");
  }

  update(key: string, entity: T): Promise<T | null> {
    throw new Error("Method not implemented.");
  }

  delete(key: string): Promise<T | null> {
    throw new Error("Method not implemented.");
  }
}
