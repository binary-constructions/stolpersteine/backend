import { IReadRepository } from "./IReadRepository";

export class ReadOnlyRepositoryWrapper<T> {
  repository: IReadRepository<T>;

  constructor(repository: IReadRepository<T>) {
    this.repository = repository;
  }

  find(): Promise<T[]> {
    return this.repository.find();
  }

  findOne(key: string): Promise<T | null> {
    return this.repository.findOne(key);
  }
}
