import { IReadRepository } from "./IReadRepository";

export class MultiReadRepositoryWrapper<T> {
  repositories: IReadRepository<T>[] = [];

  addRepository(repository: IReadRepository<T>) {
    this.repositories.push(repository);
  }

  async find(): Promise<T[]> {
    const nestedResults: T[][] = [];

    for (const repository of this.repositories) {
      nestedResults.push(await repository.find());
    }

    return ([] as T[]).concat(...nestedResults);
  }

  async findOne(key: string): Promise<T | null> {
    for (const repository of this.repositories) {
      const match = await repository.findOne(key);

      if (match !== null) {
        return match;
      }
    }

    return null;
  }
}
