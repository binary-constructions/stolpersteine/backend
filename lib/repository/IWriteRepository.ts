/**
 * The interface of repositories allowing read access.
 */
export interface IWriteRepository<T> {
  /**
   * Store the provided entity in the repository and associate it with
   * the specified key.
   *
   * Returns the provided entity again or `null` if `key` is already
   * associated with another entity.
   *
   * Might throw an error for other problems like the inability to
   * persist the created entity.
   */
  create(key: string, entity: T): Promise<T | null>;

  /**
   * Replace the entity associated with the specified key with the one
   * provided.
   *
   * Returns the provided entity again or `null` if `key` is not
   * currently associated with any.
   *
   * Might throw an error for other problems like the inability to
   * persist the updated entity.
   */
  update(key: string, entity: T): Promise<T | null>;

  /**
   * Delete the entity associated with the specified key.
   *
   * Returns the deleted entity or `null` if `key` is not currently
   * associated with any.
   *
   * Might throw an error for other problems like the inability to
   * actually delete an existing entity.
   */
  delete(key: string): Promise<T | null>;
}
