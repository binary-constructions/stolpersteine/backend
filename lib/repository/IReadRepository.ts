/**
 * The interface of repositories allowing read access.
 */
export interface IReadRepository<T> {
  /**
   * Return all entities stored in the repository.
   *
   * Might throw an error for problems like a failure to access the
   * persistence layer.
   */
  find(): Promise<T[]>;

  /**
   * Return the entity associated with the specified `key`, or `null`
   * if no such entity exists.
   *
   * Might throw an error for other problems like a failure to access
   * the persistence layer.
   */
  findOne(key: string): Promise<T | null>;
}
