import { Repository } from "./Repository";

export class SelfKeyedEntityRepositoryWrapper<T> {
  repository: Repository<T>;

  getEntityKey: (entity: T) => string;

  constructor(repository: Repository<T>, getEntityKey: (entity: T) => string) {
    this.repository = repository;

    this.getEntityKey = getEntityKey;
  }

  find(): Promise<T[]> {
    return this.repository.find();
  }

  findOne(key: string): Promise<T | null> {
    return this.repository.findOne(key);
  }

  create(entity: T): Promise<T | null> {
    return this.repository.create(this.getEntityKey(entity), entity);
  }

  update(entity: T): Promise<T | null> {
    return this.repository.update(this.getEntityKey(entity), entity);
  }

  delete(key: string): Promise<T | null> {
    return this.repository.delete(key);
  }
}
