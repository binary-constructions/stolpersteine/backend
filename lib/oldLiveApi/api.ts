import { GraphQLClient } from 'graphql-request';
import { print } from 'graphql';
import gql from 'graphql-tag';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** The javascript `Date` as string. Type represents date and time as the ISO Date string. */
  DateTime: any;
  /**
   * A date that may be exact to the day ("YYYY-MM-DD"), less specific ("YYYY-MM" or
   * "YYYY", meaning "sometime in") or a time-frame ("<earliest>/<latest>", meaning
   * "in-between").  There may also be a prefix of "~", "?" or "~?" to mark the whole
   * expression as an approximation, a presumption or an approximate presumption
   * respectively.  (Note: in the future ISO-style times and time-zones might also be
   * allowed, so that should be taken into account when parsing.)
   */
  HistoricalDate: any;
};

/** A family name given to a person at birth (as opposed to by marriage). */
export type BirthName = PersonName & TypedLiteral & {
  __typename?: 'BirthName';
  value: Scalars['String'];
};

/** An object representing a primary unit of user-created and -changeable content. */
export type Content = {
  created?: Maybe<Scalars['DateTime']>;
  lastModified?: Maybe<Scalars['DateTime']>;
  deleted?: Maybe<Scalars['DateTime']>;
  reviewStatus: ReviewStatus;
};


/**
 * A name indicating the family affiliation of a person (without further
 * information on whether it was assumed at birth or at marriage).
 */
export type FamilyName = PersonName & TypedLiteral & {
  __typename?: 'FamilyName';
  value: Scalars['String'];
};

/**
 * Gender identities as either actually or at least presumably attributed to a
 * person by themselves and/or others (falling back to classical ideas of gender
 * dichotomy in cases where nothing to the contrary is specifically known).
 */
export enum Gender {
  Unspecified = 'UNSPECIFIED',
  NonBinary = 'NON_BINARY',
  Female = 'FEMALE',
  Male = 'MALE'
}

/** A geographic point on the map. */
export type GeoPoint = {
  __typename?: 'GeoPoint';
  longitude: Scalars['Float'];
  latitude: Scalars['Float'];
};

/** A name given to a person usually at or soon after birth. */
export type GivenName = PersonName & TypedLiteral & {
  __typename?: 'GivenName';
  value: Scalars['String'];
};


/** An object representing an Image. */
export type Image = Node & {
  __typename?: 'Image';
  id: Scalars['ID'];
  /** Information on the authorship of the image and its copyright. */
  authorshipRemark: Scalars['String'];
  /** The caption text for the image. */
  caption?: Maybe<Scalars['String']>;
  /** The text to use as alternative content for the image (e.g. when rendering for a braille display). */
  alternativeText?: Maybe<Scalars['String']>;
  fileName: Scalars['String'];
  path: Scalars['String'];
};

export type ImageInput = {
  /**
   * The base64 encoded image data (needs to be set for new images and only should
   * be set for existing ones if the image data changes).
   */
  data?: Maybe<Scalars['String']>;
  /** The file name of the image (needs to be set for new images but is optional for existing ones). */
  fileName?: Maybe<Scalars['String']>;
  /** Information on the authorship of the image and its copyright. */
  authorshipRemark: Scalars['String'];
  /** The caption text for the image. */
  caption?: Maybe<Scalars['String']>;
  /** The text to use as alternative content for the image (e.g. when rendering for a braille display). */
  alternativeText?: Maybe<Scalars['String']>;
};

/** A localization target */
export type Locale = {
  __typename?: 'Locale';
  code: Scalars['String'];
  label: Scalars['String'];
};

/** A family name given to a person at marriage. */
export type MarriedName = PersonName & TypedLiteral & {
  __typename?: 'MarriedName';
  value: Scalars['String'];
};

export type Mutation = {
  __typename?: 'Mutation';
  /** Mark the Content entity with the specified id as deleted. */
  delete: Content;
  /** Change the review status of the Content entity with the specified id. */
  setReviewStatus: Content;
  /** Add a new Locale. */
  addLocale: Locale;
  /** Create or update a Relationship between two StolpersteinSubject objects. */
  setRelationship: Relationship;
  /** Remove a Relationship between two StolpersteinSubject objects (inverted or not). */
  unsetRelationship?: Maybe<Relationship>;
  /** Create a new Place object. */
  createPlace: Place;
  /** Restore a previously existing Place object. */
  restorePlace: Place;
  /** Update an existing Place object. */
  updatePlace: Place;
  /** Create a new Spot object. */
  createSpot: Spot;
  /** Restore a previously existing Spot object. */
  restoreSpot: Spot;
  /** Mark the Spot and all associated Stolperstein and StolpersteinSubject entities as deleted. */
  deepDeleteSpot: Spot;
  /** Update an existing Spot object. */
  updateSpot: Spot;
  /** Create a new Image object. */
  createImage: Image;
  /**
   * Restore a previously existing Image object.  (Note: leave the data field empty
   * if image directory and file have already been restored separately from the
   * other image details provided here.)
   */
  restoreImage: Image;
  /** Update an existing Image object. */
  updateImage: Image;
  /** Create a new Occupation object. */
  createOccupation: Occupation;
  /** Restore a previously existing Occupation object. */
  restoreOccupation: Occupation;
  /** Update an existing Occupation object. */
  updateOccupation: Occupation;
  /** Create a new RelationshipType object. */
  createRelationshipType: RelationshipType;
  /** Restore a previously existing RelationshipType object. */
  restoreRelationshipType: RelationshipType;
  /** Update an existing RelationshipType object. */
  updateRelationshipType: RelationshipType;
  /** Create a new PersecutionPretext object. */
  createPersecutionPretext: PersecutionPretext;
  /** Restore a previously existing PersecutionPretext object. */
  restorePersecutionPretext: PersecutionPretext;
  /** Update an existing PersecutionPretext object. */
  updatePersecutionPretext: PersecutionPretext;
  /** Create a new Person object. */
  createPerson: Person;
  /** Restore a previously existing Person object. */
  restorePerson: Person;
  /** Update an existing Person object. */
  updatePerson: Person;
  /** Create a new Stolperstein object. */
  createStolperstein: Stolperstein;
  /** Restore a previously existing Stolperstein object. */
  restoreStolperstein: Stolperstein;
  /** Change the ReviewStatus of the Stolperstein with the specified id. */
  setStolpersteinReviewStatus: Stolperstein;
  /** Mark the Stolperstein and all associated StolpersteinSubject entities as deleted. */
  deepDeleteStolperstein: Stolperstein;
  /** Update an existing Stolperstein object. */
  updateStolperstein: Stolperstein;
};


export type MutationDeleteArgs = {
  id: Scalars['String'];
};


export type MutationSetReviewStatusArgs = {
  status: ReviewStatus;
  id: Scalars['String'];
};


export type MutationAddLocaleArgs = {
  label: Scalars['String'];
  code: Scalars['String'];
};


export type MutationSetRelationshipArgs = {
  typeId: Scalars['String'];
  objectId: Scalars['String'];
  subjectId: Scalars['String'];
};


export type MutationUnsetRelationshipArgs = {
  otherId: Scalars['String'];
  oneId: Scalars['String'];
};


export type MutationCreatePlaceArgs = {
  input: PlaceInput;
};


export type MutationRestorePlaceArgs = {
  input: PlaceInput;
  id: Scalars['String'];
};


export type MutationUpdatePlaceArgs = {
  input: PlaceInput;
  id: Scalars['String'];
};


export type MutationCreateSpotArgs = {
  input: SpotInput;
};


export type MutationRestoreSpotArgs = {
  input: SpotInput;
  id: Scalars['String'];
};


export type MutationDeepDeleteSpotArgs = {
  id: Scalars['String'];
};


export type MutationUpdateSpotArgs = {
  input: SpotInput;
  id: Scalars['String'];
};


export type MutationCreateImageArgs = {
  input: ImageInput;
};


export type MutationRestoreImageArgs = {
  input: ImageInput;
  id: Scalars['String'];
};


export type MutationUpdateImageArgs = {
  input: ImageInput;
  id: Scalars['String'];
};


export type MutationCreateOccupationArgs = {
  input: OccupationInput;
};


export type MutationRestoreOccupationArgs = {
  input: OccupationInput;
  id: Scalars['String'];
};


export type MutationUpdateOccupationArgs = {
  input: OccupationInput;
  id: Scalars['String'];
};


export type MutationCreateRelationshipTypeArgs = {
  input: RelationshipTypeInput;
};


export type MutationRestoreRelationshipTypeArgs = {
  input: RelationshipTypeInput;
  id: Scalars['String'];
};


export type MutationUpdateRelationshipTypeArgs = {
  input: RelationshipTypeInput;
  id: Scalars['String'];
};


export type MutationCreatePersecutionPretextArgs = {
  input: PersecutionPretextInput;
};


export type MutationRestorePersecutionPretextArgs = {
  input: PersecutionPretextInput;
  id: Scalars['String'];
};


export type MutationUpdatePersecutionPretextArgs = {
  input: PersecutionPretextInput;
  id: Scalars['String'];
};


export type MutationCreatePersonArgs = {
  input: PersonInput;
};


export type MutationRestorePersonArgs = {
  input: PersonInput;
  id: Scalars['String'];
};


export type MutationUpdatePersonArgs = {
  input: PersonInput;
  id: Scalars['String'];
};


export type MutationCreateStolpersteinArgs = {
  input: StolpersteinInput;
};


export type MutationRestoreStolpersteinArgs = {
  input: StolpersteinInput;
  id: Scalars['String'];
};


export type MutationSetStolpersteinReviewStatusArgs = {
  status: ReviewStatus;
  id: Scalars['String'];
};


export type MutationDeepDeleteStolpersteinArgs = {
  id: Scalars['String'];
};


export type MutationUpdateStolpersteinArgs = {
  input: StolpersteinInput;
  id: Scalars['String'];
};

/** The type of a name of a person. */
export enum NameType {
  Given = 'GIVEN',
  Nickname = 'NICKNAME',
  Pseudonym = 'PSEUDONYM',
  Family = 'FAMILY',
  Birth = 'BIRTH',
  Married = 'MARRIED'
}

/** A name given to a person by friends or others. */
export type Nickname = PersonName & TypedLiteral & {
  __typename?: 'Nickname';
  value: Scalars['String'];
};

/** An object uniquely identified by an ID. */
export type Node = {
  id: Scalars['ID'];
};

/** An object representing a persons occupation. */
export type Occupation = Node & {
  __typename?: 'Occupation';
  id: Scalars['ID'];
  locale: Locale;
  label: Scalars['String'];
  label_f?: Maybe<Scalars['String']>;
  label_m?: Maybe<Scalars['String']>;
  localizations: Array<OccupationLocalization>;
};

export type OccupationInput = {
  locale: Scalars['String'];
  label: Scalars['String'];
  label_f?: Maybe<Scalars['String']>;
  label_m?: Maybe<Scalars['String']>;
  localizations?: Maybe<Array<OccupationLocalizationInput>>;
};

/** A localization for an Occupation object. */
export type OccupationLocalization = {
  __typename?: 'OccupationLocalization';
  locale: Locale;
  label: Scalars['String'];
  label_f?: Maybe<Scalars['String']>;
  label_m?: Maybe<Scalars['String']>;
};

export type OccupationLocalizationInput = {
  locale: Scalars['String'];
  label: Scalars['String'];
  label_f?: Maybe<Scalars['String']>;
  label_m?: Maybe<Scalars['String']>;
};

/** An object representing a persecution pretext. */
export type PersecutionPretext = Node & {
  __typename?: 'PersecutionPretext';
  id: Scalars['ID'];
  locale: Locale;
  label: Scalars['String'];
  label_f?: Maybe<Scalars['String']>;
  label_m?: Maybe<Scalars['String']>;
  localizations: Array<PersecutionPretextLocalization>;
};

export type PersecutionPretextInput = {
  locale: Scalars['String'];
  label: Scalars['String'];
  label_f?: Maybe<Scalars['String']>;
  label_m?: Maybe<Scalars['String']>;
  localizations?: Maybe<Array<PersecutionPretextLocalizationInput>>;
};

/** A localization for a PersecutionPretext object. */
export type PersecutionPretextLocalization = {
  __typename?: 'PersecutionPretextLocalization';
  locale: Locale;
  label: Scalars['String'];
  label_f?: Maybe<Scalars['String']>;
  label_m?: Maybe<Scalars['String']>;
};

export type PersecutionPretextLocalizationInput = {
  locale: Scalars['String'];
  label: Scalars['String'];
  label_f?: Maybe<Scalars['String']>;
  label_m?: Maybe<Scalars['String']>;
};

/** An object representing a person. */
export type Person = Node & Content & StolpersteinSubject & {
  __typename?: 'Person';
  id: Scalars['ID'];
  created?: Maybe<Scalars['DateTime']>;
  lastModified?: Maybe<Scalars['DateTime']>;
  deleted?: Maybe<Scalars['DateTime']>;
  reviewStatus: ReviewStatus;
  subjectOf: Array<Stolperstein>;
  names: Array<PersonName>;
  occupations: Array<Occupation>;
  title?: Maybe<PersonTitle>;
  gender: Gender;
  bornDate?: Maybe<Scalars['HistoricalDate']>;
  diedDate?: Maybe<Scalars['HistoricalDate']>;
  persecutionPretexts: Array<PersecutionPretext>;
  /** The HTML-formatted biography text for the person. */
  biographyHTML?: Maybe<Scalars['String']>;
  /** HTML-formatted internal notes. */
  editingNotesHTML?: Maybe<Scalars['String']>;
  /** Get the place the person was born at, if known. */
  bornPlace?: Maybe<Place>;
  /** Get the place the person died at, if known. */
  diedPlace?: Maybe<Place>;
};

export type PersonInput = {
  /** A list of the names of the person in display order.  Must contain at least one name. */
  names: Array<PersonNameInput>;
  /** A list of the IDs of the occupations of the person. */
  occupationIds?: Maybe<Array<Scalars['String']>>;
  /** A list of the IDs of the pretexts used in persecution of the person. */
  persecutionPretextIds?: Maybe<Array<Scalars['String']>>;
  title?: Maybe<Scalars['String']>;
  gender?: Maybe<Gender>;
  /** The date the person was born. */
  bornDate?: Maybe<Scalars['HistoricalDate']>;
  /** The ID of the place that the person was born at. */
  bornPlaceId?: Maybe<Scalars['String']>;
  /** The date the person died. */
  diedDate?: Maybe<Scalars['HistoricalDate']>;
  /** The ID of the place that the person died at. */
  diedPlaceId?: Maybe<Scalars['String']>;
  /** The HTML-formatted biography text for the person. */
  biographyHTML?: Maybe<Scalars['String']>;
  /** HTML-formatted internal notes. */
  editingNotesHTML?: Maybe<Scalars['String']>;
};

/** The name of a person. */
export type PersonName = {
  value: Scalars['String'];
};

export type PersonNameInput = {
  name: Scalars['String'];
  type: NameType;
};

/** Condensed information on a Person. */
export type PersonSummary = {
  __typename?: 'PersonSummary';
  id: Scalars['String'];
  created?: Maybe<Scalars['DateTime']>;
  lastModified?: Maybe<Scalars['DateTime']>;
  deleted?: Maybe<Scalars['DateTime']>;
  reviewStatus?: Maybe<ReviewStatus>;
  stolpersteinIds: Array<Scalars['String']>;
  names: Array<PersonName>;
  title?: Maybe<Scalars['String']>;
  bornDate?: Maybe<Scalars['String']>;
  diedDate?: Maybe<Scalars['String']>;
};

/** The title of a person. */
export type PersonTitle = TypedLiteral & {
  __typename?: 'PersonTitle';
  value: Scalars['String'];
};

/** An object representing a defined geographic area. */
export type Place = Node & {
  __typename?: 'Place';
  id: Scalars['ID'];
  label: Scalars['String'];
  placeType: PlaceType;
  /** The list of alternative names for this Place. */
  alternativeNames: Array<Scalars['String']>;
  /** Get the parent of this Place, if any. */
  parent?: Maybe<Place>;
  /** Get an ordered list of all the parents of this Place. */
  parents?: Maybe<Array<Place>>;
  /** Get a list of all the children of this Place. */
  children?: Maybe<Array<Place>>;
};

export type PlaceInput = {
  label: Scalars['String'];
  /** The ID of a place containing this one. */
  parentId?: Maybe<Scalars['String']>;
  /** The list of alternative names that this place is known by. */
  alternativeNames?: Maybe<Array<Scalars['String']>>;
};

/** Condensed information on a place as well as aassociated persons and Stolpersteine. */
export type PlaceSummary = {
  __typename?: 'PlaceSummary';
  id: Scalars['String'];
  label: Scalars['String'];
  parentId?: Maybe<Scalars['String']>;
  alternativeNames: Array<Scalars['String']>;
};

/** The type of a geographic place. */
export enum PlaceType {
  Unspecified = 'UNSPECIFIED',
  Country = 'COUNTRY',
  Province = 'PROVINCE',
  RuralDistrict = 'RURAL_DISTRICT',
  Locality = 'LOCALITY',
  District = 'DISTRICT'
}

/** A name assumed by a person publicly for a particular purpose. */
export type Pseudonym = PersonName & TypedLiteral & {
  __typename?: 'Pseudonym';
  value: Scalars['String'];
};

export type Query = {
  __typename?: 'Query';
  /** Get the Locale object with the specified ID if it exists. */
  locale?: Maybe<Locale>;
  /** Get a list of all Locale objects. */
  locales: Array<Locale>;
  /**
   * Get the (non-inverted) Relationship between the two specified
   * StolpersteinSubject objects.  (Inverse relationships need to be queried separately!)
   */
  relationship?: Maybe<Relationship>;
  /** Get a list of all Relationships. */
  relationships: Array<Relationship>;
  /** Get a list of all Relationships with the specified ID as the subject. */
  relationshipsWithSubject: Array<Relationship>;
  /** Get a list of all Relationships with the specified ID as the object. */
  relationshipsWithObject: Array<Relationship>;
  spotSummaries: Array<SpotSummary>;
  placeSummaries: Array<PlaceSummary>;
  /** Get the Node object with the specified ID if it exists. */
  node?: Maybe<Node>;
  /** Get a list of all Node objects. */
  nodes: Array<Node>;
  /** Get the Content object with the specified ID if it exists. */
  content?: Maybe<Content>;
  /** Get a list of all Content objects. */
  contents: Array<Content>;
  /** Get the StolpersteinSubject object with the specified ID if it exists. */
  stolpersteinSubject?: Maybe<StolpersteinSubject>;
  /** Get a list of all StolpersteinSubject objects. */
  stolpersteinSubjects: Array<StolpersteinSubject>;
  /** Get the Place object with the specified ID if it exists. */
  place?: Maybe<Place>;
  /** Get a list of all Place objects. */
  places: Array<Place>;
  /** Get a list of all Place objects without a parent. */
  placesByParent: Array<Place>;
  /** Get the Spot object with the specified ID if it exists. */
  spot?: Maybe<Spot>;
  /** Get a list of all (matching) Spot objects. */
  spots: Array<Spot>;
  /** Get the Image object with the specified ID if it exists. */
  image?: Maybe<Image>;
  /** Get a list of all (matching) Image objects. */
  images: Array<Image>;
  /** Get the Occupation object with the specified ID if it exists. */
  occupation?: Maybe<Occupation>;
  /** Get a list of all Occupation objects. */
  occupations: Array<Occupation>;
  /** Get the RelationshipType object with the specified ID if it exists. */
  relationshipType?: Maybe<RelationshipType>;
  /** Get a list of all RelationshipType objects. */
  relationshipTypes: Array<RelationshipType>;
  /** Get the PersecutionPretext object with the specified ID if it exists. */
  persecutionPretext?: Maybe<PersecutionPretext>;
  /** Get a list of all PersecutionPretext objects. */
  persecutionPretexts: Array<PersecutionPretext>;
  /** Get the Person object with the specified ID if it exists. */
  person?: Maybe<Person>;
  /** Get a list of all (matching) Person objects. */
  persons: Array<Person>;
  /** Get the Stolperstein object with the specified ID if it exists. */
  stolperstein?: Maybe<Stolperstein>;
  /** Get a list of all (matching) Stolperstein objects. */
  stolpersteine: Array<Stolperstein>;
};


export type QueryLocaleArgs = {
  code: Scalars['String'];
};


export type QueryRelationshipArgs = {
  objectId: Scalars['String'];
  subjectId: Scalars['String'];
};


export type QueryRelationshipsWithSubjectArgs = {
  subjectId: Scalars['String'];
};


export type QueryRelationshipsWithObjectArgs = {
  objectId: Scalars['String'];
};


export type QueryNodeArgs = {
  id: Scalars['ID'];
};


export type QueryContentArgs = {
  id: Scalars['String'];
};


export type QueryStolpersteinSubjectArgs = {
  id: Scalars['String'];
};


export type QueryPlaceArgs = {
  id: Scalars['String'];
};


export type QueryPlacesByParentArgs = {
  id?: Maybe<Scalars['String']>;
};


export type QuerySpotArgs = {
  id: Scalars['String'];
};


export type QuerySpotsArgs = {
  ids?: Maybe<Array<Scalars['String']>>;
};


export type QueryImageArgs = {
  id: Scalars['String'];
};


export type QueryImagesArgs = {
  ids?: Maybe<Array<Scalars['String']>>;
};


export type QueryOccupationArgs = {
  id: Scalars['String'];
};


export type QueryRelationshipTypeArgs = {
  id: Scalars['String'];
};


export type QueryPersecutionPretextArgs = {
  id: Scalars['String'];
};


export type QueryPersonArgs = {
  id: Scalars['String'];
};


export type QueryPersonsArgs = {
  ids?: Maybe<Array<Scalars['String']>>;
};


export type QueryStolpersteinArgs = {
  id: Scalars['String'];
};


export type QueryStolpersteineArgs = {
  ids?: Maybe<Array<Scalars['String']>>;
};

/** A relationship between two StolpersteinSubject objects. */
export type Relationship = {
  __typename?: 'Relationship';
  type: RelationshipType;
  subject: StolpersteinSubject;
  object: StolpersteinSubject;
};

/** An object representing a type of relationship. */
export type RelationshipType = Node & {
  __typename?: 'RelationshipType';
  id: Scalars['ID'];
  locale: Locale;
  subjectLabel: Scalars['String'];
  subjectLabel_f?: Maybe<Scalars['String']>;
  subjectLabel_m?: Maybe<Scalars['String']>;
  objectLabel: Scalars['String'];
  objectLabel_f?: Maybe<Scalars['String']>;
  objectLabel_m?: Maybe<Scalars['String']>;
  localizations: Array<RelationshipTypeLocalization>;
};

export type RelationshipTypeInput = {
  locale: Scalars['String'];
  subjectLabel: Scalars['String'];
  subjectLabel_f?: Maybe<Scalars['String']>;
  subjectLabel_m?: Maybe<Scalars['String']>;
  objectLabel: Scalars['String'];
  objectLabel_f?: Maybe<Scalars['String']>;
  objectLabel_m?: Maybe<Scalars['String']>;
  localizations?: Maybe<Array<RelationshipTypeLocalizationInput>>;
};

/** A localization object for a RelationshipType object. */
export type RelationshipTypeLocalization = {
  __typename?: 'RelationshipTypeLocalization';
  locale: Locale;
  subjectLabel: Scalars['String'];
  subjectLabel_f?: Maybe<Scalars['String']>;
  subjectLabel_m?: Maybe<Scalars['String']>;
  objectLabel: Scalars['String'];
  objectLabel_f?: Maybe<Scalars['String']>;
  objectLabel_m?: Maybe<Scalars['String']>;
};

export type RelationshipTypeLocalizationInput = {
  locale: Scalars['String'];
  subjectLabel: Scalars['String'];
  subjectLabel_f?: Maybe<Scalars['String']>;
  subjectLabel_m?: Maybe<Scalars['String']>;
  objectLabel: Scalars['String'];
  objectLabel_f?: Maybe<Scalars['String']>;
  objectLabel_m?: Maybe<Scalars['String']>;
};

/** The review status of a content item. */
export enum ReviewStatus {
  Undefined = 'UNDEFINED',
  Stub = 'STUB',
  WorkInProgress = 'WORK_IN_PROGRESS',
  NeedsReview = 'NEEDS_REVIEW',
  Released = 'RELEASED'
}

/** An object representing a geographic spot. */
export type Spot = Node & Content & {
  __typename?: 'Spot';
  id: Scalars['ID'];
  created?: Maybe<Scalars['DateTime']>;
  lastModified?: Maybe<Scalars['DateTime']>;
  deleted?: Maybe<Scalars['DateTime']>;
  reviewStatus: ReviewStatus;
  point: GeoPoint;
  street?: Maybe<Scalars['String']>;
  postalCode?: Maybe<Scalars['String']>;
  note?: Maybe<Scalars['String']>;
  images: Array<Image>;
  /** Get the ids of all non-deleted stolpersteine present at this spot. */
  stolpersteinIds: Array<Scalars['String']>;
  /** A detail on the specific street spot. */
  streetDetail?: Maybe<StreetDetail>;
  place?: Maybe<Place>;
  /** The Stolpersteine at this spot. */
  stolpersteine: Array<Stolperstein>;
  placeId?: Maybe<Scalars['String']>;
};

export type SpotInput = {
  longitude: Scalars['Float'];
  latitude: Scalars['Float'];
  street?: Maybe<Scalars['String']>;
  corner?: Maybe<Scalars['String']>;
  number?: Maybe<Scalars['String']>;
  postalCode?: Maybe<Scalars['String']>;
  placeId?: Maybe<Scalars['String']>;
  note?: Maybe<Scalars['String']>;
  imageIds?: Maybe<Array<Scalars['String']>>;
};

/** Condensed information on a spot as well as aassociated persons and Stolpersteine. */
export type SpotSummary = {
  __typename?: 'SpotSummary';
  id: Scalars['String'];
  created?: Maybe<Scalars['DateTime']>;
  lastModified?: Maybe<Scalars['DateTime']>;
  deleted?: Maybe<Scalars['DateTime']>;
  reviewStatus?: Maybe<ReviewStatus>;
  point: GeoPoint;
  street?: Maybe<Scalars['String']>;
  streetNumber?: Maybe<Scalars['String']>;
  streetCorner?: Maybe<Scalars['String']>;
  postalCode?: Maybe<Scalars['String']>;
  placeId?: Maybe<Scalars['String']>;
  stolpersteinSummaries: Array<StolpersteinSummary>;
};

/** An object representing a Stolperstein. */
export type Stolperstein = Node & Content & {
  __typename?: 'Stolperstein';
  id: Scalars['ID'];
  created?: Maybe<Scalars['DateTime']>;
  lastModified?: Maybe<Scalars['DateTime']>;
  deleted?: Maybe<Scalars['DateTime']>;
  reviewStatus: ReviewStatus;
  laid?: Maybe<Scalars['HistoricalDate']>;
  /** The text inscribed on the stone (containing newlines). */
  inscription?: Maybe<Scalars['String']>;
  historicalStreet?: Maybe<Scalars['String']>;
  /** Information about people/groups that initiated the laying of this Stolperstein. */
  initiativeHTML?: Maybe<Scalars['String']>;
  /** Information about people/groups that helped finance the laying of this Stolperstein. */
  patronageHTML?: Maybe<Scalars['String']>;
  /** Information about people/groups that helped gather information about the subject of the Stolperstein. */
  dataGatheringHTML?: Maybe<Scalars['String']>;
  /** Information about people/groups that funded this Stolperstein. */
  fundingHTML?: Maybe<Scalars['String']>;
  /** The spot of the Stolperstein. */
  spot: Spot;
  /** An image of the Stolperstein. */
  image?: Maybe<Image>;
  /** The subjects of the Stolperstein. */
  subjects: Array<StolpersteinSubject>;
  /** A detail on the specific historical street spot. */
  historicalStreetDetail?: Maybe<StreetDetail>;
  historicalPlace?: Maybe<Place>;
};

export type StolpersteinInput = {
  /** The ID of the Spot of the Stolperstein. */
  spotId: Scalars['String'];
  /** The date when the Stolperstein was laid. */
  laid?: Maybe<Scalars['HistoricalDate']>;
  /** The text inscribed on the Stolperstein (usually multi-line). */
  inscription?: Maybe<Scalars['String']>;
  /** The id of an image object. */
  imageId?: Maybe<Scalars['String']>;
  /** A list of the IDs of the subjects of this Stolperstein. */
  subjectIds?: Maybe<Array<Scalars['String']>>;
  historicalStreet?: Maybe<Scalars['String']>;
  historicalCorner?: Maybe<Scalars['String']>;
  historicalNumber?: Maybe<Scalars['String']>;
  historicalPlaceId?: Maybe<Scalars['String']>;
  /** Information about people/groups that initiated the laying of this Stolperstein. */
  initiativeHTML?: Maybe<Scalars['String']>;
  /** Information about people/groups that helped finance the laying of this Stolperstein. */
  patronageHTML?: Maybe<Scalars['String']>;
  /** Information about people/groups that helped gather information about the subject of the Stolperstein. */
  dataGatheringHTML?: Maybe<Scalars['String']>;
  /** Information about people/groups that funded this Stolperstein. */
  fundingHTML?: Maybe<Scalars['String']>;
};

/** An object that can be the subject of a Stolperstein. */
export type StolpersteinSubject = {
  subjectOf: Array<Stolperstein>;
};

/** Condensed information on a Stolperstein as well as aassociated persons. */
export type StolpersteinSummary = {
  __typename?: 'StolpersteinSummary';
  id: Scalars['String'];
  created?: Maybe<Scalars['DateTime']>;
  lastModified?: Maybe<Scalars['DateTime']>;
  deleted?: Maybe<Scalars['DateTime']>;
  reviewStatus?: Maybe<ReviewStatus>;
  spotId: Scalars['String'];
  personSummaries: Array<PersonSummary>;
};

export type StreetDetail = StreetDetailCorner | StreetDetailNumber;

/** A street detail given as a cornering street. */
export type StreetDetailCorner = TypedLiteral & {
  __typename?: 'StreetDetailCorner';
  value: Scalars['String'];
};

/** A street detail given as a street number. */
export type StreetDetailNumber = TypedLiteral & {
  __typename?: 'StreetDetailNumber';
  value: Scalars['String'];
};

/** A literal value differentiated by type. */
export type TypedLiteral = {
  value: Scalars['String'];
};

export type GeoPointFragment = (
  { __typename?: 'GeoPoint' }
  & Pick<GeoPoint, 'longitude' | 'latitude'>
);

type PersonName_BirthName_Fragment = (
  { __typename: 'BirthName' }
  & Pick<BirthName, 'value'>
);

type PersonName_FamilyName_Fragment = (
  { __typename: 'FamilyName' }
  & Pick<FamilyName, 'value'>
);

type PersonName_GivenName_Fragment = (
  { __typename: 'GivenName' }
  & Pick<GivenName, 'value'>
);

type PersonName_MarriedName_Fragment = (
  { __typename: 'MarriedName' }
  & Pick<MarriedName, 'value'>
);

type PersonName_Nickname_Fragment = (
  { __typename: 'Nickname' }
  & Pick<Nickname, 'value'>
);

type PersonName_Pseudonym_Fragment = (
  { __typename: 'Pseudonym' }
  & Pick<Pseudonym, 'value'>
);

export type PersonNameFragment = PersonName_BirthName_Fragment | PersonName_FamilyName_Fragment | PersonName_GivenName_Fragment | PersonName_MarriedName_Fragment | PersonName_Nickname_Fragment | PersonName_Pseudonym_Fragment;

export type SpotSummaryFragment = (
  { __typename?: 'SpotSummary' }
  & Pick<SpotSummary, 'id' | 'created' | 'lastModified' | 'deleted' | 'reviewStatus' | 'street' | 'streetNumber' | 'streetCorner' | 'postalCode' | 'placeId'>
  & { point: (
    { __typename?: 'GeoPoint' }
    & GeoPointFragment
  ), stolpersteinSummaries: Array<(
    { __typename?: 'StolpersteinSummary' }
    & Pick<StolpersteinSummary, 'id' | 'created' | 'lastModified' | 'deleted' | 'reviewStatus' | 'spotId'>
    & { personSummaries: Array<(
      { __typename?: 'PersonSummary' }
      & Pick<PersonSummary, 'id' | 'created' | 'lastModified' | 'deleted' | 'reviewStatus' | 'stolpersteinIds' | 'bornDate' | 'diedDate' | 'title'>
      & { names: Array<(
        { __typename?: 'BirthName' }
        & PersonName_BirthName_Fragment
      ) | (
        { __typename?: 'FamilyName' }
        & PersonName_FamilyName_Fragment
      ) | (
        { __typename?: 'GivenName' }
        & PersonName_GivenName_Fragment
      ) | (
        { __typename?: 'MarriedName' }
        & PersonName_MarriedName_Fragment
      ) | (
        { __typename?: 'Nickname' }
        & PersonName_Nickname_Fragment
      ) | (
        { __typename?: 'Pseudonym' }
        & PersonName_Pseudonym_Fragment
      )> }
    )> }
  )> }
);

export type PlaceSummaryFragment = (
  { __typename?: 'PlaceSummary' }
  & Pick<PlaceSummary, 'id' | 'label' | 'parentId' | 'alternativeNames'>
);

export type ImageFragment = (
  { __typename?: 'Image' }
  & Pick<Image, 'id' | 'path' | 'fileName' | 'authorshipRemark' | 'caption' | 'alternativeText'>
);

export type PersonFragment = (
  { __typename?: 'Person' }
  & Pick<Person, 'id' | 'gender' | 'bornDate' | 'diedDate' | 'biographyHTML' | 'editingNotesHTML'>
  & { subjectOf: Array<(
    { __typename?: 'Stolperstein' }
    & Pick<Stolperstein, 'id'>
  )>, names: Array<(
    { __typename?: 'BirthName' }
    & PersonName_BirthName_Fragment
  ) | (
    { __typename?: 'FamilyName' }
    & PersonName_FamilyName_Fragment
  ) | (
    { __typename?: 'GivenName' }
    & PersonName_GivenName_Fragment
  ) | (
    { __typename?: 'MarriedName' }
    & PersonName_MarriedName_Fragment
  ) | (
    { __typename?: 'Nickname' }
    & PersonName_Nickname_Fragment
  ) | (
    { __typename?: 'Pseudonym' }
    & PersonName_Pseudonym_Fragment
  )>, occupations: Array<(
    { __typename?: 'Occupation' }
    & Pick<Occupation, 'id'>
  )>, persecutionPretexts: Array<(
    { __typename?: 'PersecutionPretext' }
    & Pick<PersecutionPretext, 'id'>
  )>, title?: Maybe<(
    { __typename?: 'PersonTitle' }
    & Pick<PersonTitle, 'value'>
  )>, bornPlace?: Maybe<(
    { __typename?: 'Place' }
    & Pick<Place, 'id'>
  )>, diedPlace?: Maybe<(
    { __typename?: 'Place' }
    & Pick<Place, 'id'>
  )> }
  & IContent_Person_Fragment
);

export type StolpersteinFragment = (
  { __typename?: 'Stolperstein' }
  & Pick<Stolperstein, 'id' | 'laid' | 'inscription' | 'historicalStreet' | 'initiativeHTML' | 'patronageHTML' | 'dataGatheringHTML' | 'fundingHTML'>
  & { spot: (
    { __typename?: 'Spot' }
    & Pick<Spot, 'id'>
  ), image?: Maybe<(
    { __typename?: 'Image' }
    & Pick<Image, 'id'>
  )>, historicalStreetDetail?: Maybe<(
    { __typename: 'StreetDetailCorner' }
    & Pick<StreetDetailCorner, 'value'>
  ) | (
    { __typename: 'StreetDetailNumber' }
    & Pick<StreetDetailNumber, 'value'>
  )>, historicalPlace?: Maybe<(
    { __typename?: 'Place' }
    & Pick<Place, 'id'>
  )>, subjects: Array<(
    { __typename?: 'Person' }
    & Pick<Person, 'id'>
  )> }
  & IContent_Stolperstein_Fragment
);

export type PlaceFragment = (
  { __typename?: 'Place' }
  & Pick<Place, 'id' | 'label' | 'placeType' | 'alternativeNames'>
  & { parent?: Maybe<(
    { __typename?: 'Place' }
    & Pick<Place, 'id'>
  )> }
);

export type SpotFragment = (
  { __typename?: 'Spot' }
  & Pick<Spot, 'id' | 'street' | 'postalCode' | 'placeId' | 'stolpersteinIds' | 'note'>
  & { point: (
    { __typename?: 'GeoPoint' }
    & GeoPointFragment
  ), streetDetail?: Maybe<(
    { __typename: 'StreetDetailCorner' }
    & Pick<StreetDetailCorner, 'value'>
  ) | (
    { __typename: 'StreetDetailNumber' }
    & Pick<StreetDetailNumber, 'value'>
  )> }
  & IContent_Spot_Fragment
);

export type PersecutionPretextFragment = (
  { __typename?: 'PersecutionPretext' }
  & Pick<PersecutionPretext, 'id' | 'label' | 'label_f' | 'label_m'>
  & { locale: (
    { __typename?: 'Locale' }
    & Pick<Locale, 'code'>
  ), localizations: Array<(
    { __typename?: 'PersecutionPretextLocalization' }
    & Pick<PersecutionPretextLocalization, 'label' | 'label_f' | 'label_m'>
    & { locale: (
      { __typename?: 'Locale' }
      & Pick<Locale, 'code'>
    ) }
  )> }
);

export type OccupationFragment = (
  { __typename?: 'Occupation' }
  & Pick<Occupation, 'id' | 'label' | 'label_f' | 'label_m'>
  & { locale: (
    { __typename?: 'Locale' }
    & Pick<Locale, 'code'>
  ), localizations: Array<(
    { __typename?: 'OccupationLocalization' }
    & Pick<OccupationLocalization, 'label' | 'label_f' | 'label_m'>
    & { locale: (
      { __typename?: 'Locale' }
      & Pick<Locale, 'code'>
    ) }
  )> }
);

export type RelationshipTypeFragment = (
  { __typename?: 'RelationshipType' }
  & Pick<RelationshipType, 'id' | 'subjectLabel' | 'subjectLabel_f' | 'subjectLabel_m' | 'objectLabel' | 'objectLabel_f' | 'objectLabel_m'>
  & { locale: (
    { __typename?: 'Locale' }
    & Pick<Locale, 'code'>
  ), localizations: Array<(
    { __typename?: 'RelationshipTypeLocalization' }
    & Pick<RelationshipTypeLocalization, 'subjectLabel' | 'subjectLabel_f' | 'subjectLabel_m' | 'objectLabel' | 'objectLabel_f' | 'objectLabel_m'>
    & { locale: (
      { __typename?: 'Locale' }
      & Pick<Locale, 'code'>
    ) }
  )> }
);

export type RelationshipFragment = (
  { __typename?: 'Relationship' }
  & { type: (
    { __typename?: 'RelationshipType' }
    & Pick<RelationshipType, 'id'>
  ), subject: (
    { __typename?: 'Person' }
    & Pick<Person, 'id'>
  ), object: (
    { __typename?: 'Person' }
    & Pick<Person, 'id'>
  ) }
);

export type LocaleFragment = (
  { __typename?: 'Locale' }
  & Pick<Locale, 'code' | 'label'>
);

type INode_RelationshipType_Fragment = (
  { __typename?: 'RelationshipType' }
  & Pick<RelationshipType, 'id'>
);

type INode_Stolperstein_Fragment = (
  { __typename?: 'Stolperstein' }
  & Pick<Stolperstein, 'id'>
);

type INode_Spot_Fragment = (
  { __typename?: 'Spot' }
  & Pick<Spot, 'id'>
);

type INode_Image_Fragment = (
  { __typename?: 'Image' }
  & Pick<Image, 'id'>
);

type INode_Place_Fragment = (
  { __typename?: 'Place' }
  & Pick<Place, 'id'>
);

type INode_Occupation_Fragment = (
  { __typename?: 'Occupation' }
  & Pick<Occupation, 'id'>
);

type INode_PersecutionPretext_Fragment = (
  { __typename?: 'PersecutionPretext' }
  & Pick<PersecutionPretext, 'id'>
);

type INode_Person_Fragment = (
  { __typename?: 'Person' }
  & Pick<Person, 'id'>
);

export type INodeFragment = INode_RelationshipType_Fragment | INode_Stolperstein_Fragment | INode_Spot_Fragment | INode_Image_Fragment | INode_Place_Fragment | INode_Occupation_Fragment | INode_PersecutionPretext_Fragment | INode_Person_Fragment;

type Node_RelationshipType_Fragment = (
  { __typename: 'RelationshipType' }
  & INode_RelationshipType_Fragment
);

type Node_Stolperstein_Fragment = (
  { __typename: 'Stolperstein' }
  & INode_Stolperstein_Fragment
);

type Node_Spot_Fragment = (
  { __typename: 'Spot' }
  & INode_Spot_Fragment
);

type Node_Image_Fragment = (
  { __typename: 'Image' }
  & INode_Image_Fragment
);

type Node_Place_Fragment = (
  { __typename: 'Place' }
  & INode_Place_Fragment
);

type Node_Occupation_Fragment = (
  { __typename: 'Occupation' }
  & INode_Occupation_Fragment
);

type Node_PersecutionPretext_Fragment = (
  { __typename: 'PersecutionPretext' }
  & INode_PersecutionPretext_Fragment
);

type Node_Person_Fragment = (
  { __typename: 'Person' }
  & INode_Person_Fragment
);

export type NodeFragment = Node_RelationshipType_Fragment | Node_Stolperstein_Fragment | Node_Spot_Fragment | Node_Image_Fragment | Node_Place_Fragment | Node_Occupation_Fragment | Node_PersecutionPretext_Fragment | Node_Person_Fragment;

type IContent_Stolperstein_Fragment = (
  { __typename?: 'Stolperstein' }
  & Pick<Stolperstein, 'created' | 'lastModified' | 'deleted' | 'reviewStatus'>
);

type IContent_Spot_Fragment = (
  { __typename?: 'Spot' }
  & Pick<Spot, 'created' | 'lastModified' | 'deleted' | 'reviewStatus'>
);

type IContent_Person_Fragment = (
  { __typename?: 'Person' }
  & Pick<Person, 'created' | 'lastModified' | 'deleted' | 'reviewStatus'>
);

export type IContentFragment = IContent_Stolperstein_Fragment | IContent_Spot_Fragment | IContent_Person_Fragment;

type Content_Stolperstein_Fragment = (
  { __typename: 'Stolperstein' }
  & IContent_Stolperstein_Fragment
);

type Content_Spot_Fragment = (
  { __typename: 'Spot' }
  & IContent_Spot_Fragment
);

type Content_Person_Fragment = (
  { __typename: 'Person' }
  & IContent_Person_Fragment
);

export type ContentFragment = Content_Stolperstein_Fragment | Content_Spot_Fragment | Content_Person_Fragment;

export type StolpersteinSubjectFragment = (
  { __typename: 'Person' }
  & { subjectOf: Array<(
    { __typename?: 'Stolperstein' }
    & Pick<Stolperstein, 'id'>
  )> }
);

export type SetStolpersteinReviewStatusMutationVariables = Exact<{
  id: Scalars['String'];
  status: ReviewStatus;
}>;


export type SetStolpersteinReviewStatusMutation = (
  { __typename?: 'Mutation' }
  & { setStolpersteinReviewStatus: (
    { __typename?: 'Stolperstein' }
    & StolpersteinFragment
  ) }
);

export type DeepDeleteSpotMutationVariables = Exact<{
  id: Scalars['String'];
}>;


export type DeepDeleteSpotMutation = (
  { __typename?: 'Mutation' }
  & { deepDeleteSpot: (
    { __typename?: 'Spot' }
    & SpotFragment
  ) }
);

export type DeepDeleteStolpersteinMutationVariables = Exact<{
  id: Scalars['String'];
}>;


export type DeepDeleteStolpersteinMutation = (
  { __typename?: 'Mutation' }
  & { deepDeleteStolperstein: (
    { __typename?: 'Stolperstein' }
    & StolpersteinFragment
  ) }
);

export type SetRelationshipMutationVariables = Exact<{
  typeId: Scalars['String'];
  subjectId: Scalars['String'];
  objectId: Scalars['String'];
}>;


export type SetRelationshipMutation = (
  { __typename?: 'Mutation' }
  & { setRelationship: (
    { __typename?: 'Relationship' }
    & RelationshipFragment
  ) }
);

export type UnsetRelationshipMutationVariables = Exact<{
  oneId: Scalars['String'];
  otherId: Scalars['String'];
}>;


export type UnsetRelationshipMutation = (
  { __typename?: 'Mutation' }
  & { unsetRelationship?: Maybe<(
    { __typename?: 'Relationship' }
    & RelationshipFragment
  )> }
);

export type CreateSpotMutationVariables = Exact<{
  input: SpotInput;
}>;


export type CreateSpotMutation = (
  { __typename?: 'Mutation' }
  & { createSpot: (
    { __typename?: 'Spot' }
    & SpotFragment
  ) }
);

export type UpdateSpotMutationVariables = Exact<{
  id: Scalars['String'];
  input: SpotInput;
}>;


export type UpdateSpotMutation = (
  { __typename?: 'Mutation' }
  & { updateSpot: (
    { __typename?: 'Spot' }
    & SpotFragment
  ) }
);

export type RestoreSpotMutationVariables = Exact<{
  id: Scalars['String'];
  input: SpotInput;
}>;


export type RestoreSpotMutation = (
  { __typename?: 'Mutation' }
  & { restoreSpot: (
    { __typename?: 'Spot' }
    & SpotFragment
  ) }
);

export type CreateImageMutationVariables = Exact<{
  input: ImageInput;
}>;


export type CreateImageMutation = (
  { __typename?: 'Mutation' }
  & { createImage: (
    { __typename?: 'Image' }
    & ImageFragment
  ) }
);

export type UpdateImageMutationVariables = Exact<{
  id: Scalars['String'];
  input: ImageInput;
}>;


export type UpdateImageMutation = (
  { __typename?: 'Mutation' }
  & { updateImage: (
    { __typename?: 'Image' }
    & ImageFragment
  ) }
);

export type RestoreImageMutationVariables = Exact<{
  id: Scalars['String'];
  input: ImageInput;
}>;


export type RestoreImageMutation = (
  { __typename?: 'Mutation' }
  & { restoreImage: (
    { __typename?: 'Image' }
    & ImageFragment
  ) }
);

export type CreateStolpersteinMutationVariables = Exact<{
  input: StolpersteinInput;
}>;


export type CreateStolpersteinMutation = (
  { __typename?: 'Mutation' }
  & { createStolperstein: (
    { __typename?: 'Stolperstein' }
    & StolpersteinFragment
  ) }
);

export type UpdateStolpersteinMutationVariables = Exact<{
  id: Scalars['String'];
  input: StolpersteinInput;
}>;


export type UpdateStolpersteinMutation = (
  { __typename?: 'Mutation' }
  & { updateStolperstein: (
    { __typename?: 'Stolperstein' }
    & StolpersteinFragment
  ) }
);

export type RestoreStolpersteinMutationVariables = Exact<{
  id: Scalars['String'];
  input: StolpersteinInput;
}>;


export type RestoreStolpersteinMutation = (
  { __typename?: 'Mutation' }
  & { restoreStolperstein: (
    { __typename?: 'Stolperstein' }
    & StolpersteinFragment
  ) }
);

export type CreatePersonMutationVariables = Exact<{
  input: PersonInput;
}>;


export type CreatePersonMutation = (
  { __typename?: 'Mutation' }
  & { createPerson: (
    { __typename?: 'Person' }
    & PersonFragment
  ) }
);

export type UpdatePersonMutationVariables = Exact<{
  id: Scalars['String'];
  input: PersonInput;
}>;


export type UpdatePersonMutation = (
  { __typename?: 'Mutation' }
  & { updatePerson: (
    { __typename?: 'Person' }
    & PersonFragment
  ) }
);

export type RestorePersonMutationVariables = Exact<{
  id: Scalars['String'];
  input: PersonInput;
}>;


export type RestorePersonMutation = (
  { __typename?: 'Mutation' }
  & { restorePerson: (
    { __typename?: 'Person' }
    & PersonFragment
  ) }
);

export type CreateOccupationMutationVariables = Exact<{
  input: OccupationInput;
}>;


export type CreateOccupationMutation = (
  { __typename?: 'Mutation' }
  & { createOccupation: (
    { __typename?: 'Occupation' }
    & OccupationFragment
  ) }
);

export type UpdateOccupationMutationVariables = Exact<{
  id: Scalars['String'];
  input: OccupationInput;
}>;


export type UpdateOccupationMutation = (
  { __typename?: 'Mutation' }
  & { updateOccupation: (
    { __typename?: 'Occupation' }
    & OccupationFragment
  ) }
);

export type RestoreOccupationMutationVariables = Exact<{
  id: Scalars['String'];
  input: OccupationInput;
}>;


export type RestoreOccupationMutation = (
  { __typename?: 'Mutation' }
  & { restoreOccupation: (
    { __typename?: 'Occupation' }
    & OccupationFragment
  ) }
);

export type CreatePersecutionPretextMutationVariables = Exact<{
  input: PersecutionPretextInput;
}>;


export type CreatePersecutionPretextMutation = (
  { __typename?: 'Mutation' }
  & { createPersecutionPretext: (
    { __typename?: 'PersecutionPretext' }
    & PersecutionPretextFragment
  ) }
);

export type UpdatePersecutionPretextMutationVariables = Exact<{
  id: Scalars['String'];
  input: PersecutionPretextInput;
}>;


export type UpdatePersecutionPretextMutation = (
  { __typename?: 'Mutation' }
  & { updatePersecutionPretext: (
    { __typename?: 'PersecutionPretext' }
    & PersecutionPretextFragment
  ) }
);

export type RestorePersecutionPretextMutationVariables = Exact<{
  id: Scalars['String'];
  input: PersecutionPretextInput;
}>;


export type RestorePersecutionPretextMutation = (
  { __typename?: 'Mutation' }
  & { restorePersecutionPretext: (
    { __typename?: 'PersecutionPretext' }
    & PersecutionPretextFragment
  ) }
);

export type CreatePlaceMutationVariables = Exact<{
  input: PlaceInput;
}>;


export type CreatePlaceMutation = (
  { __typename?: 'Mutation' }
  & { createPlace: (
    { __typename?: 'Place' }
    & PlaceFragment
  ) }
);

export type UpdatePlaceMutationVariables = Exact<{
  id: Scalars['String'];
  input: PlaceInput;
}>;


export type UpdatePlaceMutation = (
  { __typename?: 'Mutation' }
  & { updatePlace: (
    { __typename?: 'Place' }
    & PlaceFragment
  ) }
);

export type RestorePlaceMutationVariables = Exact<{
  id: Scalars['String'];
  input: PlaceInput;
}>;


export type RestorePlaceMutation = (
  { __typename?: 'Mutation' }
  & { restorePlace: (
    { __typename?: 'Place' }
    & PlaceFragment
  ) }
);

export type CreateRelationshipTypeMutationVariables = Exact<{
  input: RelationshipTypeInput;
}>;


export type CreateRelationshipTypeMutation = (
  { __typename?: 'Mutation' }
  & { createRelationshipType: (
    { __typename?: 'RelationshipType' }
    & RelationshipTypeFragment
  ) }
);

export type UpdateRelationshipTypeMutationVariables = Exact<{
  id: Scalars['String'];
  input: RelationshipTypeInput;
}>;


export type UpdateRelationshipTypeMutation = (
  { __typename?: 'Mutation' }
  & { updateRelationshipType: (
    { __typename?: 'RelationshipType' }
    & RelationshipTypeFragment
  ) }
);

export type RestoreRelationshipTypeMutationVariables = Exact<{
  id: Scalars['String'];
  input: RelationshipTypeInput;
}>;


export type RestoreRelationshipTypeMutation = (
  { __typename?: 'Mutation' }
  & { restoreRelationshipType: (
    { __typename?: 'RelationshipType' }
    & RelationshipTypeFragment
  ) }
);

export type AddLocaleMutationVariables = Exact<{
  code: Scalars['String'];
  label: Scalars['String'];
}>;


export type AddLocaleMutation = (
  { __typename?: 'Mutation' }
  & { addLocale: (
    { __typename?: 'Locale' }
    & LocaleFragment
  ) }
);

export type SummariesQueryVariables = Exact<{ [key: string]: never; }>;


export type SummariesQuery = (
  { __typename?: 'Query' }
  & { spotSummaries: Array<(
    { __typename?: 'SpotSummary' }
    & SpotSummaryFragment
  )>, placeSummaries: Array<(
    { __typename?: 'PlaceSummary' }
    & PlaceSummaryFragment
  )> }
);

export type LocalesQueryVariables = Exact<{ [key: string]: never; }>;


export type LocalesQuery = (
  { __typename?: 'Query' }
  & { locales: Array<(
    { __typename?: 'Locale' }
    & LocaleFragment
  )> }
);

export type SpotsQueryVariables = Exact<{
  ids?: Maybe<Array<Scalars['String']>>;
}>;


export type SpotsQuery = (
  { __typename?: 'Query' }
  & { spots: Array<(
    { __typename?: 'Spot' }
    & { place?: Maybe<(
      { __typename?: 'Place' }
      & Pick<Place, 'label'>
    )> }
    & SpotFragment
  )> }
);

export type ImagesQueryVariables = Exact<{
  ids?: Maybe<Array<Scalars['String']>>;
}>;


export type ImagesQuery = (
  { __typename?: 'Query' }
  & { images: Array<(
    { __typename?: 'Image' }
    & ImageFragment
  )> }
);

export type StolpersteineQueryVariables = Exact<{
  ids?: Maybe<Array<Scalars['String']>>;
}>;


export type StolpersteineQuery = (
  { __typename?: 'Query' }
  & { stolpersteine: Array<(
    { __typename?: 'Stolperstein' }
    & StolpersteinFragment
  )> }
);

export type StolpersteinQueryVariables = Exact<{
  id: Scalars['String'];
}>;


export type StolpersteinQuery = (
  { __typename?: 'Query' }
  & { stolperstein?: Maybe<(
    { __typename?: 'Stolperstein' }
    & StolpersteinFragment
  )> }
);

export type PersonsQueryVariables = Exact<{
  ids?: Maybe<Array<Scalars['String']>>;
}>;


export type PersonsQuery = (
  { __typename?: 'Query' }
  & { persons: Array<(
    { __typename?: 'Person' }
    & PersonFragment
  )> }
);

export type OccupationsQueryVariables = Exact<{ [key: string]: never; }>;


export type OccupationsQuery = (
  { __typename?: 'Query' }
  & { occupations: Array<(
    { __typename?: 'Occupation' }
    & OccupationFragment
  )> }
);

export type PersecutionPretextsQueryVariables = Exact<{ [key: string]: never; }>;


export type PersecutionPretextsQuery = (
  { __typename?: 'Query' }
  & { persecutionPretexts: Array<(
    { __typename?: 'PersecutionPretext' }
    & PersecutionPretextFragment
  )> }
);

export type PlacesQueryVariables = Exact<{ [key: string]: never; }>;


export type PlacesQuery = (
  { __typename?: 'Query' }
  & { places: Array<(
    { __typename?: 'Place' }
    & PlaceFragment
  )> }
);

export type RelationshipTypesQueryVariables = Exact<{ [key: string]: never; }>;


export type RelationshipTypesQuery = (
  { __typename?: 'Query' }
  & { relationshipTypes: Array<(
    { __typename?: 'RelationshipType' }
    & RelationshipTypeFragment
  )> }
);

export type RelationshipsQueryVariables = Exact<{ [key: string]: never; }>;


export type RelationshipsQuery = (
  { __typename?: 'Query' }
  & { relationships: Array<(
    { __typename?: 'Relationship' }
    & RelationshipFragment
  )> }
);

export type RelationshipsOfQueryVariables = Exact<{
  id: Scalars['String'];
}>;


export type RelationshipsOfQuery = (
  { __typename?: 'Query' }
  & { relationshipsWithSubject: Array<(
    { __typename?: 'Relationship' }
    & RelationshipFragment
  )>, relationshipsWithObject: Array<(
    { __typename?: 'Relationship' }
    & RelationshipFragment
  )> }
);

export type DumpQueryVariables = Exact<{ [key: string]: never; }>;


export type DumpQuery = (
  { __typename?: 'Query' }
  & { locales: Array<(
    { __typename?: 'Locale' }
    & LocaleFragment
  )>, persecutionPretexts: Array<(
    { __typename?: 'PersecutionPretext' }
    & PersecutionPretextFragment
  )>, occupations: Array<(
    { __typename?: 'Occupation' }
    & OccupationFragment
  )>, relationshipTypes: Array<(
    { __typename?: 'RelationshipType' }
    & RelationshipTypeFragment
  )>, places: Array<(
    { __typename?: 'Place' }
    & PlaceFragment
  )>, spots: Array<(
    { __typename?: 'Spot' }
    & SpotFragment
  )>, relationships: Array<(
    { __typename?: 'Relationship' }
    & RelationshipFragment
  )>, persons: Array<(
    { __typename?: 'Person' }
    & PersonFragment
  )>, stolpersteine: Array<(
    { __typename?: 'Stolperstein' }
    & StolpersteinFragment
  )>, nodes: Array<(
    { __typename?: 'RelationshipType' }
    & Node_RelationshipType_Fragment
  ) | (
    { __typename?: 'Stolperstein' }
    & Node_Stolperstein_Fragment
  ) | (
    { __typename?: 'Spot' }
    & Node_Spot_Fragment
  ) | (
    { __typename?: 'Image' }
    & Node_Image_Fragment
  ) | (
    { __typename?: 'Place' }
    & Node_Place_Fragment
  ) | (
    { __typename?: 'Occupation' }
    & Node_Occupation_Fragment
  ) | (
    { __typename?: 'PersecutionPretext' }
    & Node_PersecutionPretext_Fragment
  ) | (
    { __typename?: 'Person' }
    & Node_Person_Fragment
  )>, contents: Array<(
    { __typename?: 'Stolperstein' }
    & Content_Stolperstein_Fragment
  ) | (
    { __typename?: 'Spot' }
    & Content_Spot_Fragment
  ) | (
    { __typename?: 'Person' }
    & Content_Person_Fragment
  )>, stolpersteinSubjects: Array<(
    { __typename?: 'Person' }
    & StolpersteinSubjectFragment
  )> }
);


      export interface PossibleTypesResultData {
        possibleTypes: {
          [key: string]: string[]
        }
      }
      const result: PossibleTypesResultData = {
  "possibleTypes": {
    "Node": [
      "RelationshipType",
      "Stolperstein",
      "Spot",
      "Image",
      "Place",
      "Occupation",
      "PersecutionPretext",
      "Person"
    ],
    "StolpersteinSubject": [
      "Person"
    ],
    "Content": [
      "Stolperstein",
      "Spot",
      "Person"
    ],
    "StreetDetail": [
      "StreetDetailCorner",
      "StreetDetailNumber"
    ],
    "TypedLiteral": [
      "StreetDetailCorner",
      "StreetDetailNumber",
      "PersonTitle",
      "BirthName",
      "FamilyName",
      "GivenName",
      "MarriedName",
      "Nickname",
      "Pseudonym"
    ],
    "PersonName": [
      "BirthName",
      "FamilyName",
      "GivenName",
      "MarriedName",
      "Nickname",
      "Pseudonym"
    ]
  }
};
      export default result;
    
export const GeoPointFragmentDoc = gql`
    fragment GeoPoint on GeoPoint {
  longitude
  latitude
}
    `;
export const PersonNameFragmentDoc = gql`
    fragment PersonName on PersonName {
  __typename
  value
}
    `;
export const SpotSummaryFragmentDoc = gql`
    fragment SpotSummary on SpotSummary {
  id
  created
  lastModified
  deleted
  reviewStatus
  street
  streetNumber
  streetCorner
  postalCode
  placeId
  point {
    ...GeoPoint
  }
  stolpersteinSummaries {
    id
    created
    lastModified
    deleted
    reviewStatus
    spotId
    personSummaries {
      id
      created
      lastModified
      deleted
      reviewStatus
      stolpersteinIds
      bornDate
      diedDate
      title
      names {
        ...PersonName
      }
    }
  }
}
    ${GeoPointFragmentDoc}
${PersonNameFragmentDoc}`;
export const PlaceSummaryFragmentDoc = gql`
    fragment PlaceSummary on PlaceSummary {
  id
  label
  parentId
  alternativeNames
}
    `;
export const ImageFragmentDoc = gql`
    fragment Image on Image {
  id
  path
  fileName
  authorshipRemark
  caption
  alternativeText
}
    `;
export const IContentFragmentDoc = gql`
    fragment IContent on Content {
  created
  lastModified
  deleted
  reviewStatus
}
    `;
export const PersonFragmentDoc = gql`
    fragment Person on Person {
  id
  subjectOf {
    id
  }
  names {
    ...PersonName
  }
  occupations {
    id
  }
  persecutionPretexts {
    id
  }
  title {
    value
  }
  gender
  bornDate
  bornPlace {
    id
  }
  diedDate
  diedPlace {
    id
  }
  biographyHTML
  editingNotesHTML
  ...IContent
}
    ${PersonNameFragmentDoc}
${IContentFragmentDoc}`;
export const StolpersteinFragmentDoc = gql`
    fragment Stolperstein on Stolperstein {
  id
  spot {
    id
  }
  laid
  inscription
  image {
    id
  }
  historicalStreet
  historicalStreetDetail {
    __typename
    ... on StreetDetailCorner {
      value
    }
    ... on StreetDetailNumber {
      value
    }
  }
  historicalPlace {
    id
  }
  subjects {
    ... on Node {
      id
    }
  }
  initiativeHTML
  patronageHTML
  dataGatheringHTML
  fundingHTML
  ...IContent
}
    ${IContentFragmentDoc}`;
export const PlaceFragmentDoc = gql`
    fragment Place on Place {
  id
  label
  parent {
    id
  }
  placeType
  alternativeNames
}
    `;
export const SpotFragmentDoc = gql`
    fragment Spot on Spot {
  id
  point {
    ...GeoPoint
  }
  street
  streetDetail {
    __typename
    ... on StreetDetailCorner {
      value
    }
    ... on StreetDetailNumber {
      value
    }
  }
  postalCode
  placeId
  stolpersteinIds
  note
  ...IContent
}
    ${GeoPointFragmentDoc}
${IContentFragmentDoc}`;
export const PersecutionPretextFragmentDoc = gql`
    fragment PersecutionPretext on PersecutionPretext {
  id
  locale {
    code
  }
  label
  label_f
  label_m
  localizations {
    locale {
      code
    }
    label
    label_f
    label_m
  }
}
    `;
export const OccupationFragmentDoc = gql`
    fragment Occupation on Occupation {
  id
  locale {
    code
  }
  label
  label_f
  label_m
  localizations {
    locale {
      code
    }
    label
    label_f
    label_m
  }
}
    `;
export const RelationshipTypeFragmentDoc = gql`
    fragment RelationshipType on RelationshipType {
  id
  locale {
    code
  }
  subjectLabel
  subjectLabel_f
  subjectLabel_m
  objectLabel
  objectLabel_f
  objectLabel_m
  localizations {
    locale {
      code
    }
    subjectLabel
    subjectLabel_f
    subjectLabel_m
    objectLabel
    objectLabel_f
    objectLabel_m
  }
}
    `;
export const RelationshipFragmentDoc = gql`
    fragment Relationship on Relationship {
  type {
    id
  }
  subject {
    ... on Node {
      id
    }
  }
  object {
    ... on Node {
      id
    }
  }
}
    `;
export const LocaleFragmentDoc = gql`
    fragment Locale on Locale {
  code
  label
}
    `;
export const INodeFragmentDoc = gql`
    fragment INode on Node {
  id
}
    `;
export const NodeFragmentDoc = gql`
    fragment Node on Node {
  __typename
  ...INode
}
    ${INodeFragmentDoc}`;
export const ContentFragmentDoc = gql`
    fragment Content on Content {
  __typename
  ...IContent
}
    ${IContentFragmentDoc}`;
export const StolpersteinSubjectFragmentDoc = gql`
    fragment StolpersteinSubject on StolpersteinSubject {
  __typename
  subjectOf {
    id
  }
}
    `;
export const SetStolpersteinReviewStatusDocument = gql`
    mutation setStolpersteinReviewStatus($id: String!, $status: ReviewStatus!) {
  setStolpersteinReviewStatus(id: $id, status: $status) {
    ...Stolperstein
  }
}
    ${StolpersteinFragmentDoc}`;
export const DeepDeleteSpotDocument = gql`
    mutation deepDeleteSpot($id: String!) {
  deepDeleteSpot(id: $id) {
    ...Spot
  }
}
    ${SpotFragmentDoc}`;
export const DeepDeleteStolpersteinDocument = gql`
    mutation deepDeleteStolperstein($id: String!) {
  deepDeleteStolperstein(id: $id) {
    ...Stolperstein
  }
}
    ${StolpersteinFragmentDoc}`;
export const SetRelationshipDocument = gql`
    mutation setRelationship($typeId: String!, $subjectId: String!, $objectId: String!) {
  setRelationship(typeId: $typeId, subjectId: $subjectId, objectId: $objectId) {
    ...Relationship
  }
}
    ${RelationshipFragmentDoc}`;
export const UnsetRelationshipDocument = gql`
    mutation unsetRelationship($oneId: String!, $otherId: String!) {
  unsetRelationship(oneId: $oneId, otherId: $otherId) {
    ...Relationship
  }
}
    ${RelationshipFragmentDoc}`;
export const CreateSpotDocument = gql`
    mutation createSpot($input: SpotInput!) {
  createSpot(input: $input) {
    ...Spot
  }
}
    ${SpotFragmentDoc}`;
export const UpdateSpotDocument = gql`
    mutation updateSpot($id: String!, $input: SpotInput!) {
  updateSpot(id: $id, input: $input) {
    ...Spot
  }
}
    ${SpotFragmentDoc}`;
export const RestoreSpotDocument = gql`
    mutation restoreSpot($id: String!, $input: SpotInput!) {
  restoreSpot(id: $id, input: $input) {
    ...Spot
  }
}
    ${SpotFragmentDoc}`;
export const CreateImageDocument = gql`
    mutation createImage($input: ImageInput!) {
  createImage(input: $input) {
    ...Image
  }
}
    ${ImageFragmentDoc}`;
export const UpdateImageDocument = gql`
    mutation updateImage($id: String!, $input: ImageInput!) {
  updateImage(id: $id, input: $input) {
    ...Image
  }
}
    ${ImageFragmentDoc}`;
export const RestoreImageDocument = gql`
    mutation restoreImage($id: String!, $input: ImageInput!) {
  restoreImage(id: $id, input: $input) {
    ...Image
  }
}
    ${ImageFragmentDoc}`;
export const CreateStolpersteinDocument = gql`
    mutation createStolperstein($input: StolpersteinInput!) {
  createStolperstein(input: $input) {
    ...Stolperstein
  }
}
    ${StolpersteinFragmentDoc}`;
export const UpdateStolpersteinDocument = gql`
    mutation updateStolperstein($id: String!, $input: StolpersteinInput!) {
  updateStolperstein(id: $id, input: $input) {
    ...Stolperstein
  }
}
    ${StolpersteinFragmentDoc}`;
export const RestoreStolpersteinDocument = gql`
    mutation restoreStolperstein($id: String!, $input: StolpersteinInput!) {
  restoreStolperstein(id: $id, input: $input) {
    ...Stolperstein
  }
}
    ${StolpersteinFragmentDoc}`;
export const CreatePersonDocument = gql`
    mutation createPerson($input: PersonInput!) {
  createPerson(input: $input) {
    ...Person
  }
}
    ${PersonFragmentDoc}`;
export const UpdatePersonDocument = gql`
    mutation updatePerson($id: String!, $input: PersonInput!) {
  updatePerson(id: $id, input: $input) {
    ...Person
  }
}
    ${PersonFragmentDoc}`;
export const RestorePersonDocument = gql`
    mutation restorePerson($id: String!, $input: PersonInput!) {
  restorePerson(id: $id, input: $input) {
    ...Person
  }
}
    ${PersonFragmentDoc}`;
export const CreateOccupationDocument = gql`
    mutation createOccupation($input: OccupationInput!) {
  createOccupation(input: $input) {
    ...Occupation
  }
}
    ${OccupationFragmentDoc}`;
export const UpdateOccupationDocument = gql`
    mutation updateOccupation($id: String!, $input: OccupationInput!) {
  updateOccupation(id: $id, input: $input) {
    ...Occupation
  }
}
    ${OccupationFragmentDoc}`;
export const RestoreOccupationDocument = gql`
    mutation restoreOccupation($id: String!, $input: OccupationInput!) {
  restoreOccupation(id: $id, input: $input) {
    ...Occupation
  }
}
    ${OccupationFragmentDoc}`;
export const CreatePersecutionPretextDocument = gql`
    mutation createPersecutionPretext($input: PersecutionPretextInput!) {
  createPersecutionPretext(input: $input) {
    ...PersecutionPretext
  }
}
    ${PersecutionPretextFragmentDoc}`;
export const UpdatePersecutionPretextDocument = gql`
    mutation updatePersecutionPretext($id: String!, $input: PersecutionPretextInput!) {
  updatePersecutionPretext(id: $id, input: $input) {
    ...PersecutionPretext
  }
}
    ${PersecutionPretextFragmentDoc}`;
export const RestorePersecutionPretextDocument = gql`
    mutation restorePersecutionPretext($id: String!, $input: PersecutionPretextInput!) {
  restorePersecutionPretext(id: $id, input: $input) {
    ...PersecutionPretext
  }
}
    ${PersecutionPretextFragmentDoc}`;
export const CreatePlaceDocument = gql`
    mutation createPlace($input: PlaceInput!) {
  createPlace(input: $input) {
    ...Place
  }
}
    ${PlaceFragmentDoc}`;
export const UpdatePlaceDocument = gql`
    mutation updatePlace($id: String!, $input: PlaceInput!) {
  updatePlace(id: $id, input: $input) {
    ...Place
  }
}
    ${PlaceFragmentDoc}`;
export const RestorePlaceDocument = gql`
    mutation restorePlace($id: String!, $input: PlaceInput!) {
  restorePlace(id: $id, input: $input) {
    ...Place
  }
}
    ${PlaceFragmentDoc}`;
export const CreateRelationshipTypeDocument = gql`
    mutation createRelationshipType($input: RelationshipTypeInput!) {
  createRelationshipType(input: $input) {
    ...RelationshipType
  }
}
    ${RelationshipTypeFragmentDoc}`;
export const UpdateRelationshipTypeDocument = gql`
    mutation updateRelationshipType($id: String!, $input: RelationshipTypeInput!) {
  updateRelationshipType(id: $id, input: $input) {
    ...RelationshipType
  }
}
    ${RelationshipTypeFragmentDoc}`;
export const RestoreRelationshipTypeDocument = gql`
    mutation restoreRelationshipType($id: String!, $input: RelationshipTypeInput!) {
  restoreRelationshipType(id: $id, input: $input) {
    ...RelationshipType
  }
}
    ${RelationshipTypeFragmentDoc}`;
export const AddLocaleDocument = gql`
    mutation addLocale($code: String!, $label: String!) {
  addLocale(code: $code, label: $label) {
    ...Locale
  }
}
    ${LocaleFragmentDoc}`;
export const SummariesDocument = gql`
    query Summaries {
  spotSummaries {
    ...SpotSummary
  }
  placeSummaries {
    ...PlaceSummary
  }
}
    ${SpotSummaryFragmentDoc}
${PlaceSummaryFragmentDoc}`;
export const LocalesDocument = gql`
    query Locales {
  locales {
    ...Locale
  }
}
    ${LocaleFragmentDoc}`;
export const SpotsDocument = gql`
    query Spots($ids: [String!]) {
  spots(ids: $ids) {
    ...Spot
    place {
      label
    }
  }
}
    ${SpotFragmentDoc}`;
export const ImagesDocument = gql`
    query Images($ids: [String!]) {
  images(ids: $ids) {
    ...Image
  }
}
    ${ImageFragmentDoc}`;
export const StolpersteineDocument = gql`
    query Stolpersteine($ids: [String!]) {
  stolpersteine(ids: $ids) {
    ...Stolperstein
  }
}
    ${StolpersteinFragmentDoc}`;
export const StolpersteinDocument = gql`
    query Stolperstein($id: String!) {
  stolperstein(id: $id) {
    ...Stolperstein
  }
}
    ${StolpersteinFragmentDoc}`;
export const PersonsDocument = gql`
    query Persons($ids: [String!]) {
  persons(ids: $ids) {
    ...Person
  }
}
    ${PersonFragmentDoc}`;
export const OccupationsDocument = gql`
    query Occupations {
  occupations {
    ...Occupation
  }
}
    ${OccupationFragmentDoc}`;
export const PersecutionPretextsDocument = gql`
    query PersecutionPretexts {
  persecutionPretexts {
    ...PersecutionPretext
  }
}
    ${PersecutionPretextFragmentDoc}`;
export const PlacesDocument = gql`
    query Places {
  places {
    ...Place
  }
}
    ${PlaceFragmentDoc}`;
export const RelationshipTypesDocument = gql`
    query RelationshipTypes {
  relationshipTypes {
    ...RelationshipType
  }
}
    ${RelationshipTypeFragmentDoc}`;
export const RelationshipsDocument = gql`
    query Relationships {
  relationships {
    ...Relationship
  }
}
    ${RelationshipFragmentDoc}`;
export const RelationshipsOfDocument = gql`
    query RelationshipsOf($id: String!) {
  relationshipsWithSubject(subjectId: $id) {
    ...Relationship
  }
  relationshipsWithObject(objectId: $id) {
    ...Relationship
  }
}
    ${RelationshipFragmentDoc}`;
export const DumpDocument = gql`
    query Dump {
  locales {
    ...Locale
  }
  persecutionPretexts {
    ...PersecutionPretext
  }
  occupations {
    ...Occupation
  }
  relationshipTypes {
    ...RelationshipType
  }
  places {
    ...Place
  }
  spots {
    ...Spot
  }
  relationships {
    ...Relationship
  }
  persons {
    ...Person
  }
  stolpersteine {
    ...Stolperstein
  }
  nodes {
    ...Node
  }
  contents {
    ...Content
  }
  stolpersteinSubjects {
    ...StolpersteinSubject
  }
}
    ${LocaleFragmentDoc}
${PersecutionPretextFragmentDoc}
${OccupationFragmentDoc}
${RelationshipTypeFragmentDoc}
${PlaceFragmentDoc}
${SpotFragmentDoc}
${RelationshipFragmentDoc}
${PersonFragmentDoc}
${StolpersteinFragmentDoc}
${NodeFragmentDoc}
${ContentFragmentDoc}
${StolpersteinSubjectFragmentDoc}`;

export type SdkFunctionWrapper = <T>(action: () => Promise<T>) => Promise<T>;


const defaultWrapper: SdkFunctionWrapper = sdkFunction => sdkFunction();
export function getSdk(client: GraphQLClient, withWrapper: SdkFunctionWrapper = defaultWrapper) {
  return {
    setStolpersteinReviewStatus(variables: SetStolpersteinReviewStatusMutationVariables): Promise<SetStolpersteinReviewStatusMutation> {
      return withWrapper(() => client.request<SetStolpersteinReviewStatusMutation>(print(SetStolpersteinReviewStatusDocument), variables));
    },
    deepDeleteSpot(variables: DeepDeleteSpotMutationVariables): Promise<DeepDeleteSpotMutation> {
      return withWrapper(() => client.request<DeepDeleteSpotMutation>(print(DeepDeleteSpotDocument), variables));
    },
    deepDeleteStolperstein(variables: DeepDeleteStolpersteinMutationVariables): Promise<DeepDeleteStolpersteinMutation> {
      return withWrapper(() => client.request<DeepDeleteStolpersteinMutation>(print(DeepDeleteStolpersteinDocument), variables));
    },
    setRelationship(variables: SetRelationshipMutationVariables): Promise<SetRelationshipMutation> {
      return withWrapper(() => client.request<SetRelationshipMutation>(print(SetRelationshipDocument), variables));
    },
    unsetRelationship(variables: UnsetRelationshipMutationVariables): Promise<UnsetRelationshipMutation> {
      return withWrapper(() => client.request<UnsetRelationshipMutation>(print(UnsetRelationshipDocument), variables));
    },
    createSpot(variables: CreateSpotMutationVariables): Promise<CreateSpotMutation> {
      return withWrapper(() => client.request<CreateSpotMutation>(print(CreateSpotDocument), variables));
    },
    updateSpot(variables: UpdateSpotMutationVariables): Promise<UpdateSpotMutation> {
      return withWrapper(() => client.request<UpdateSpotMutation>(print(UpdateSpotDocument), variables));
    },
    restoreSpot(variables: RestoreSpotMutationVariables): Promise<RestoreSpotMutation> {
      return withWrapper(() => client.request<RestoreSpotMutation>(print(RestoreSpotDocument), variables));
    },
    createImage(variables: CreateImageMutationVariables): Promise<CreateImageMutation> {
      return withWrapper(() => client.request<CreateImageMutation>(print(CreateImageDocument), variables));
    },
    updateImage(variables: UpdateImageMutationVariables): Promise<UpdateImageMutation> {
      return withWrapper(() => client.request<UpdateImageMutation>(print(UpdateImageDocument), variables));
    },
    restoreImage(variables: RestoreImageMutationVariables): Promise<RestoreImageMutation> {
      return withWrapper(() => client.request<RestoreImageMutation>(print(RestoreImageDocument), variables));
    },
    createStolperstein(variables: CreateStolpersteinMutationVariables): Promise<CreateStolpersteinMutation> {
      return withWrapper(() => client.request<CreateStolpersteinMutation>(print(CreateStolpersteinDocument), variables));
    },
    updateStolperstein(variables: UpdateStolpersteinMutationVariables): Promise<UpdateStolpersteinMutation> {
      return withWrapper(() => client.request<UpdateStolpersteinMutation>(print(UpdateStolpersteinDocument), variables));
    },
    restoreStolperstein(variables: RestoreStolpersteinMutationVariables): Promise<RestoreStolpersteinMutation> {
      return withWrapper(() => client.request<RestoreStolpersteinMutation>(print(RestoreStolpersteinDocument), variables));
    },
    createPerson(variables: CreatePersonMutationVariables): Promise<CreatePersonMutation> {
      return withWrapper(() => client.request<CreatePersonMutation>(print(CreatePersonDocument), variables));
    },
    updatePerson(variables: UpdatePersonMutationVariables): Promise<UpdatePersonMutation> {
      return withWrapper(() => client.request<UpdatePersonMutation>(print(UpdatePersonDocument), variables));
    },
    restorePerson(variables: RestorePersonMutationVariables): Promise<RestorePersonMutation> {
      return withWrapper(() => client.request<RestorePersonMutation>(print(RestorePersonDocument), variables));
    },
    createOccupation(variables: CreateOccupationMutationVariables): Promise<CreateOccupationMutation> {
      return withWrapper(() => client.request<CreateOccupationMutation>(print(CreateOccupationDocument), variables));
    },
    updateOccupation(variables: UpdateOccupationMutationVariables): Promise<UpdateOccupationMutation> {
      return withWrapper(() => client.request<UpdateOccupationMutation>(print(UpdateOccupationDocument), variables));
    },
    restoreOccupation(variables: RestoreOccupationMutationVariables): Promise<RestoreOccupationMutation> {
      return withWrapper(() => client.request<RestoreOccupationMutation>(print(RestoreOccupationDocument), variables));
    },
    createPersecutionPretext(variables: CreatePersecutionPretextMutationVariables): Promise<CreatePersecutionPretextMutation> {
      return withWrapper(() => client.request<CreatePersecutionPretextMutation>(print(CreatePersecutionPretextDocument), variables));
    },
    updatePersecutionPretext(variables: UpdatePersecutionPretextMutationVariables): Promise<UpdatePersecutionPretextMutation> {
      return withWrapper(() => client.request<UpdatePersecutionPretextMutation>(print(UpdatePersecutionPretextDocument), variables));
    },
    restorePersecutionPretext(variables: RestorePersecutionPretextMutationVariables): Promise<RestorePersecutionPretextMutation> {
      return withWrapper(() => client.request<RestorePersecutionPretextMutation>(print(RestorePersecutionPretextDocument), variables));
    },
    createPlace(variables: CreatePlaceMutationVariables): Promise<CreatePlaceMutation> {
      return withWrapper(() => client.request<CreatePlaceMutation>(print(CreatePlaceDocument), variables));
    },
    updatePlace(variables: UpdatePlaceMutationVariables): Promise<UpdatePlaceMutation> {
      return withWrapper(() => client.request<UpdatePlaceMutation>(print(UpdatePlaceDocument), variables));
    },
    restorePlace(variables: RestorePlaceMutationVariables): Promise<RestorePlaceMutation> {
      return withWrapper(() => client.request<RestorePlaceMutation>(print(RestorePlaceDocument), variables));
    },
    createRelationshipType(variables: CreateRelationshipTypeMutationVariables): Promise<CreateRelationshipTypeMutation> {
      return withWrapper(() => client.request<CreateRelationshipTypeMutation>(print(CreateRelationshipTypeDocument), variables));
    },
    updateRelationshipType(variables: UpdateRelationshipTypeMutationVariables): Promise<UpdateRelationshipTypeMutation> {
      return withWrapper(() => client.request<UpdateRelationshipTypeMutation>(print(UpdateRelationshipTypeDocument), variables));
    },
    restoreRelationshipType(variables: RestoreRelationshipTypeMutationVariables): Promise<RestoreRelationshipTypeMutation> {
      return withWrapper(() => client.request<RestoreRelationshipTypeMutation>(print(RestoreRelationshipTypeDocument), variables));
    },
    addLocale(variables: AddLocaleMutationVariables): Promise<AddLocaleMutation> {
      return withWrapper(() => client.request<AddLocaleMutation>(print(AddLocaleDocument), variables));
    },
    Summaries(variables?: SummariesQueryVariables): Promise<SummariesQuery> {
      return withWrapper(() => client.request<SummariesQuery>(print(SummariesDocument), variables));
    },
    Locales(variables?: LocalesQueryVariables): Promise<LocalesQuery> {
      return withWrapper(() => client.request<LocalesQuery>(print(LocalesDocument), variables));
    },
    Spots(variables?: SpotsQueryVariables): Promise<SpotsQuery> {
      return withWrapper(() => client.request<SpotsQuery>(print(SpotsDocument), variables));
    },
    Images(variables?: ImagesQueryVariables): Promise<ImagesQuery> {
      return withWrapper(() => client.request<ImagesQuery>(print(ImagesDocument), variables));
    },
    Stolpersteine(variables?: StolpersteineQueryVariables): Promise<StolpersteineQuery> {
      return withWrapper(() => client.request<StolpersteineQuery>(print(StolpersteineDocument), variables));
    },
    Stolperstein(variables: StolpersteinQueryVariables): Promise<StolpersteinQuery> {
      return withWrapper(() => client.request<StolpersteinQuery>(print(StolpersteinDocument), variables));
    },
    Persons(variables?: PersonsQueryVariables): Promise<PersonsQuery> {
      return withWrapper(() => client.request<PersonsQuery>(print(PersonsDocument), variables));
    },
    Occupations(variables?: OccupationsQueryVariables): Promise<OccupationsQuery> {
      return withWrapper(() => client.request<OccupationsQuery>(print(OccupationsDocument), variables));
    },
    PersecutionPretexts(variables?: PersecutionPretextsQueryVariables): Promise<PersecutionPretextsQuery> {
      return withWrapper(() => client.request<PersecutionPretextsQuery>(print(PersecutionPretextsDocument), variables));
    },
    Places(variables?: PlacesQueryVariables): Promise<PlacesQuery> {
      return withWrapper(() => client.request<PlacesQuery>(print(PlacesDocument), variables));
    },
    RelationshipTypes(variables?: RelationshipTypesQueryVariables): Promise<RelationshipTypesQuery> {
      return withWrapper(() => client.request<RelationshipTypesQuery>(print(RelationshipTypesDocument), variables));
    },
    Relationships(variables?: RelationshipsQueryVariables): Promise<RelationshipsQuery> {
      return withWrapper(() => client.request<RelationshipsQuery>(print(RelationshipsDocument), variables));
    },
    RelationshipsOf(variables: RelationshipsOfQueryVariables): Promise<RelationshipsOfQuery> {
      return withWrapper(() => client.request<RelationshipsOfQuery>(print(RelationshipsOfDocument), variables));
    },
    Dump(variables?: DumpQueryVariables): Promise<DumpQuery> {
      return withWrapper(() => client.request<DumpQuery>(print(DumpDocument), variables));
    }
  };
}
export type Sdk = ReturnType<typeof getSdk>;