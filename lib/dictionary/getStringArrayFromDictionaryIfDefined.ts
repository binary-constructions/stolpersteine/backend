import { IDictionary, isValidStringArrayValue } from "./internal";

export const getStringArrayFromDictionaryIfDefined = (
  dict: IDictionary,
  term: string
): undefined | null | string[] => {
  const value: unknown = dict[term];

  if (
    value !== undefined &&
    value !== null &&
    !isValidStringArrayValue(value)
  ) {
    throw new Error(`Property '${term}' is not of type string[].`);
  }

  return value;
};
