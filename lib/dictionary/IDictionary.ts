export interface IDictionary {
  [term: string]: unknown;
}
