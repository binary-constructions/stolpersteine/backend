import { IDictionary } from "./internal";

export const getStringOrNullFromDictionary = (
  dict: IDictionary,
  term: string
): string | null => {
  const value: unknown = dict[term];

  if (value !== null && typeof value !== "string") {
    throw new Error(`Property '${term}' is neither of type string nor null.`);
  }

  return value;
};
