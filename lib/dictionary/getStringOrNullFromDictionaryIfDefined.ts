import { IDictionary } from "./internal";

export const getStringOrNullFromDictionaryIfDefined = (
  dict: IDictionary,
  term: string
): string | null | undefined => {
  const value: unknown = dict[term];

  if (value !== null && value !== undefined && typeof value !== "string") {
    throw new Error(`Property '${term}' is neither of type string nor null.`);
  }

  return value;
};
