import { IDictionary } from "./internal";

export const getNumberOrNullFromDictionaryIfDefined = (
  dict: IDictionary,
  term: string
): number | null | undefined => {
  const value: unknown = term in dict ? dict[term] : undefined;

  if (value !== undefined && value !== null && typeof value !== "number") {
    throw new Error(`Property '${term}' is defined but is not of type number.`);
  }

  return value;
};
