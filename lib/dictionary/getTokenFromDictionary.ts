import { IDictionary } from "./internal";

const isSomeEnum = <T>(e: T) => (token: any): token is T[keyof T] =>
  Object.values(e).includes(token as T[keyof T]);

export const getTokenFromDictionary = <T>(
  dict: IDictionary,
  token: string,
  enumType: T,
  fallback?: T[keyof T]
): T[keyof T] => {
  const value: unknown = dict[token];

  const valueWithFallback =
    value === undefined || value === null ? fallback : value;

  const testFunc = isSomeEnum(enumType);

  if (!testFunc(valueWithFallback)) {
    throw new Error(`Property '${token}' is not one of the allowed tokens.`);
  }

  return valueWithFallback;
};
