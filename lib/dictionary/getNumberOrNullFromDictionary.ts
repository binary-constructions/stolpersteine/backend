import { IDictionary } from "./internal";

export const getNumberOrNullFromDictionary = (
  dict: IDictionary,
  term: string
): number | null => {
  const value: unknown = dict[term];

  if (value !== null && typeof value !== "number") {
    throw new Error(`Property '${term}' is neither of type number nor null.`);
  }

  return value;
};
