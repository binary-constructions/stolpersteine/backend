import { IDictionary } from "./internal";

export const getNumberFromDictionary = (
  dict: IDictionary,
  term: string
): number => {
  const value: unknown = dict[term];

  if (typeof value !== "number") {
    throw new Error(`Property '${term}' is not of type number.`);
  }

  return value;
};
