export const isValidStringArrayValue = (value: unknown): value is string[] =>
  value instanceof Array && value.every((v) => typeof v === "string");
