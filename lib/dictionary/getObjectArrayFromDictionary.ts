import { IDictionary } from "./internal";

export const getObjectArrayFromDictionary = <T extends Object>(
  dict: IDictionary,
  term: string,
  asFunc: (something: IDictionary) => T
): T[] => {
  const value: unknown = dict[term];

  if (!(value instanceof Array)) {
    throw new Error(`Property '${term}' is not an array.`);
  }

  return value.map((v) => {
    if (!(v instanceof Object)) {
      throw new Error(
        `Property '${term}' contains at least one element that is not an Object`
      );
    }

    return asFunc(v);
  });
};
