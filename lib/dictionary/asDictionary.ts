import { IDictionary } from "./internal";

export const asDictionary = (something: unknown): IDictionary => {
  if (something === null || typeof something !== "object") {
    throw new Error("Dictionary needs to be an object.");
  }

  return something as IDictionary;
};
