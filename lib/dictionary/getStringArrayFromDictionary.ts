import { IDictionary, isValidStringArrayValue } from "./internal";

export const getStringArrayFromDictionary = (
  dict: IDictionary,
  term: string
): string[] => {
  const value: unknown = dict[term];

  if (!isValidStringArrayValue(value)) {
    throw new Error(`Property '${term}' is not of type string[].`);
  }

  return value;
};
