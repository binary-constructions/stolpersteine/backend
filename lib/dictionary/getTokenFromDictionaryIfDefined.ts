import { IDictionary } from "./internal";

const isSomeEnum = <T>(e: T) => (token: any): token is T[keyof T] =>
  Object.values(e).includes(token as T[keyof T]);

export const getTokenFromDictionaryIfDefined = <T>(
  dict: IDictionary,
  token: string,
  enumType: T
): T[keyof T] | undefined => {
  const value: unknown = dict[token];

  const testFunc = isSomeEnum(enumType);

  if (value !== undefined && !testFunc(value)) {
    throw new Error(
      `Property '${token}' is defined but not one of the allowed tokens.`
    );
  }

  return value;
};
