import { IDictionary } from "./internal";

export const getStringFromDictionaryIfDefined = (
  dict: IDictionary,
  term: string
): string | undefined => {
  const value: unknown = term in dict ? dict[term] : undefined;

  if (value !== undefined && typeof value !== "string") {
    throw new Error(`Property '${term}' is defined but is not of type string.`);
  }

  return value;
};
