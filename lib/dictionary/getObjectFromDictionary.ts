import { IDictionary, asDictionary } from "./internal";

export const getObjectFromDictionary = <T extends Object>(
  dict: IDictionary,
  term: string,
  asFunc: (something: IDictionary) => T
): T => asFunc(asDictionary(dict[term]));
