import { IDictionary } from "./internal";

export const getStringFromDictionary = (
  dict: IDictionary,
  term: string
): string => {
  const value: unknown = dict[term];

  if (typeof value !== "string") {
    throw new Error(`Property '${term}' is not of type string.`);
  }

  return value;
};
