import { IDictionary, asDictionary } from "./internal";

export const getObjectOrNullFromDictionary = <T extends Object>(
  dict: IDictionary,
  term: string,
  asFunc: (something: IDictionary) => T | null
): T | null => (dict[term] !== null ? asFunc(asDictionary(dict[term])) : null);
