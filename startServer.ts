import "reflect-metadata";
import * as express from "express";
//import * as graphqlHTTP from 'express-graphql';
import * as graphqlHTTP from "./localForks/express-graphql"; // FIXME

import { createSchema } from "./src/createSchema";

const port: string = process.env.PORT || "1789";

createSchema()
  .then((schema) => {
    const server = express();

    server.use(express.static("public"));

    server.use(
      "/graphql",
      graphqlHTTP({
        schema: schema,
        graphiql: true,
      })
    );

    server.listen(port, () => {
      console.log(`Server is listening on port ${port}!`);
    });
  })
  .catch((reason) => console.log(reason));
